/*
package app.livecampus.chat_activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hamik.Activity.Edit_profile;
import com.hamik.isquare3.hamik.R;
import com.hamik.json_files.ApiClient;
import com.hamik.model.DataModel;
import com.hamik.model.active_user_list.ActiveUserResponse;
import com.hamik.model.hotel_private_chat_list.PostsItem;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Groupchat_activity extends AppCompatActivity {

    private String messageBody;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    private JSONObject success;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList, arrayList_count;
    DataModel category;
    GridLayoutManager mLayoutManager;
    private Button mButtonSend;
    private EditText mEditTextMessage;
    ImageView img1,img2,img3,img4,img5,img6,addacti,delete;

    AsyncTask<Void, Void, Void> SendData;
    public TextView tv_Username;
    int value = -1;
//    Receivemessage asyncFetch;
    public Intent intent;
    private static final long delay = 2000L;
    private boolean mRecentlyBackPressed = false;
    private Handler mExitHandler = new Handler();
    private Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };
    CircleImageView userImage;
    Button blockbtn;
    LinearLayout linearChatView;
    Button btnJoin;
    String blockReason, login_userid;
    ImageView imageBack;
    boolean blockStatus = false;
    String strGroupID, strName, strImage;
    TextView txtcountprofile;
    private Call<ActiveUserResponse> call1;
    List<com.hamik.model.active_user_list.PostsItem> profileimagelist;

    public RelativeLayout reltimg;
    Listattendig grid_viewad;
    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);
        getSupportActionBar().hide();
        sp = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
        login_userid = sp.getString("login_userid", "");

        reltimg = (RelativeLayout) findViewById(R.id.reltimg);

        arrayList = new ArrayList<>();
        arrayList_count = new ArrayList<>();

        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        img5 = findViewById(R.id.img5);
        img6 = findViewById(R.id.img6);

        txtcountprofile = findViewById(R.id.txtcountprofile);

        linearChatView = (LinearLayout) findViewById(R.id.linearChatView);
        listView = (RecyclerView) findViewById(R.id.recyclerview);
        mButtonSend = (Button) findViewById(R.id.sendmsgbtn);
        btnJoin = (Button) findViewById(R.id.btnJoin);
        tv_Username = (TextView) findViewById(R.id.tv_Username);
        imageBack = (ImageView) findViewById(R.id.imageBack);

        if (getIntent().getExtras().getString("strGroupID") != null) {
            strGroupID = getIntent().getExtras().getString("strGroupID");
            strName = getIntent().getExtras().getString("strName");
            Log.e("strGroupID", "in group" + strGroupID);
            tv_Username.setText("" + strName);

            new Enter_GroupChat().execute();

        }
        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new JoinGroup().execute();
            }
        });


        reltimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calldialog();

            }
        });

        mEditTextMessage = (EditText) findViewById(R.id.et_message);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);
        mButtonSend.setAlpha(0.4f);


        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    mButtonSend.setAlpha(1f);

                }
            }


            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() <= 0) {
                    mButtonSend.setAlpha(0.4f);

                }
            }
        });
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditTextMessage.getText().length() > 1) {
                    messageBody = mEditTextMessage.getText().toString();
                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                } else {

                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");
            }
        });

        */
/*asyncFetch = new Receivemessage();
        asyncFetch.execute();
        callAsynchronousTask();
        listOfUsers();*//*


    }
    private void listOfUsers() {

        PrivateChatActivityParam params = new PrivateChatActivityParam(strGroupID);

        call1 = ApiClient.getApi().activeUsers(params);

        call1.enqueue(new Callback<ActiveUserResponse>() {
            @Override
            public void onResponse(Call<ActiveUserResponse> call, Response<ActiveUserResponse> response) {
                if(response.isSuccessful()){

                    profileimagelist=new ArrayList<>();

                    if(response.body().getPosts()!=null){
                        profileimagelist=response.body().getPosts();

                        if(profileimagelist.size()==1){
                            img6.setVisibility(View.VISIBLE);

                            String temp1 = (profileimagelist.get(0).getProfileimage());

                            temp1 = temp1.replaceAll(" ", "%20");


                            Picasso.with(Groupchat_activity.this)
                                    .load(temp1).placeholder(R.drawable.noimg)
                                    .into(img6);

                            img1.setVisibility(View.INVISIBLE);
                            img2.setVisibility(View.INVISIBLE);
                            img3.setVisibility(View.INVISIBLE);
                            img4.setVisibility(View.INVISIBLE);
                            img5.setVisibility(View.INVISIBLE);
                            img6.setVisibility(View.INVISIBLE);

                        }else if(profileimagelist.size()==2){
                            img5.setVisibility(View.VISIBLE);
                            img6.setVisibility(View.VISIBLE);

                            String temp1 = (profileimagelist.get(0).getProfileimage());
                            String temp2 = (profileimagelist.get(1).getProfileimage());

                            temp1 = temp1.replaceAll(" ", "%20");
                            temp2 = temp2.replaceAll(" ", "%20");


                            Picasso.with(Groupchat_activity.this)
                                    .load(temp1)
                                    .into(img5);

                            Picasso.with(Groupchat_activity.this)
                                    .load(temp2)
                                    .into(img6);

                            img3.setVisibility(View.INVISIBLE);
                            img4.setVisibility(View.INVISIBLE);
                            img1.setVisibility(View.INVISIBLE);
                            img2.setVisibility(View.INVISIBLE);

                        }else if(profileimagelist.size()==3){

                            img5.setVisibility(View.VISIBLE);
                            img6.setVisibility(View.VISIBLE);
                            img4.setVisibility(View.VISIBLE);


                            String temp1 = (profileimagelist.get(0).getProfileimage());
                            String temp2 = (profileimagelist.get(1).getProfileimage());
                            String temp3 = (profileimagelist.get(2).getProfileimage());

                            temp1 = temp1.replaceAll(" ", "%20");
                            temp2 = temp2.replaceAll(" ", "%20");
                            temp3 = temp3.replaceAll(" ", "%20");


                            Picasso.with(Groupchat_activity.this)
                                    .load(temp1)
                                    .into(img6);

                            Picasso.with(Groupchat_activity.this)
                                    .load(temp2)
                                    .into(img5);


                            Picasso.with(Groupchat_activity.this)
                                    .load(temp3)
                                    .into(img4);



                            img1.setVisibility(View.INVISIBLE);
                            img2.setVisibility(View.INVISIBLE);
                            img3.setVisibility(View.INVISIBLE);
                        }else if(profileimagelist.size()==4){
                            img6.setVisibility(View.VISIBLE);
                            img5.setVisibility(View.VISIBLE);
                            img4.setVisibility(View.VISIBLE);
                            img3.setVisibility(View.VISIBLE);

                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(0).getProfileimage())
                                    .into(img3);

                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(1).getProfileimage())
                                    .into(img4);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(2).getProfileimage())
                                    .into(img5);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(3).getProfileimage())
                                    .into(img6);

                            img1.setVisibility(View.INVISIBLE);
                            img2.setVisibility(View.INVISIBLE);

                        }else if(profileimagelist.size()==5){

                            img6.setVisibility(View.VISIBLE);
                            img5.setVisibility(View.VISIBLE);
                            img4.setVisibility(View.VISIBLE);
                            img3.setVisibility(View.VISIBLE);
                            img2.setVisibility(View.VISIBLE);

                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(0).getProfileimage())
                                    .into(img2);

                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(1).getProfileimage())
                                    .into(img3);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(2).getProfileimage())
                                    .into(img4);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(3).getProfileimage())
                                    .into(img5);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(4).getProfileimage())
                                    .into(img6);

                            img1.setVisibility(View.INVISIBLE);

                        }else if(profileimagelist.size()==6){

                            img1.setVisibility(View.VISIBLE);
                            img2.setVisibility(View.VISIBLE);
                            img3.setVisibility(View.VISIBLE);
                            img4.setVisibility(View.VISIBLE);
                            img5.setVisibility(View.VISIBLE);
                            img6.setVisibility(View.VISIBLE);

                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(0).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img1);

                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(1).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img2);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(2).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img3);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(3).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img4);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(4).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img5);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(5).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img6);
                        }else if(profileimagelist.size()>6){

                            img1.setVisibility(View.VISIBLE);
                            img2.setVisibility(View.VISIBLE);
                            img3.setVisibility(View.VISIBLE);
                            img4.setVisibility(View.VISIBLE);
                            img5.setVisibility(View.VISIBLE);
                            img6.setVisibility(View.VISIBLE);
                            txtcountprofile.setVisibility(View.VISIBLE);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(0).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img1);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(1).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img2);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(2).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img3);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(3).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img4);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(4).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img5);
                            Picasso.with(Groupchat_activity.this)
                                    .load(profileimagelist.get(5).getProfileimage()).placeholder(R.drawable.noimg)
                                    .into(img6);

                            txtcountprofile.setText("+"+ String.valueOf(profileimagelist.size()-6));

                        }

                    }

                }

            }


            @Override
            public void onFailure(Call<ActiveUserResponse> call, Throwable t) {
            }
        });

    }
    private void calldialog() {

        dialog= new Dialog(Groupchat_activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.imageclick_dialog);

        ImageView image = (ImageView) dialog.findViewById(R.id.ib_close);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.list);
        LinearLayout linearLayout = (LinearLayout) dialog.findViewById(R.id.ln1);
        Log.e("dialog","list"+profileimagelist);
        //profileimagelistdialog.addAll(profileimagelist);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Groupchat_activity.this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        grid_viewad = new Listattendig(Groupchat_activity.this,R.layout.attedinguseritems, (ArrayList<com.hamik.model.active_user_list.PostsItem>) profileimagelist);
        recyclerView.setAdapter(grid_viewad);


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public class Listattendig extends RecyclerView.Adapter<Listattendig.MyViewHolder> {

        private final Context context;
        private final int resource;
        private final ArrayList<com.hamik.model.active_user_list.PostsItem> list1;
        LayoutInflater layoutInflater;
        String user_type="different_user";
        public Listattendig(Context context, int resource, ArrayList<com.hamik.model.active_user_list.PostsItem> list1){
            this.context=context;
            this.resource=resource;
            this.list1=list1;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = layoutInflater.inflate(resource,parent,false);
            Listattendig.MyViewHolder myViewHolder= new Listattendig.MyViewHolder(v);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(Listattendig.MyViewHolder holder, final int position) {

            Picasso.with(context)
                    .load(list1.get(position).getProfileimage()).placeholder(R.drawable.noimg)
                    .into(holder.rawimage);

            holder.rawname.setText(list1.get(position).getUsername());

            holder.rawattendlineardialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, Edit_profile.class);
                    i.putExtra("user_id",list1.get(position).getUserid());
                    //Log.e("user_id","attending "+list1.get(position).getUids());
                    i.putExtra("different_user",user_type);
                    //Log.e("user_id","attending "+user_type);
                    context.startActivity(i);
//                    dialog.dismiss();

                }
            });
        }

        @Override
        public int getItemCount() {
            return list1.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView rawimage;
            TextView rawname;
            LinearLayout rawattendlineardialog;
            public MyViewHolder(View itemView) {
                super(itemView);

                rawimage=itemView.findViewById(R.id.userImage);
                rawname=itemView.findViewById(R.id.name);
                rawattendlineardialog=itemView.findViewById(R.id.rawattendlineardialog);

            }


        }
    }

    public static class PrivateChatActivityParam {
        String groupid;


        public PrivateChatActivityParam(String groupid) {

            this.groupid = groupid;
        }
    }
    private class Enter_GroupChat extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = ApiClient.BASE_URL + "enter_group_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("g_id", strGroupID);
                jo.put("from_user_id", login_userid);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {


                    if (success.getString("success").equals("1")) {


                        linearChatView.setVisibility(View.VISIBLE);
                        btnJoin.setVisibility(View.GONE);

                    } else {
                        linearChatView.setVisibility(View.GONE);
                        btnJoin.setVisibility(View.VISIBLE);

                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class JoinGroup extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = ApiClient.BASE_URL + "join_group_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("g_id", strGroupID);
                jo.put("invite_id", login_userid);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {


                    if (success.getString("success").equals("1")) {


                        linearChatView.setVisibility(View.VISIBLE);
                        btnJoin.setVisibility(View.GONE);

                    } else {
                        linearChatView.setVisibility(View.GONE);
                        btnJoin.setVisibility(View.VISIBLE);

                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private void sendTextMessage(String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = ApiClient.BASE_URL + "input_group_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("g_id", strGroupID);
                    jo.put("from_user_id", login_userid);
                    jo.put("to_user_id", "");
                    jo.put("message", messageBody);
                    jo.put("image", "NA");


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");


                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();


                        category.setSenderID("" + login_userid);
                        category.setRecieverID("" + strGroupID);
                        category.setMsg("" + messageBody);
//                        category.setImage("NA" );

                        arrayList.add(category);
                        adapter = new RecyclerViewAdapter_chat(Groupchat_activity.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect Charactor", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = ApiClient.BASE_URL + "output_group_chat.php";
            System.out.println("the url is :" + serverUrl);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("g_id", strGroupID);
                jo.put("from_user_id", "");
                jo.put("to_user_id", "");


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader("Content-Type", "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);


                try {

                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();


                                success = jsonArrayCategory.getJSONObject(i);

                                category.setSenderID("" + success.getString("SenderId"));
                                category.setRecieverID("" + success.getString("GroupId"));
                                category.setMsg("" + success.getString("Message"));
                                category.setProfile("" + success.getString("ProfileImage"));
                                category.setDatetime("" + success.getString("Time"));


                                arrayList.add(category);


                                adapter = new RecyclerViewAdapter_chat(Groupchat_activity.this, arrayList);
                                listView.setAdapter(adapter);

                                listView.scrollToPosition(arrayList.size() - 1);

                            }
                        }
                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = ApiClient.BASE_URL + "chat_pushnotification.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("from_user_id", login_userid);
                jo.put("to_user_id", strGroupID);
                jo.put("message", messageBody);
                jo.put("sender_name", strName);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {


                    if (success.getString("success").equals("1")) {

                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }


    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            new Receivemessage().execute();
                            adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);// 1 second

    }


    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            int layoutResource = 0;

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE);
            login_userid = sp.getString("login_userid", "");

            //String rid = sp.getString(Constant.USERID, "");
            String rid = login_userid;

            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);

            if (rid.equals(getDataAdapter1.getSenderID())) {
                layoutResource = R.layout.right_chat_bubble;
                String name = getDataAdapter1.getMsg();
                holder.tv_sender.setText(name);
                holder.txt_show_date_sender.setText(getDataAdapter1.getDatetime());
                holder.tv_sender.setVisibility(View.VISIBLE);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.relative_right.setVisibility(View.VISIBLE);
                holder.linear_Left.setVisibility(View.GONE);

            } else {
                layoutResource = R.layout.left_chat_bubble;
                holder.relative_right.setVisibility(View.GONE);
                holder.txt_show_date_reciever.setText(getDataAdapter1.getDatetime());

                String name = getDataAdapter1.getMsg();
                holder.tv_receiver.setText(name);
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.linear_Left.setVisibility(View.VISIBLE);
//                Picasso.with(Groupchat_activity.this).load(getDataAdapter1.getProfile()).rotate(0).placeholder(R.drawable.noimg).into(holder.my_image_view);

                String temp = getDataAdapter1.getProfile();

                temp = temp.replaceAll(" ", "%20");





                Glide.with(Groupchat_activity.this).load(temp)
                        .thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.drawable.noimg)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.my_image_view);

                Log.e("hell","ok");

            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tv_sender, tv_receiver, txt_show_date_sender, txt_show_date_reciever;
            LinearLayout linear_Left;
            LinearLayout relative_right;
            CircleImageView my_image_view;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.txt_msg_sender);
                tv_receiver = (TextView) itemView.findViewById(R.id.txt_msg_reciever);
                txt_show_date_sender = (TextView) itemView.findViewById(R.id.txt_show_date_sender);
                txt_show_date_reciever = (TextView) itemView.findViewById(R.id.txt_show_date_reciever);
                relative_right = (LinearLayout) itemView.findViewById(R.id.linear_right);
                linear_Left = (LinearLayout) itemView.findViewById(R.id.linear_left);
                my_image_view = (CircleImageView) itemView.findViewById(R.id.my_image_view);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopTimer();
        finish();
    }

    private void stopTimer() {

//        asyncFetch.cancel(true);

        if (timerTask != null) {
            timerTask.cancel();

        } else {
            Log.e("notcancle", "notcancle");

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        callAsynchronousTask();
        listOfUsers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimer();

    }
}

*/
