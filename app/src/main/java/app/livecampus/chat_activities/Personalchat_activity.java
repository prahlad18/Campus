package app.livecampus.chat_activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import app.livecampus.Activity.Campuschat_activity;
import app.livecampus.Activity.Chat_Tabbar;
import app.livecampus.Fragments.SearchBookFragment;
import app.livecampus.Fragments.SellTextFragment;
import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;


public class Personalchat_activity extends AppCompatActivity {

    private static final long delay = 2000L;
    public ImageView imageBook;
    public LinearLayout toolbar1;
    public Intent intent;
    public TextView tv_Username;
    public TextView tv_sender, bookname, offer;
    public CircleImageView sendrimage;
    public TextView title;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList, arrayList_count;
    DataModel category;
    GridLayoutManager mLayoutManager;
    String book_id, book_name, book_image, rid, receiver_id, sendername, senderimage, checktrue, senderimage1, charnm, searchuser;
    AsyncTask<Void, Void, Void> SendData;
    String strImage, strName;
    int value = -1;
    Receivemessage asyncFetch;
    CircleImageView userImage;
    Button blockbtn;
    LinearLayout sendmsglayout;
    String blockReason, login_userid;
    ImageView imageBack;
    String touserid, to_username;
    private String messageBody;
    private JSONObject success;
    private Button mButtonSend;
    private EditText mEditTextMessage;
    private boolean mRecentlyBackPressed = false;
    private Handler mExitHandler = new Handler();
    private Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personalchat);
        getSupportActionBar().hide();
        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);


        arrayList = new ArrayList<>();
        arrayList_count = new ArrayList<>();
        listView = (RecyclerView) findViewById(R.id.listview);
        mButtonSend = (Button) findViewById(R.id.btn_send);
        sendrimage = (CircleImageView) findViewById(R.id.senderimage);
        title = (TextView) findViewById(R.id.title);
        bookname = (TextView) findViewById(R.id.bookname);
        toolbar1 = (LinearLayout) findViewById(R.id.toolbar1);
        offer = (TextView) findViewById(R.id.title1);
        imageBook = (ImageView) findViewById(R.id.image);
        intent = getIntent();


        receiver_id = intent.getStringExtra("receiver_id");
        sendername = intent.getStringExtra("sendername");
        senderimage = intent.getStringExtra("senderimage");
        checktrue = intent.getStringExtra("list");
        searchuser = intent.getStringExtra("searchuser");

        String chars = capitalize(sendername);

        title.setText("" + chars);

        if (senderimage.equals("")) {
            Picasso.with(this)
                    .load(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .into(sendrimage);
        } else {
            Picasso.with(this)
                    .load(Constant.MAIN_URL + senderimage)
                    .placeholder(R.drawable.user)
                    .into(sendrimage);
        }

        mEditTextMessage = (EditText) findViewById(R.id.et_message);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);
        mButtonSend.setAlpha(0.4f);


        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mButtonSend.setAlpha(1f);

                }
            }


            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() <= 0) {
                    mButtonSend.setAlpha(0.4f);

                }
            }
        });
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditTextMessage.getText().length() > 0) {
                    messageBody = mEditTextMessage.getText().toString();

                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                    new Pushnotification().execute();

                } else {

                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");
            }
        });

        asyncFetch = new Receivemessage();
        asyncFetch.execute();
        callAsynchronousTask();

    }

    public void hideKeyBoard() {
// Check if no view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void sendTextMessage(String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "input_personal_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("from_user_id", sp.getString(Constant.rid, ""));
                    jo.put("to_user_id", receiver_id);
                    jo.put("message", messageBody);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");


                        value++;
                        category = new DataModel();

                        category.setSenderID(sp.getString(Constant.rid, ""));
                        category.setRecieverID("" + receiver_id);
                        category.setMsg("" + messageBody);
                        arrayList.add(category);
                        adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect Charactor", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            new Receivemessage().execute();
                            adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);// 1 second

    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        stopTimer();
        finish();
    }*/

    private void stopTimer() {

        asyncFetch.cancel(true);

        if (asyncFetch.getStatus().equals(AsyncTask.Status.RUNNING)) {
            asyncFetch.cancel(true);
        }

        if (timerTask != null) {
            timerTask.cancel();

        } else {
            Log.e("notcancle", "notcancle");

        }
    }

    private String capitalize(final String text) {

        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }

    @Override
    public void onBackPressed() {


        if (asyncFetch != null && asyncFetch.getStatus() == AsyncTask.Status.RUNNING) {
            asyncFetch.cancel(true);


        } else if (intent.hasExtra("list")) {
            Intent i = new Intent(Personalchat_activity.this, Chat_Tabbar.class);
            startActivity(i);
            stopTimer();
            asyncFetch.cancel(true);
            Log.e("back1", "back1");
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else if (intent.hasExtra("searchuser")) {

            stopTimer();
            asyncFetch.cancel(true);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            SearchBookFragment addPriceOfBookFragment = new SearchBookFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("BOOLEAN_VALUE", false);
            addPriceOfBookFragment.setArguments(bundle);
            ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
            ft.commit();


        } else if (intent.hasExtra("msg")) {

            stopTimer();
            asyncFetch.cancel(true);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            SellTextFragment addPriceOfBookFragment = new SellTextFragment();
            Bundle bundle = new Bundle();
            //bundle.putBoolean("BOOLEAN_VALUE",false);
            addPriceOfBookFragment.setArguments(bundle);
            ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
            ft.commit();
        } else {
            Intent i = new Intent(Personalchat_activity.this, Campuschat_activity.class);
            startActivity(i);
            stopTimer();
            asyncFetch.cancel(true);
            Log.e("back2", "back2");
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();


        }
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_personal_chat.php";
            System.out.println("the url is :" + serverUrl);

            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();
                jo.put("from_user_id", sp.getString(Constant.rid, ""));
                jo.put("to_user_id", receiver_id);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader("Content-Type", "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);


                try {

                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();

                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setRecieverID("" + success.getString("ReceiverId"));
                                // category.setReciever_name("" + success.getString("Name"));
                                category.setMsg("" + success.getString("Message"));
                                category.setBookName("" + success.getString("BookName"));
                                category.setBookImage("" + success.getString("BookImage"));
                                category.setStatus("" + success.getString("Status"));


                                String bnm = category.getBookName();
                                String bim = category.getBookImage();
                                String st = category.getStatus();


                                if (st.equals("Accept")) {
                                    toolbar1.setVisibility(View.VISIBLE);
                                    imageBook.setVisibility(View.VISIBLE);
                                    offer.setTextColor(getResources().getColor(R.color.lime_lite));
                                    offer.setText("Offer Accepted:");

                                    //Log.e("offer accepted","offer accepted");

                                } else if (st.equals("Reject")) {
                                    toolbar1.setVisibility(View.VISIBLE);
                                    imageBook.setVisibility(View.VISIBLE);
                                    offer.setText("Offer Rejected:");
                                    offer.setTextColor(getResources().getColor(R.color.red));

                                    //Log.e("offer rejected","offer rejected");

                                } else {
                                    toolbar1.setVisibility(View.GONE);
                                    imageBook.setVisibility(View.GONE);

                                }

                                if (bnm.length() > 19) {

                                    bnm = bnm.substring(0, 18) + "...";

                                    bookname.setText(bnm);
                                } else {

                                    bookname.setText(bnm); //Dont do any change

                                }


                                if (bim.equals("")) {
                                    Picasso.with(Personalchat_activity.this)
                                            .load(R.drawable.user1)
                                            .placeholder(R.drawable.user1)
                                            .into(imageBook);
                                } else if (bim.contains("https://")) {
                                    Picasso.with(Personalchat_activity.this)
                                            .load(bim)
                                            .placeholder(R.drawable.user1)
                                            .into(imageBook);
                                } else {
                                    Picasso.with(Personalchat_activity.this)
                                            .load(Constant.MAIN_URL + bim)
                                            .placeholder(R.drawable.user1)
                                            .into(imageBook);
                                }

                                arrayList_count.add(category);

                                if (arrayList_count != arrayList) {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                                    listView.setAdapter(adapter);

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }

                            }

                        }
                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "personalchat_push.php";
            System.out.println("the url is personalchat_push :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("from_user_id", sp.getString(Constant.rid, ""));
                jo.put("to_user_id", receiver_id);
                jo.put("message", messageBody);
                jo.put("sender_name", charnm);
                jo.put("type", "personal");

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {


                    if (success.getString("success").equals("1")) {

                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            int layoutResource = 0;

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username = sp.getString(Constant.username, "");
            String rusername = getDataAdapter1.getReciever_name();
            String chars = capitalize(username);
            String charr = capitalize(sendername);

            //String rchars = capitalize(rusername);

            String rid = sp.getString(Constant.rid, "");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);
            holder.tv_sender_name.setVisibility(View.VISIBLE);
            holder.tv_receiver_name.setVisibility(View.VISIBLE);


            if (rid.equals(getDataAdapter1.getSenderID())) {
                String name = getDataAdapter1.getMsg();
                String charsf = capitalize(name);
                //name=messageBody;
                holder.tv_sender.setText(charsf);
                holder.tv_sender_name.setText(" :" + chars);
                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);

            } else {
                String name = getDataAdapter1.getMsg();
                String charsr = capitalize(name);
                holder.tv_receiver_name.setText(charr + ": ");
                holder.tv_receiver.setText(charsr);
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);


            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tv_sender, tv_receiver, txt_show_date_sender, txt_show_date_reciever;
            LinearLayout linear_Left;
            LinearLayout relative_right;
            CircleImageView my_image_view;
            TextView tv_receiver_name, tv_sender_name;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
                tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);

            }
        }
    }
}

