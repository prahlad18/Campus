package app.livecampus.Utils;

public class Constant {

    public static String MAIN_URL = "http://www.smartbaba.in/liveatcampus/api/";
//    public static String MAIN_URL = "http://smartbaba.in/liveatcampus/api/";

    public static String profile = "profile";
    public static String username = "username";
    public static String campus_id = "campus_id";
    public static String StrCampus_name = "strCampus_name";
    public static String Strmajorname = "Strmajorname";
    public static String Stryeare = "Stryeare";
    public static String CHKBUY = "chkBuy";
    public static String Book_id = "book_id";
    public static String SEARCHTEX = "searchtext";
    public static String COURSE_ID = "course_id";
    public static String IMAGE = "image";
    public static String TITLE = "title";
    public static String ISBN = "isbn";
    public static String PRI = "pri";
    public static String RAT = "rat";
    public static String BLUB = "blub";
    public static String IMAGECM1 = "imagecm1";
    public static String IMAGECM2 = "imagecm2";
    public static String IMAGECM3 = "imagecm3";
    public static String IMAGEDECOD = "imagedecod";
    public static String IMAGEDECOD1 = "imagedecod1";
    public static String IMAGEDECOD2 = "imagedecod2";
    public static String BLUBSAVE = "blubsave";
    public static String BOOK_I = "book_i";
    public static String isbn = "isbn";
    public static String name = "name";
    public static String email = "email";
    public static String password = "password";
    public static String Prefrence = "Prefrence";
    public static String yes = "no";
    public static String PRICE = "price";
    public static String DATE = "date";
    public static String IMAGES = "images";
    public static String RATING = "rating";
    public static String IMAGES1 = "images1";
    public static String IMAGES2 = "images2";
    public static String IMAGES3 = "images3";
    public static String rid = "rid";
    public static String scan = "scan";
    public static String uid = "uid";
    public static String mid = "mid";
    public static String uni_ID = "uni_ID";
    public static String campus_name = "campus_name";
    public static String campus_image = "campus_image";
    public static String selected = "selected";
    public static String year_id = "x4";
    public static String CNAME1 = "cNAME1";
    public static String cISBN1 = "cISBN1";
    public static String IMAGE1 = "image1";
    public static String COURSENAME = "coursename";
    public static String COURSENAME1 = "coursename1";
    public static String COURSENAME2 = "coursename2";
    public static String COURSENAME3 = "coursename3";

    public static String BASE1 = "yes";
    public static String BASE2 = "yes";
    public static String BASE3 = "yes";

    public static String CNAME2 = "cNAME2";
    public static String cISBN2 = "cISBN2";
    public static String IMAGE2 = "image2";
    public static String CNAME3 = "cNAME3";
    public static String cISBN3 = "cISBN3";
    public static String IMAGE3 = "image3";

}
