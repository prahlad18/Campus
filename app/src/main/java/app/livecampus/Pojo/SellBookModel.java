package app.livecampus.Pojo;

/**
 * Created by isquare on 20-Jan-18.
 */

public class SellBookModel {

    private int id;
    private String title;
    private String image;
    private String price;
    private String isbn;
    private String course_id;
    private String date;
    private String images;
    private String rating;
    private String rid;
    private String Coursename;
    private String Buyername;
    private String Offercount;
    private String Finalprice;

    public SellBookModel() {

    }

    public SellBookModel(int id, String title, String image, String price, String isbn, String date, String images, String rating) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.price = price;
        this.isbn = isbn;
        this.date = date;
        this.images = images;
        this.rating = rating;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getCoursename() {
        return Coursename;
    }

    public void setCoursename(String coursename) {
        Coursename = coursename;
    }

    public String getBuyername() {
        return Buyername;
    }

    public void setBuyername(String buyername) {
        Buyername = buyername;
    }

    public String getOffercount() {
        return Offercount;
    }

    public void setOffercount(String offercount) {
        Offercount = offercount;
    }

    public String getFinalprice() {
        return Finalprice;
    }

    public void setFinalprice(String finalprice) {
        Finalprice = finalprice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
