package app.livecampus.Pojo;

public class BeanPage {


    private final int image;
    private final String name;
    private final String mname;
    private final int chatimage;
    private final String msg;


    public BeanPage(int image, String name, String mname, String msg, int chatimage) {
        this.image = image;
        this.name = name;
        this.mname = mname;
        this.msg = msg;
        this.chatimage = chatimage;


    }

    public int getChatimage() {
        return chatimage;
    }

    public String getMsg() {
        return msg;
    }

    public String getMname() {
        return mname;
    }

    public int getImage() {
        return image;
    }

    public String getName() {
        return name;
    }


}
