package app.livecampus.Pojo;

/**
 * Created by isquare on 20-Jan-18.
 */

public class SellBookModel_new {

    public String id;
    public String rid;
    public String recever_id;
    public String BookImage;
    public String BookNane;
    public String Bookname;
    public String Username;
    public String Univervity;
    public String Major;
    public String Blub;
    public String Images1;
    public String Images2;
    public String Images3;
    public String Sellerprice;
    public String status;
    public String Buyerid;
    public String Sellerid;
    public String Year;
    public String Userimage;
    private String price;
    private String isbn;
    private String date;
    private String rating;
    private String title;
    private String image;
    private String course_id;

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getBlub() {
        return Blub;
    }

    public void setBlub(String blub) {
        Blub = blub;
    }

    public String getImages1() {
        return Images1;
    }

    public void setImages1(String images1) {
        Images1 = images1;
    }

    public String getImages2() {
        return Images2;
    }

    public void setImages2(String images2) {
        Images2 = images2;
    }

    public String getImages3() {
        return Images3;
    }

    public void setImages3(String images3) {
        Images3 = images3;
    }

    public String getSellerprice() {
        return Sellerprice;
    }

    public void setSellerprice(String sellerprice) {
        Sellerprice = sellerprice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBuyerid() {
        return Buyerid;
    }

    public void setBuyerid(String buyerid) {
        Buyerid = buyerid;
    }

    public String getSellerid() {
        return Sellerid;
    }

    public void setSellerid(String sellerid) {
        Sellerid = sellerid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getBookname() {
        return Bookname;
    }

    public void setBookname(String bookname) {
        Bookname = bookname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserimage() {
        return Userimage;
    }

    public void setUserimage(String userimage) {
        Userimage = userimage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecever_id() {
        return recever_id;
    }

    public void setRecever_id(String recever_id) {
        this.recever_id = recever_id;
    }

    public String getBookImage() {
        return BookImage;
    }

    public void setBookImage(String bookImage) {
        BookImage = bookImage;
    }

    public String getBookNane() {
        return BookNane;
    }

    public void setBookNane(String bookNane) {
        BookNane = bookNane;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getUnivervity() {
        return Univervity;
    }

    public void setUnivervity(String univervity) {
        Univervity = univervity;
    }

    public String getMajor() {
        return Major;
    }

    public void setMajor(String major) {
        Major = major;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }


}
