package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import app.livecampus.Activity.Barcode_Activity;
import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.SellBookModel;
import app.livecampus.Pojo.Univercity_Model;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.adapter.RecyclerViewAdapter;


public class Change_ISBN_Course extends Fragment {

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public RecyclerView image;
    public Boolean myBooleanVariable = true;
    public ImageView img1, img2, img3, imgnext, imgbck;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    JSONObject catObj;
    JSONArray jsonArrayCategory;
    SellBookModel category;
    SharedPreferences app_sharedprefrence;
    ArrayList<SellBookModel> listOfBook = new ArrayList<>();
    RecyclerViewAdapter adapter;
    Point p;
    String course_id, price, rating;
    String selectedDate;
    ArrayList<String> imagesEncodedList;
    Calendar myCalendar = Calendar.getInstance();
    ProgressDialog pDialog;
    ArrayAdapter<String> spinnerArrayUnivercity;
    ArrayAdapter<String> spinnerMajor;
    ArrayAdapter<String> spinnerYear;
    ArrayAdapter<String> spinnerArrayCampus;
    EditText editCn1, editCn2, editCn3, editCn4;
    Button barcodscn;
    TextView txtCname1, txtCname2, txtCname3, txtCname4, back;
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private JSONObject success;
    private ArrayList<Univercity_Model> arrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.addandeditetextbook, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        listOfBook = new ArrayList<>();
        arrayList = new ArrayList<>();
        setHasOptionsMenu(true);
    /*    sp = getActivity().getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        pref = getActivity().getSharedPreferences("contact", Context.MODE_PRIVATE);*/
        app_sharedprefrence = getActivity().getSharedPreferences("livecampus", Context.MODE_PRIVATE);

        //textView = (TextView) convertView.findViewById(R.id.text);
        getActivity().setTitle("Add & Edit Textbbok");


        image = (RecyclerView) convertView.findViewById(R.id.image);
        imgnext = (ImageView) convertView.findViewById(R.id.imgnext);
        imgbck = (ImageView) convertView.findViewById(R.id.imgbck);
        back = (TextView) convertView.findViewById(R.id.btnEdit);

      /*  final Bundle bundle = getArguments();
        String stredit = bundle.getString("TextBox");
*/




      /*  editCn1=(EditText)convertView.findViewById( R.id.editcn1 );
        editCn2=(EditText)convertView.findViewById( R.id.editcn2 );
        editCn3=(EditText)convertView.findViewById( R.id.editcn3 );
        editCn4=(EditText)convertView.findViewById( R.id.editcn4 );

        txtCname1=(TextView)convertView.findViewById( R.id.txtCname1 );
        txtCname2=(TextView)convertView.findViewById( R.id.txtCname2 );
        txtCname3=(TextView)convertView.findViewById( R.id.txtCname3 );
        txtCname4=(TextView)convertView.findViewById( R.id.txtCname4 );*/
        barcodscn = (Button) convertView.findViewById(R.id.barcode_btn);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), HomeActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        image.setHasFixedSize(true);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        image.setLayoutManager(horizontalLayoutManagaer);


        new Get_Gallery().execute();

        imgnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //image.smoothScrollToPosition(adapter.getItemCount());
                image.smoothScrollBy(100, 0);
            }
        });
        imgbck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.smoothScrollBy(-100, 0);
            }
        });
        barcodscn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Barcode_Activity.class);
                i.putExtra("list", "list");
                i.putExtra("my_boolean_key", false);
                startActivity(i);
                getActivity().finish();


            }
        });


        return convertView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    @Override
    public void onResume() {
        super.onResume();
      /*  arrayList.clear();
        categoryArrayList.clear();
        new PostDataTOServer().execute();*/

    }

    private class Get_Gallery extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_course.php";

            System.out.println("the get course url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                Log.i("user id", app_sharedprefrence.getString("rid", ""));
                jo.put("rid", app_sharedprefrence.getString("rid", ""));

                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");

                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel();
                        catObj = jsonArrayCategory.getJSONObject(i);
                        category.setCourse_id(catObj.getString("course_id"));
                        category.setTitle(catObj.getString(Constant.CNAME1));
                        category.setIsbn(catObj.getString(Constant.cISBN1));
                        category.setImage(catObj.getString(Constant.IMAGE1));
                        category.setRating(catObj.getString("rating"));
                        category.setCoursename(catObj.getString("CourseName"));
                        Log.i("rating", catObj.getString("rating"));
                        if (catObj.getString(Constant.PRICE).equals("")) {
                            category.setPrice("0");
                        } else {
                            category.setPrice((catObj.getString(Constant.PRICE)));
                        }


//                            category.setDate(catObj.getString(Constant.DATE));
                        listOfBook.add(category);
                    }
                    adapter = new RecyclerViewAdapter(getActivity(), listOfBook);
                    image.setAdapter(adapter);
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(getActivity(), "Record not found", Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                pDialog.dismiss();
                System.out.println("the title exeption is :" + e);
                e.printStackTrace();
            }
        }
    }
}