package app.livecampus.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.R;
import app.livecampus.Utils.Constant;


public class AddPriceOfBookFragment extends AppCompatActivity implements View.OnClickListener {

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int PICK_IMAGE_GALLERY = 2;
    public Button post;
    public int count = 0;
    public Button done;
    public Dialog dialog;
    public String imagePath, imagePath2, imagePath3;
    public String img1, img2, img3;
    public int i = 0;
    EditText entprc, txt_message;
    RatingBar ratingBar;
    Point p;
    String yourDate;
    String blubvalue, book_i, blb;
    String selectedDate;
    ImageView image1, image2, image3;
    String ba1, blur;
    int flag = 1;
    String mCurrentPhotoPath;
    java.util.Date date = null;
    String strStart = null;
    Bitmap bm1 = null, bm2 = null, bm3 = null;
    SharedPreferences sp;
    String Str1, Str2, Str3, temp, temp1, temp2;
    ProgressDialog pDialog;
    Calendar myCalendar = Calendar.getInstance();
    JSONObject success;
    String booksearch, booksea;
    String dateMy = "";
    String course_id, price, rating;
    String images[];
    ArrayList<String> imagesEncodedList;
    Context context;
    Button btn_Home;
    private TextView txtHome, txtSearch, txtISBN, back, txt_sell, txtBookTitle, rttext;
    private ImageView imgCamera, imgBlurb, imgCalender, imgBook, img;
    private int PICK_IMAGE = 1;
    private Bitmap bitmap1, bitmap2, bitmap3;
    private File destination1 = null, destination2 = null, destination3 = null;
    private InputStream inputStreamImg;
    private int PICK_IMAGE_CAMERA, PICK_IMAGE_CAMERA1, PICK_IMAGE_CAMERA2;
    private ImageView imageView, img_Back;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_price_of_book);
        getSupportActionBar().hide();
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Rakesh/";
        File newdir = new File(dir);
        imagesEncodedList = new ArrayList<String>();
        this.context = AddPriceOfBookFragment.this;

       /* ByteArrayOutputStream stream = new ByteArrayOutputStream();//rmd
        byte imageInByte[] = stream.toByteArray();
        imagePath = Base64.encodeToString(imageInByte,Base64.DEFAULT);*/
        sp = AddPriceOfBookFragment.this.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

        blubvalue = sp.getString(Constant.BLUBSAVE, "");

        newdir.mkdirs();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @SuppressLint("WrongConstant")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                updateLabel();
            }


        };
        txtHome = (TextView) findViewById(R.id.txt_sell_home);
        txtHome.setText(R.string.home);
        img_Back = findViewById(R.id.img_Back);
        btn_Home = findViewById(R.id.btn_Home);

        btn_Home.setOnClickListener(this);
        img_Back.setOnClickListener(this);

        txtSearch = (TextView) findViewById(R.id.txt_sell_search);
        txtSearch.setText(R.string.search);

        txtBookTitle = (TextView) findViewById(R.id.txt_book_title);

        back = (TextView) findViewById(R.id.btnEdit);

        txtISBN = (TextView) findViewById(R.id.txt_book_isbn);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        //simpleRatingBar = (RatingBar)findViewById(R.id.simpleRatingBar); // initiate a rating bar


        imgCamera = (ImageView) findViewById(R.id.image_camera);
        txt_sell = (TextView) findViewById(R.id.txt_sell);


        rttext = (TextView) findViewById(R.id.rttext);
        imgBlurb = (ImageView) findViewById(R.id.image_blurb);
        entprc = (EditText) findViewById(R.id.entprc);
        //img = (ImageView)findViewById(R.id.imageView1);
        imgCalender = (ImageView) findViewById(R.id.image_calender);
        imgBook = (ImageView) findViewById(R.id.image_book);
        rttext.setVisibility(View.GONE);

        Intent bundle = getIntent();
        course_id = bundle.getStringExtra("course_id");
        String strImage = bundle.getStringExtra("image");
        String title = bundle.getStringExtra("title");
        String ISBN_NO = bundle.getStringExtra("ISBN");
        String strprice = bundle.getStringExtra("price");
        String strrating = bundle.getStringExtra("rating");
        booksearch = bundle.getStringExtra("booksearch");
        booksea = bundle.getStringExtra("booksea");

//        Log.e("star",strrating);

        if (strrating.equals("")) {

            ratingBar.setRating(Float.parseFloat(""));

        } else {
            ratingBar.setRating(Float.parseFloat("" + strrating));

        }
        if (strprice.equals("0")) {

            entprc.setText("");

        } else {
            entprc.setText(strprice);

        }


        Log.i("Title in add price", title + " " + course_id);
        txtISBN.setText("ISBN # " + ISBN_NO);

        if (title.length() > 10) {

            title = title.substring(0, 9) + "...";

            txtBookTitle.setText(title);
        } else {

            txtBookTitle.setText(title); //Dont do any change

        }
        // txtBookTitle.setText(title);

        Log.e("bundle image", strImage);
        {
            Picasso.with(context)
                    .load(strImage)
                    .placeholder(R.drawable.load)
                    .into(imgBook);
        }
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rate, boolean b) {
                rttext.setVisibility(View.VISIBLE);

                if ((int) rate == 1) {
                    ratingBar.setNumStars(5);
                    ratingBar.setProgress(1);
                    ratingBar.setRating(1);

                    LayerDrawable layerDrawable = (LayerDrawable) ratingBar.getProgressDrawable();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), ContextCompat.getColor(AddPriceOfBookFragment.this, R.color.yellowx));
                        DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)), ContextCompat.getColor(AddPriceOfBookFragment.this, R.color.yellowx));

                    } else {
                        layerDrawable.getDrawable(2).setColorFilter(ContextCompat.getColor(AddPriceOfBookFragment.this, R.color.yellowx), PorterDuff.Mode.SRC_ATOP);
                    }
                    rttext.setText("OK");
                    Log.i("rating", "1");
                } else if ((int) rate == 2) {
                    rttext.setText("Fair");
                    Log.i("rating", "2");
                } else if ((int) rate == 3) {
                    rttext.setText("Good");
                    Log.i("rating", "3");
                } else if ((int) rate == 4) {
                    rttext.setText("Excellent");
                    Log.i("rating", "4");
                } else if ((int) rate == 5) {
                    rttext.setText("Mint");
                    Log.i("rating", "5");
                }
            }
        });
        imgCalender.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub

                DatePickerDialog mDate = new DatePickerDialog(AddPriceOfBookFragment.this, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDate.show();
      /*          new DatePickerDialog(AddPriceOfBookFragment.this,
                        date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                date.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);*/


            }
        });
        txtHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Menu_Activity menu = new Menu_Activity();
                FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, menu);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SellTextFragment());
                ft.addToBackStack(null);
                ft.commit();*/

               /* Boolean getValue= getArguments().getBoolean("BOOLEAN_VALUE");
                if(getValue)
                {
                    FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    SearchBookFragment addPriceOfBookFragment=new SearchBookFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("BOOLEAN_VALUE",false);
                    addPriceOfBookFragment.setArguments(bundle);
                    ft.replace(R.id.container,addPriceOfBookFragment,"Sell");
                    ft.commit();


                }
                else
                {
                    FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.container, new SellTextFragment());
                    ft.addToBackStack(null);
                    ft.commit();
                }
*/
            }
        });

        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SearchBookFragment());
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        txt_sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Str1 = sp.getString(Constant.IMAGECM1, "");
                Str2 = sp.getString(Constant.IMAGECM2, "");
                Str3 = sp.getString(Constant.IMAGECM3, "");


                price = entprc.getText().toString().trim();


                rating = String.valueOf(ratingBar.getRating());
                if (selectedDate == null || entprc.getText().toString().trim().length() == 0) {


                    if (entprc.getText().toString().trim().length() == 0) {
                        Toast.makeText(AddPriceOfBookFragment.this, "Enter Price", Toast.LENGTH_SHORT).show();


                    }
                    Toast.makeText(AddPriceOfBookFragment.this, "Select Date", Toast.LENGTH_SHORT).show();


                } else {


                    if (img1 == null) {
                        Toast.makeText(AddPriceOfBookFragment.this, "Please upload textbook snaps.", Toast.LENGTH_SHORT).show();
                    } else {


                        try {
                            blur = txt_message.getText().toString().trim();


                            //Toast.makeText(AddPriceOfBookFragment.this,""+myValue,Toast.LENGTH_SHORT).show();
                            sp.edit().putString(Constant.BLUBSAVE, blur).commit();
                            new PostDataTOServer().execute();


                        } catch (Exception e) {
                            Toast toast = Toast.makeText(AddPriceOfBookFragment.this, "Please enter blub value", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                }



                /*if(imagesEncodedList.size()<0)
                {
                    Toast.makeText(AddPriceOfBookFragment.this,"Please select any image for book from gallery",Toast.LENGTH_LONG).show();
                }
                else
                {
                    new PostDataTOServer().execute();
                }
*/
            }
        });

        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertbox();

            }
        });
        imgBlurb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int[] location = new int[2];
                // Get the x, y location and store it in the location[] array
                // location[0] = x, location[1] = y.
                txtISBN.getLocationOnScreen(location);

                InputMethodManager imm = (InputMethodManager) AddPriceOfBookFragment.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                //Initialize the Point with x, and y positions
                p = new Point();
                p.x = location[0];
                p.y = location[1];
                if (p != null) {
                    showPopup(AddPriceOfBookFragment.this, p);
                }
            }
        });


    }

    private void showPopup(final Activity context, Point p) {
        int popupWidth = 550;
        int popupHeight = 550;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.addbook_blub, viewGroup);

        txt_message = (EditText) layout.findViewById(R.id.textView2);
        post = (Button) layout.findViewById(R.id.post);


        //blur=txt_message.getText().toString().trim();

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = -80;
        int OFFSET_Y = -270;


        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                blur = txt_message.getText().toString().trim();

                sp.edit().putString(Constant.BLUBSAVE, blur).commit();

                sp.edit().putString(Constant.BOOK_I, course_id).commit();

                //sp.edit().putString(Constant.BLUBVALUE,blur).commit();
            }
        });


        book_i = sp.getString(Constant.BOOK_I, "");
        blb = sp.getString(Constant.BLUBSAVE, "");

        if (course_id.equals(book_i)) {

            txt_message.setText(blb);

        } else {

            txt_message.setText("");


        }


        // Displaying the popup at the specified location, + offsets.
        Log.i("Pixels", p.x + " " + p.y);
        popup.showAtLocation(layout, Gravity.TOP, p.x + OFFSET_X, p.y + OFFSET_Y);
    }

    private void alertbox() {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popupcamera);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView cameraopen = (ImageView) dialog.findViewById(R.id.cameraopen);
        image1 = (ImageView) dialog.findViewById(R.id.camImage1);
        done = (Button) dialog.findViewById(R.id.done);
        image2 = (ImageView) dialog.findViewById(R.id.camImage2);
        image3 = (ImageView) dialog.findViewById(R.id.camImage3);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


        String image = sp.getString(Constant.IMAGEDECOD, "");


        if (image1.equals(image)) {

            Log.e("imasss1", "heloo");

        } else {

            Picasso.with(context)
                    .load(R.drawable.bookimagedefult)
                    .placeholder(R.drawable.bookimagedefult)
                    .into(image1);
        }


        if (image.equals("")) {

            Picasso.with(context)
                    .load(R.drawable.bookimagedefult)
                    .placeholder(R.drawable.bookimagedefult)
                    .into(image1);
        } else {

            image1.setImageBitmap(bm1);

        }

        String image1 = sp.getString(Constant.IMAGEDECOD1, "");


        if (image2.equals(image1)) {

            Log.e("imasss1", "heloo");

        } else {

            Picasso.with(context)
                    .load(R.drawable.bookimagedefult)
                    .placeholder(R.drawable.bookimagedefult)
                    .into(image2);
        }

        if (image1.equals("")) {

            Picasso.with(context)
                    .load(R.drawable.bookimagedefult)
                    .placeholder(R.drawable.bookimagedefult)
                    .into(image2);
        } else {
            image2.setImageBitmap(bm2);

        }

        String image2 = sp.getString(Constant.IMAGEDECOD2, "");


        if (image3.equals(image2)) {

            Log.e("imasss1", "heloo");

        } else {

            Picasso.with(context)
                    .load(R.drawable.bookimagedefult)
                    .placeholder(R.drawable.bookimagedefult)
                    .into(image3);
        }

        if (image2.equals("")) {

            Picasso.with(context)
                    .load(R.drawable.bookimagedefult)
                    .placeholder(R.drawable.bookimagedefult)
                    .into(image3);
        } else {
            image3.setImageBitmap(bm3);

        }


        cameraopen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                i++;

                if (i == 1) {
//                    i=i+1;

                    Log.e("im1", "im1");

                    PICK_IMAGE_CAMERA = 1;
                    PICK_IMAGE_CAMERA1 = 0;
                    PICK_IMAGE_CAMERA2 = 0;


                    selectImage();

                } else if (i == 2) {
//                    i=i+1;

                    Log.e("im2", "im2");

                    PICK_IMAGE_CAMERA1 = 1;
                    PICK_IMAGE_CAMERA = 0;
                    PICK_IMAGE_CAMERA2 = 0;
                    selectImage();

                } else if (i == 3) {
//                    i=i+1;

                    Log.e("im3", "im3");

                    PICK_IMAGE_CAMERA2 = 1;
                    PICK_IMAGE_CAMERA1 = 0;
                    PICK_IMAGE_CAMERA = 0;


                    selectImage();
                } else if (i == 4) {
//                    i=i+1;

                    Toast.makeText(AddPriceOfBookFragment.this, "You have selected three image", Toast.LENGTH_SHORT).show();


                }
            }
        });
        dialog.show();

    }

    private void selectImage() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);

    }

    @SuppressLint("ApplySharedPref")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (requestCode == PICK_IMAGE_CAMERA) {
        if (PICK_IMAGE_CAMERA == 1) {
            try {
                Uri selectedImage = data.getData();
                bitmap1 = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination1 = new File(Environment.getExternalStorageDirectory() + "/" +
                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination1.createNewFile();
                    fo = new FileOutputStream(destination1);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i = i++;
                byte imageInByte[] = bytes.toByteArray();

                imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);

                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);

                temp = String.valueOf(BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length));


                if (img1 == null) {
                    img1 = imagePath;
                    Log.e("cm1", img1);

                    sp.edit().putString(Constant.IMAGECM1, imagePath).commit();


                    String Str1 = sp.getString(Constant.IMAGECM1, "");

                }

                //imgPath = destination.getAbsolutePath();
                image1.setImageBitmap(bm1);

                sp.edit().putString(Constant.IMAGEDECOD, "" + temp).commit();
                Log.e("im1", String.valueOf(temp));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /* if (requestCode == PICK_IMAGE_CAMERA1) {*/
        if (PICK_IMAGE_CAMERA1 == 1) {
            try {
                Uri selectedImage = data.getData();
                bitmap2 = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination2 = new File(Environment.getExternalStorageDirectory() + "/" +
                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination2.createNewFile();
                    fo = new FileOutputStream(destination2);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i = i++;

                byte imageInByte[] = bytes.toByteArray();


                imagePath2 = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm2 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);
                temp1 = String.valueOf(BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length));


                if (img2 == null) {
                    img2 = imagePath2;
                    Log.e("cm2", img2);
                    sp.edit().putString(Constant.IMAGECM2, imagePath2).commit();

                    String Str2 = sp.getString(Constant.IMAGECM2, "");

                }
                //Log.e("image2",img2);
                //imgPath = destination.getAbsolutePath();
                image2.setImageBitmap(bm2);

                sp.edit().putString(Constant.IMAGEDECOD1, "" + temp1).commit();
                Log.e("im2", String.valueOf(bm2));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        if (requestCode == PICK_IMAGE_CAMERA2) {
        if (PICK_IMAGE_CAMERA2 == 1) {
            try {
                Uri selectedImage = data.getData();
                bitmap3 = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination3 = new File(Environment.getExternalStorageDirectory() + "/" +
                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination3.createNewFile();
                    fo = new FileOutputStream(destination3);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i = i++;

                byte imageInByte[] = bytes.toByteArray();


                imagePath3 = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm3 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);
                temp2 = String.valueOf(BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length));


                if (img3 == null) {
                    img3 = imagePath3;
                    Log.e("cm3", img3);
                    sp.edit().putString(Constant.IMAGECM3, imagePath3).commit();

                    String Str3 = sp.getString(Constant.IMAGECM3, "");


                }
                //Log.e("image3",img3);
                //imgPath = destination.getAbsolutePath();
                image3.setImageBitmap(bm3);

                sp.edit().putString(Constant.IMAGEDECOD2, "" + temp2).commit();

                Log.e("im3", String.valueOf(bm3));


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Bitmap bm = null;
            Bitmap bm1 = null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(AddPriceOfBookFragment.this.getContentResolver(), data.getData());
                    Matrix matrix = new Matrix();
                    // matrix.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte imageInByte[] = stream.toByteArray();
                    imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                    bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // circleImageView.setImageBitmap(bm1);
        }
    }
//pp

    private void updateLabel() {

        String myFormat = "MMMM dd yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        selectedDate = sdf.format(myCalendar.getTime());
        Log.e("selected date is ", selectedDate);

    }


    //pp


    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();

        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Home:
                Intent i = new Intent(AddPriceOfBookFragment.this, HomeActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.img_Back:
                /*Intent intent = new Intent(AddPriceOfBookFragment.this, HomeActivity.class);
                startActivity(intent);*/
                finish();
                break;
        }
    }

    public class PostDataTOServer extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String serverUrl = Constant.MAIN_URL + "edit_course.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();

                jo.put("course_id", course_id);

                jo.put(Constant.PRICE, price);

                jo.put(Constant.IMAGES1, img1);
                jo.put(Constant.IMAGES2, img2);
                jo.put(Constant.IMAGES3, img3);
                jo.put(Constant.BLUB, blur);

                jo.put(Constant.DATE, selectedDate);

                jo.put(Constant.RATING, rating);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);

                    sp.edit().putString(Constant.DATE, selectedDate).commit();


                    /*Boolean getValue = getArguments().getBoolean("BOOLEAN_VALUE");
                    if (getValue) {
                        FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.container, new SearchBookFragment());
                        ft.addToBackStack(null);
                        ft.commit();


                    } else {
                        FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.container, new SellTextFragment());
                        ft.addToBackStack(null);
                        ft.commit();
                    }*/

                    /*FragmentManager fm = AddPriceOfBookFragment.this.getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.container, new SellTextFragment());
                    ft.addToBackStack(null);
                    ft.commit();*/
                    startActivity(new Intent(AddPriceOfBookFragment.this, HomeActivity.class));

                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(AddPriceOfBookFragment.this, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("the exx" + e);
            }
        }
    }


}
