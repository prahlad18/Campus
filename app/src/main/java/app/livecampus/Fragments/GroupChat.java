package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Activity.ActivityGroupChat;
import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.GroupModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 6/27/18.
 */

public class GroupChat extends Fragment {

    public TextView txtblub, blubtext, edate, tv_courseName, tvCram, txt_NoData;
    public ImageView img_Bulb, img_Calendar;
    public CircleImageView img_book;
    public String bookimage1, blub, date, course, strCram;
    RecyclerView listView;
    GridLayoutManager mLayoutManager;
    String Prefrence = "Prefrence";
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    SharedPreferences sp;
    ArrayList<GroupModel> arrayList = new ArrayList<>();
    String rid, groupID;
    ProgressDialog pDialog;
    private JSONObject success;
    private Button mButtonSend;
    LinearLayout toolbar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.groupchat, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        sp = getActivity().getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        setHasOptionsMenu(true);
        toolbar =  convertView.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        listView = (RecyclerView) convertView.findViewById(R.id.recycler_view);
        txt_NoData = (TextView) convertView.findViewById(R.id.txt_NoData);

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);
//        groupID=sp.getString("groupid","");


        new Receivemessage().execute();

        return convertView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "invite_request.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("status", "Show");
                jo.put("rid", sp.getString(Constant.rid, ""));//userid

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                if (pDialog.isShowing())
                    pDialog.dismiss();


                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("post");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            GroupModel category = new GroupModel();

                            success = jsonArrayCategory.getJSONObject(i);
                            category.setGroupID("" + success.getString("group_id"));
                            category.setBookImage("" + success.getString("image1"));
                            category.setGruopName("" + success.getString("group_name"));
                            category.setJoin_status("" + success.getString("status")); //pp
                            category.setStatus("" + success.getString("group_type")); //pp
                            category.setDate("" + success.getString("group_date")); //pp
                            category.setBulb("" + success.getString("group_blub")); //pp

                            if (success.getString("status").equalsIgnoreCase("1")) {
                                arrayList.add(category);
                            }

                        }
                        pDialog.dismiss();

                        adapter = new RecyclerViewAdapter_chat(getActivity(), arrayList);
                        listView.setAdapter(adapter);

                    } else if (success.getString("success").equals("0")) {
                        txt_NoData.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        pDialog.dismiss();

                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                    pDialog.dismiss();

                }
            }

        }
    }

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<GroupModel> getDataAdapter;
        SharedPreferences sp;
        String buyerid;
        String strGroupName, strGroupID;

        public RecyclerViewAdapter_chat(Context context, ArrayList<GroupModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter_chat.ViewHolder holder, final int position) {

            GroupModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String strProfile = getDataAdapter1.getBookImage();
            holder.mGroupNameTv.setText("" + getDataAdapter1.getGruopName());
            holder.mGroupBlubTv.setText("" + getDataAdapter1.getBulb());
            holder.mGroupStatusTv.setText("" + getDataAdapter1.getStatus());
            holder.mGroupDateTv.setText("" + getDataAdapter1.getDate());
            holder.mGroupBlubTv.setText("" + getDataAdapter1.getBulb());


            Log.e("if part", " status is " + getDataAdapter1.getJoin_status());
            holder.itemView.setVisibility(View.VISIBLE);

            if (getDataAdapter1.getStatus().equalsIgnoreCase("Exam")) {
                holder.linearColor.setBackgroundColor(getResources().getColor(R.color.red));
            } else {
                holder.linearColor.setBackgroundColor(getResources().getColor(R.color.lightblue));

            }


            if (strProfile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.main_ic)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.mGroupImage);
            } else if (strProfile.contains("https://")) {
                Picasso.with(context)
                        .load(strProfile)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.mGroupImage);
            } else {
                Picasso.with(context)
                        .load(Constant.MAIN_URL + strProfile)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.mGroupImage);
            }


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView mGroupImage;
            private TextView mGroupNameTv;
            private TextView mGroupBlubTv;
            private TextView mGroupStatusTv;
            private TextView mGroupDateTv;
            private Button mBtnAccept;
            private LinearLayout linearColor, linearMain;

            public ViewHolder(View itemView) {

                super(itemView);
                mGroupImage = (ImageView) itemView.findViewById(R.id.groupImage);
                mGroupNameTv = (TextView) itemView.findViewById(R.id.tv_groupName);
                mGroupBlubTv = (TextView) itemView.findViewById(R.id.tv_groupBlub);
                mGroupStatusTv = (TextView) itemView.findViewById(R.id.tv_groupStatus);
                mGroupDateTv = (TextView) itemView.findViewById(R.id.tv_groupDate);
                mBtnAccept = (Button) itemView.findViewById(R.id.btnAccept);
                linearColor = (LinearLayout) itemView.findViewById(R.id.linearColor);
                linearMain = (LinearLayout) itemView.findViewById(R.id.linearMain);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GroupModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());

                        buyerid = getDataAdapter1.getGroupID();
                        Log.e("getGroupID is", buyerid);

//                        new Clearenotification().execute();
                        Intent i = new Intent(context, ActivityGroupChat.class);//Msg_actvity_fromChat
                        i.putExtra("groupid", getDataAdapter1.getGroupID());
                        context.startActivity(i);
                        sp.edit().putString("groupImage", getDataAdapter1.getBookImage()).commit();
                        Log.e("send grop if", "is " + getDataAdapter1.getBookImage());
                        sp.edit().putString("groupid", getDataAdapter1.getGroupID()).commit();
                        getActivity().finish();
                    }
                });


            }
        }



    }


}

