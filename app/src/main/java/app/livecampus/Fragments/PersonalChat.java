package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.chat_activities.Personalchat_activity;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 4/13/18.
 */

public class PersonalChat extends Fragment {

    public CircleImageView bookimage;
    public boolean call = true;
    public String userID = "4";
    public Thread t;
    public TextView title;
    public TextView tv_sender, txt_NoData;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList = new ArrayList<>();
    ArrayList<DataModel> hs = new ArrayList<>();
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    Handler mHandler;
    String book_id, book_name, book_image, rid;
    int value = -1;
    private String messageBody, img;
    private JSONObject success;
    private ProgressDialog pDialog;
    private Button mButtonSend, btn_ok;
    private EditText mEditTextMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.chatnamelist, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(true);

        sp = getActivity().getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        listView = (RecyclerView) convertView.findViewById(R.id.listview);
        txt_NoData = (TextView) convertView.findViewById(R.id.txt_NoData);

        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);

        new Receivemessage().execute();
        return convertView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    private String capitalize(final String text) {

        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
//            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "personalchat_list.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("rid", sp.getString(Constant.rid, ""));//userid


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                if (pDialog.isShowing())
                    pDialog.dismiss();

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            category = new DataModel();


                            success = jsonArrayCategory.getJSONObject(i);
                            //category.setSenderID("" + success.getString("pic"));
                            category.setRecieverID("" + success.getString("R_Id"));
                            category.setReciever_name("" + success.getString("Name"));
                            category.setImage("" + success.getString("Image"));
                            category.setMsg("" + success.getString("Message"));
                            category.setNotcount("" + success.getString("Unread"));
                            category.setTime("" + success.getString("Time"));

                            arrayList.add(category);

                        }
                        Log.e("arraylist size", "" + arrayList.size());
                        adapter = new RecyclerViewAdapter_chat(getActivity(), arrayList);
                        listView.setAdapter(adapter);
                        pDialog.dismiss();

                    } else if (success.getString("success").equals("0")) {
                        pDialog.dismiss();


                        listView.setVisibility(View.GONE);
                        txt_NoData.setVisibility(View.VISIBLE);

                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                    pDialog.dismiss();

                }
            }

        }
    }

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;
        String buyerid;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.personalchatlist, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username = sp.getString(Constant.username, "");
            String rusername = getDataAdapter1.getReciever_name();
            String charsr = capitalize(rusername);
            String ruserImage = getDataAdapter1.getImage();
            String msg = getDataAdapter1.getMsg();
            String chars = capitalize(msg);

            String notification = getDataAdapter1.getNotcount();
            String time = getDataAdapter1.getTime();
            //buyerid=getDataAdapter1.getRecieverID();

            //String chars = capitalize(username);
            //String rchars = capitalize(rusername);
            holder.name.setText("" + charsr);
            holder.msg.setText("" + chars);
            holder.booknotif.setText("" + notification);
            holder.time.setText("" + time);

            if (notification.equalsIgnoreCase("0")) {

                holder.booknotif.setVisibility(View.INVISIBLE);


            } else {
                holder.booknotif.setVisibility(View.VISIBLE);


            }

            Log.e("userImage", ruserImage);
            if (ruserImage.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.user1)
                        .placeholder(R.drawable.user1)
                        .into(holder.circleImageView);
            } else {
                Picasso.with(context)
                        .load(Constant.MAIN_URL + ruserImage)
                        .placeholder(R.drawable.user1)
                        .into(holder.circleImageView);
            }


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            String userID = "4";
            TextView name, msg, booknotif, time;
            LinearLayout username_lnr1;
            CircleImageView circleImageView;

            public ViewHolder(View itemView) {

                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
                msg = (TextView) itemView.findViewById(R.id.msg);
                time = (TextView) itemView.findViewById(R.id.time);
                username_lnr1 = (LinearLayout) itemView.findViewById(R.id.username_lnr1);

                booknotif = (TextView) itemView.findViewById(R.id.booknotif);


                //booknotif.setVisibility(View.INVISIBLE);

                circleImageView = (CircleImageView) itemView.findViewById(R.id.userImage);

                username_lnr1.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                DataModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());


                switch (v.getId()) {
                    case R.id.username_lnr1:
                        buyerid = getDataAdapter1.getRecieverID();
                        Log.e("recever_is", buyerid);

                        new Clearenotification().execute();
                        String recevername = capitalize(getDataAdapter1.getReciever_name());
//                        Intent i = new Intent(context, Personalchat_activity.class);//Msg_actvity_fromChat
                        Intent i = new Intent(context, Personalchat_activity.class);//Msg_actvity_fromChat
                        i.putExtra("receiver_id", getDataAdapter1.getRecieverID());
                        i.putExtra("sendername", recevername);
                        i.putExtra("senderimage", getDataAdapter1.getImage());
                        i.putExtra("list", "list");

                        Log.e("user_id", getDataAdapter1.getRecieverID());
                        Log.e("image", getDataAdapter1.getImage());

                        context.startActivity(i);
                        getActivity().finish();

                        break;

//                    http://www.smartbaba.in/liveatcampus/api
                }
            }
        }


        private class Clearenotification extends AsyncTask<Void, Void, Void> {


            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "clear_push_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);


                try {
                    JSONObject jo = new JSONObject();


                    jo.put("rid", sp.getString(Constant.rid, ""));
                    //jo.put("book_id", book_id);


                    System.out.println("the send data clear_push_chat :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                {
                    super.onPostExecute(result);
                    try {


                        if (success.getString("success").equals("1")) {


                        } else if (success.getString("success").equals("0")) {
                            // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        System.out.println("the title exeption is :" + e);
                        e.printStackTrace();
                    }
                }


            }
        }

    }


    /*@Override
    public void onBackPressed() {
    *//*    super.onBackPressed();
        finish();*//*
        Intent i=new Intent(getApplicationContext(),HomeActivity.class);
        startActivity(i);
        finish();
*//*        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, new Menu_Activity());
        ft.addToBackStack(null);
        ft.commit();
        finish();*//*

    }*/

}