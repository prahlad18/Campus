package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.Univercity_Model;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.Utils.MultiSelectionSpinner;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;


public class Edit_Profile extends Fragment {
    static final int PICK_IMAGE_REQUEST = 1;
    ListView ListFile;
    String encodedImage;
    EditText editName, editEmail, editPassword;
    String imagePath;
    Spinner spnUniversity, spnMajor, spnYear;
    MultiSelectionSpinner spnTwo;
    ProgressDialog pDialog;
    StringBuffer campusNameList = new StringBuffer();
    Univercity_Model category;
    ArrayAdapter<String> spinnerArrayUnivercity;
    ArrayAdapter<String> spinnerMajor;
    ArrayAdapter<String> spinnerYear;
    String strUid, strMajorid, strYear, strparent_id, strSession_id, parent_id;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    SharedPreferences.Editor editor;
    EditText edtAssignment;
    private TextView btnEdit;
    private ArrayList<String> selectedCampusList;
    private CircleImageView circleImageView;
    private ImageView imgUniversityLogo;
    private int SELECT_PICTURE = 100;
    private SharedPreferences app_sharedPreferences;
    private Spinner spnLevel;
    private MultiSelectionSpinner spnCampus;
    private ArrayList<String> univercityName;
    private ArrayList<String> campusName;
    private ArrayList<String> yearName;
    private ArrayList<String> majorName;
    private ArrayList<String> parentNameArrayList;
    private ArrayList<String> classId;
    private ArrayList<String> Uid;
    private ArrayList<String> campusID;
    private ArrayList<String> majorID;
    private ArrayList<String> yearID;
    private JSONObject success;
    private ArrayList<Univercity_Model> arrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.edit_proifle, container, false);
        sp = getActivity().getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        setHasOptionsMenu(true);
        SharedPreferences app_preferences = getActivity().getSharedPreferences("livecampus", MODE_PRIVATE);
        editor = app_preferences.edit();
        app_sharedPreferences = getActivity().getSharedPreferences("livecampus", MODE_PRIVATE);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().setTitle("Edit Profile");
        btnEdit = (TextView) convertView.findViewById(R.id.btnEdit);
        editName = (EditText) convertView.findViewById(R.id.editName);
        editName.setText(sp.getString(Constant.username, ""));
        circleImageView = (CircleImageView) convertView.findViewById(R.id.image_profile);
        Picasso.with(getContext())
                .load(sp.getString(Constant.profile, ""))
                .into(circleImageView);
        spnUniversity = (Spinner) convertView.findViewById(R.id.spnOne);
        spnCampus = (MultiSelectionSpinner) convertView.findViewById(R.id.spnTwo);
        spnMajor = (Spinner) convertView.findViewById(R.id.spnThree);
        spnYear = (Spinner) convertView.findViewById(R.id.spnFour);
        // imgUniversityLogo = (ImageView)convertView.findViewById(R.id.university_logo);
        univercityName = new ArrayList<String>();
        majorName = new ArrayList<String>();
        yearName = new ArrayList<String>();
        campusName = new ArrayList<String>();
        campusID = new ArrayList<String>();
        Uid = new ArrayList<String>();
        majorID = new ArrayList<String>();
        yearID = new ArrayList<String>();
        parentNameArrayList = new ArrayList<String>();
        arrayList = new ArrayList<Univercity_Model>();

        new PostDataTOSession().execute();
        new GetMajorData().execute();
        new GetYEarData().execute();
        // new PostDataTOSession_Get_by_Uni().execute();
        spnUniversity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                } else {
                    strUid = Uid.get(position);
                    System.out.println("the Uid :" + strUid);
                    arrayList.clear();
                    parentNameArrayList.clear();
                    parentNameArrayList.removeAll(parentNameArrayList);
                    campusName.remove(campusNameList);


                    new PostDataTOSession_Get_by_Uni().execute();


                }


            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnCampus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {


                } else {


                    try {

                        parent_id = campusID.get(position);

                        Log.e("campus", parent_id);

                        selectedCampusList.clear();

                        try {
                            String xxx = spnTwo.getSelectedItemsAsString();

                            Log.e("xxx", xxx);
                        } catch (Exception e) {

                        }


                        System.out.println("the campus id :" + parent_id);
                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spnMajor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    strMajorid = majorID.get(position);
                    System.out.println("the major id  :" + strMajorid);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    strYear = yearID.get(position);
                    System.out.println("the year id  :" + strYear);

                }

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strUid == null) {
                    Toast.makeText(getActivity(), "please Select the university", Toast.LENGTH_LONG).show();
                } else if (strMajorid == null) {
                    Toast.makeText(getActivity(), "please Select the Major", Toast.LENGTH_LONG).show();
                } else if (strYear == null) {
                    Toast.makeText(getActivity(), "please Select the Year", Toast.LENGTH_LONG).show();
                } else if (campusNameList == null) {
                    Toast.makeText(getActivity(), "please Select Campus Names", Toast.LENGTH_LONG).show();
                } else {
                    String name = editName.getText().toString().trim();
                    Log.i("university id", strUid);
                    Log.i("Major Id", "" + strMajorid);
                    Log.i("year_id", "" + strYear);
                    Log.i("campusName", "" + spnCampus.getSelectedItemsAsString());
                    new PostDataTOServer().execute();
                }
            }
        });

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Profile Pic", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
              /*  Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image*//*");
                //intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE);*/

            }
        });

        return convertView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE && resultCode == getActivity().RESULT_OK) {
            Uri selectedImageUri = data.getData();
            if (null != selectedImageUri) {
                // Get the path from the Uri
                String path = getPathFromURI(selectedImageUri);
                // Set the image in ImageView
                onSelectFromGalleryResult(data);
            }
        }

    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        Bitmap bm1 = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                Matrix matrix = new Matrix();
                // matrix.postRotate(90);
                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte imageInByte[] = stream.toByteArray();
                imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        circleImageView.setImageBitmap(bm1);
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    @Override
    public void onResume() {
        super.onResume();
      /*  arrayList.clear();
        categoryArrayList.clear();
        new PostDataTOServer().execute();*/

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }


    //get_univercity_value

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Updating");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "edituser_profile.php";
            // String serverUrl="http://www.smartbaba.in/gomobile/api/get_teacherleavenotes.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {

                JSONObject jo = new JSONObject();

                //  jo.put("tid", "" + sp.getString(Constant.Id, ""));

                jo.put("rid", "" + sp.getString(Constant.rid, ""));
                jo.put("name", "" + editName.getText().toString());
                jo.put("uid", "" + strUid);
                jo.put("mid", "" + strMajorid);
                jo.put("campusname", "" + spnCampus.getSelectedItemsAsString());
                jo.put("year_id", "" + strYear);
                jo.put("image", "" + imagePath);
                //Log.e("profile",imagePath);


                Log.e("class id is :", Constant.rid);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);

            } catch (Exception e) {
                System.out.println("Exception in profile edit : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1"))

                {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my update : " + j);


                    {
                        System.out.println("the updated data " + j);

                        String rid = j.getString("rid");

                        editor.putString("rid", rid).commit();
                        //sp.edit().putString(Constant.rid, rid).commit();
                        String profile = j.getString("image");

                        String name = j.getString("name");

                        sp.edit().putString(Constant.username, name).commit();
                        sp.edit().putString(Constant.profile, profile).commit();

                        Log.e("name", name);
                        Log.e("rid is", rid);

                        Toast.makeText(getActivity(), "Profile Updated", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);

                    }


                } else if (success.getString("success").equals("0")) {


                    Log.e("error in", "Edit Profile Fail");


                    pDialog.dismiss();


                } else {
                    pDialog.dismiss();

                }

            } catch (Exception e) {
                pDialog.dismiss();
            }

        }
    }

    private class PostDataTOSession extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getunivercities.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
           /* if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        Log.i("total university", jsonArrayClass.length() + "");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            univercityName.add(j.getString("univercity_name"));
                            Log.i("universityname", j.getString("univercity_name"));
                            Uid.add(j.getString("uid"));//report id*/
                        }
                        //Toast.makeText(getActivity(),"total university :"+univercityName.size(),Toast.LENGTH_LONG).show();

                        spinnerArrayUnivercity = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, univercityName);
                        spinnerArrayUnivercity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnUniversity.setAdapter(spinnerArrayUnivercity);
                        spinnerArrayUnivercity.notifyDataSetChanged();

                        if (spinnerArrayUnivercity != null) {

                            spnUniversity.setAdapter(spinnerArrayUnivercity);

//                            sp.edit().putString( Constant.selected,"").clear();

                            spinnerArrayUnivercity.notifyDataSetChanged();


                        } else {

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }

    private class GetMajorData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getmajors.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            majorName.add(j.getString("majors_name"));
                            // sessionName.add(j.getString("session_name"));


                            majorID.add(j.getString("mid"));//report id
                        }


                        spinnerMajor = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, majorName);
                        spinnerMajor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnMajor.setAdapter(spinnerMajor);
                        spinnerMajor.notifyDataSetChanged();


                        if (spinnerMajor != null) {

                            spnMajor.setAdapter(spinnerMajor);

                            spinnerMajor.notifyDataSetChanged();


                        } else {
                            Toast.makeText(getActivity(), "Call", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }

    private class PostDataTOSession_Get_by_Uni extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getcampusbyuniversity.php";

            campusName.clear();

            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("uid", "" + strUid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        if (success.getString("success").equals("1")) {
                            try {
                                JSONArray jsonArrayClass = success.getJSONArray("posts");
                                for (int i = 0; i < jsonArrayClass.length(); i++) {
                                    JSONObject j = jsonArrayClass.getJSONObject(i);
                                    campusName.add(j.getString("camp_name"));
                                    campusID.add(j.getString("camp_id"));
                                }

                                try {
//                                    spnTwo.setItems(campusName, "Campus", this);

//                                    univercityName.clear();


                                    spnCampus.setItems(campusName);

                                    selectedCampusList.clear();
                                    selectedCampusList.notify();
                                    campusID.clear();
                                    campusID.notify();

                                    campusName.notify();

                                } catch (Exception e) {

                                }


                            } catch (JSONException e) {

                                System.out.println("the title exe is :" + e);
                                e.printStackTrace();
                            }
                            // pDialog.hide();
                        } else if (success.getString("success").equals("0")) {
                            // pDialog.hide();
                        } else {

                        }
                    } catch (Exception e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                        //pDialog.hide();
                    }
                    //  pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    //pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }

       /* @Override
        public void onItemsSelected(boolean[] selected) {
            sp.edit().putString( Constant.selected,""+campusNameList).commit();

        }

        @Override
        public void onItemsTextSelected(ArrayList<String> selectedItemText) {


                selectedCampusList = selectedItemText;*/




           /* sp.edit().putString( Constant.selected,""+campusNameList).commit();



            Log.e( "shared data",sp.getString( Constant.selected,"" ) );*/
           /* try {


                campusNameList.setLength(0);
                for (String parentName : selectedCampusList) {

                    //my code
                    if (campusNameList.length() == 0) {
                        campusNameList.append(parentName);
                        campusNameList.append("");


                        Log.e( "single",""+campusNameList );
//                        spnTwo.setItems( campusName,""+campusNameList,listener );


                    }

                    else {


                        campusNameList.append("," + parentName);
                        campusNameList.append("");

                        Log.e( "multi else",""+campusNameList );
                        spnTwo.setItems( campusName,""+campusNameList,listener );

                    }

                }

            }catch (Exception e)
            {
                System.out.print(""+e.getMessage());
            }
*/


    }

    private class GetYEarData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getschoolyears.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            yearName.add(j.getString("year_name"));
                            // sessionName.add(j.getString("session_name"));
                            yearID.add(j.getString("year_id"));//report id
                        }


                        spinnerYear = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, yearName);
                        spinnerYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnYear.setAdapter(spinnerYear);
                        spinnerYear.notifyDataSetChanged();


                        if (spinnerYear != null) {

                            spnYear.setAdapter(spinnerYear);

                            spinnerYear.notifyDataSetChanged();

                        } else {
                            Toast.makeText(getActivity(), "Call", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }


}