package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.SellBookModel_new;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.adapter.Sellandreject_adapter;

/**
 * Created by isquare3 on 3/14/18.
 */

public class SellAndReject extends Fragment {

    public TextView txtSearch, txtTextbook, txtHome, txt_search_1, txtBookTitle, txtISBN;
    public EditText etxt_search;
    public AutoCompleteTextView autoComplete;
    public ArrayAdapter<String> aAdapter;
    public ImageView image_book;
    public TextView back, btnEdit;
    ArrayList<SellBookModel_new> listOfBook = new ArrayList<SellBookModel_new>();
    JSONObject catObj;
    SellBookModel_new category;
    String image, strrating;
    JSONArray jsonArrayCategory;
    String course_id, price, rating;
    RatingBar ratingBar;
    String book_id;
    JSONObject success;
    SharedPreferences sp;
    String Prefrence = "Prefrence";
    ListView searchList;
    private ProgressDialog pDialog;
    private String strSearch = "";
    private SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_sellandreject, container, false);

        sp = getActivity().getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        setHasOptionsMenu(true);
        back = (TextView) view.findViewById(R.id.back);
        btnEdit = (TextView) view.findViewById(R.id.btnEdit);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        image_book = (ImageView) view.findViewById(R.id.image_book);
        txtBookTitle = (TextView) view.findViewById(R.id.txt_book_title);
        txtISBN = (TextView) view.findViewById(R.id.txt_book_isbn);
        searchList = (ListView) view.findViewById(R.id.searchList);


        sp = getActivity().getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        final SellBookModel_new model = new SellBookModel_new();
        // txtHome.setText(R.string.home);
        //txtTextbook.setText(R.string.mytextbook);
        // txtSearch.setText(R.string.searchtext);


      /*  Sellandreject_adapter adapter = new Sellandreject_adapter(getActivity(), listOfBook,getActivity());
        searchList.setAdapter(adapter);*/
        //new Get_detail().execute();
        new SellAndReject.Get_Gallery().execute();

        //  new SellAndReject.PostDataTOServer().execute();
        //book_id=sp.getString("book_id","");


        Bundle bundle = getArguments();
        course_id = bundle.getString("course_id");
        String strImage = bundle.getString("image");
        String title = bundle.getString("title");
        String ISBN_NO = bundle.getString("ISBN");
        String strprice = bundle.getString("price");
        String strrating = bundle.getString("rating");

     /*   course_id=sp.getString(Constant.COURSE_ID,"");
        String strImage=sp.getString(Constant.IMAGE,"");
        String title=sp.getString(Constant.TITLE,"");
        String ISBN_NO=sp.getString(Constant.ISBN,"");
        String strprice=sp.getString(Constant.PRI,"");
        Float strrat=sp.getFloat(Constant.RAT,Float.parseFloat(""));
        Log.e("rating", String.valueOf(strrat));*/


//        Log.e("book_id",course_id);

        btnEdit.setText("$ " + strprice + ".00");
        txtBookTitle.setText(title);
        txtISBN.setText(ISBN_NO);


        if (strrating.equals("")) {

            ratingBar.setRating(Float.parseFloat(""));

        } else {
            ratingBar.setRating(Float.parseFloat("" + strrating));

        }
        if (strImage.equals("")) {
            Picasso.with(getActivity())
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.main_ic)
                    .into(image_book);
        } else {
            Picasso.with(getActivity())
                    .load(strImage)
                    .resize(150, 230)
                    .placeholder(R.drawable.main_ic)
                    .into(image_book);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SellTextFragment());
                ft.addToBackStack(null);
                ft.commit();
                getActivity().onBackPressed();


                //getActivity().finish();
            }
        });


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    private class Get_Gallery extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "counter_link.php";

            System.out.println("the get course url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", course_id);

                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");

                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel_new();
                        catObj = jsonArrayCategory.getJSONObject(i);
                        category.setBookNane(catObj.getString("name"));
                        category.setBookname(catObj.getString("cNAME1"));
                        category.setBookImage(catObj.getString("image1"));
                        category.setUserimage(catObj.getString("image"));
                        category.setUnivervity(catObj.getString("univercity_name"));
                        category.setMajor(catObj.getString("majors_name"));
                        category.setYear(catObj.getString("year_name"));
                        category.setRating(catObj.getString("rating"));
                        category.setPrice(catObj.getString("price"));
                        category.setIsbn(catObj.getString("cISBN1"));
                        // category.setRid(catObj.getString("rid"));
                        category.setBuyerid(catObj.getString("Buyer_Id"));
                        category.setSellerid(catObj.getString("Seller_Id"));
                        category.setRecever_id(catObj.getString("CourseId"));


                        listOfBook.add(category);
                    }
                    Sellandreject_adapter adapter = new Sellandreject_adapter(getActivity(), listOfBook, getActivity());
                    searchList.setAdapter(adapter);
                } else if (success.getString("success").equals("0")) {

                }

            } catch (JSONException e) {
                pDialog.dismiss();
                System.out.println("the title exeption is :" + e);
                e.printStackTrace();
            }
        }


    }


}


