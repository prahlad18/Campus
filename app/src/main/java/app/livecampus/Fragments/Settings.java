package app.livecampus.Fragments;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.R;


public class Settings extends Fragment {


    String inputPattern = "yyyy-MM-dd";
    final SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
    String outputPattern = "dd-MM-yyyy";
    final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.setting, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().setTitle("Settings");
        setHasOptionsMenu(true);
        return convertView;
    }

    @Override
    public void onResume() {
        super.onResume();
      /*  arrayList.clear();
        categoryArrayList.clear();
        new PostDataTOServer().execute();*/

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }


}