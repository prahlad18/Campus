package app.livecampus.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.Univercity_Model;
import app.livecampus.R;
import app.livecampus.Services.GPSTracker;
import app.livecampus.Utils.Constant;
import app.livecampus.Utils.MultiSelectionSpinner;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by isquare3 on 3/1/18.
 */

public class Edit_Profile2 extends Fragment {
    static final int PICK_IMAGE_REQUEST = 1;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String ruserImage;
    ListView ListFile;
    String encodedImage;
    EditText editName, editEmail, editPassword;
    String imagePath;
    Spinner spnUniversity, spnMajor, spnYear;
    Spinner spnTwo;
    String img;
    String Strname, Struid, Stryer, Strmid, Strimg;
    TextView txt_next;
    ProgressDialog pDialog;
    StringBuffer campusNameList = new StringBuffer();
    Univercity_Model category;
    ArrayAdapter<String> spinnerArrayUnivercity;
    ArrayAdapter<String> spinnerMajor;
    ArrayAdapter<String> spinnerCampus;
    ArrayAdapter<String> spinnerYear;
    String strUid, strMajorid, strYear, strCampusid, campus_Name, parent_id, majorname, yeare;
    String uid, name, mid, yid, caid, cpname, cid, cpnm;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    SharedPreferences.Editor editor;
    GPSTracker gpsTracker;
    EditText edtAssignment;
    private TextView btnEdit;
    private ArrayList<String> selectedCampusList;
    private CircleImageView circleImageView;
    private ImageView imgUniversityLogo;
    private int SELECT_PICTURE = 100;
    private SharedPreferences app_sharedPreferences;
    private Spinner spnLevel;
    private MultiSelectionSpinner spnCampus;
    private ArrayList<String> univercityName;
    private ArrayList<String> campusName;
    private ArrayList<String> yearName;
    private ArrayList<String> majorName;
    private ArrayList<String> parentNameArrayList;
    private ArrayList<String> classId;
    private ArrayList<String> Uid;
    private ArrayList<String> campusID;
    private ArrayList<String> majorID;
    private ArrayList<String> yearID;
    private JSONObject success;
    private ArrayList<Univercity_Model> arrayList;
    private Bitmap bitmap;
     File destination = null;
     InputStream inputStreamImg;
     String imgPath = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.edit_proifle, container, false);
        setHasOptionsMenu(true);

        sp = getActivity().getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        gpsTracker = new GPSTracker(getActivity());
        ruserImage = sp.getString(Constant.profile, "");

       /* if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        }*/

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(true);
        pDialog.show();

        arrayList = new ArrayList<>();
        univercityName = new ArrayList<>();
        campusName = new ArrayList<>();
        majorName = new ArrayList<>();
        yearName = new ArrayList<>();

        parentNameArrayList = new ArrayList<>();

        classId = new ArrayList<>();
        Uid = new ArrayList<>();
        campusID = new ArrayList<>();
        majorID = new ArrayList<>();
        yearID = new ArrayList<>();


        SharedPreferences app_preferences = getActivity().getSharedPreferences("livecampus", MODE_PRIVATE);
        editor = app_preferences.edit();


        app_sharedPreferences = getActivity().getSharedPreferences("livecampus", MODE_PRIVATE);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().setTitle("Edit Profile");


        btnEdit = (TextView) convertView.findViewById(R.id.btnEdit);
        editName = (EditText) convertView.findViewById(R.id.editName);
        editName.setText(sp.getString(Constant.username, ""));
        circleImageView = (CircleImageView) convertView.findViewById(R.id.image_profile);

        if (ruserImage.equals("")) {
            Picasso.with(getActivity())
                    .load(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .into(circleImageView);
        } else {
            Picasso.with(getActivity())
                    .load(ruserImage)
                    .placeholder(R.drawable.user)
                    .into(circleImageView);
        }


        imgUniversityLogo = (ImageView) convertView.findViewById(R.id.ulogo);
        spnUniversity = (Spinner) convertView.findViewById(R.id.spnOne);
        spnTwo = (Spinner) convertView.findViewById(R.id.spnTwo);
        spnMajor = (Spinner) convertView.findViewById(R.id.spnThree);
        spnYear = (Spinner) convertView.findViewById(R.id.spnFour);
        imgUniversityLogo = (ImageView) convertView.findViewById(R.id.ulogo);
        univercityName = new ArrayList<String>();
        majorName = new ArrayList<String>();
        yearName = new ArrayList<String>();
        campusName = new ArrayList<String>();
        campusID = new ArrayList<String>();
        Uid = new ArrayList<String>();
        majorID = new ArrayList<String>();
        yearID = new ArrayList<String>();
        parentNameArrayList = new ArrayList<String>();
        arrayList = new ArrayList<Univercity_Model>();
        classId = new ArrayList<>();
        Uid = new ArrayList<>();
        campusID = new ArrayList<>();
        majorID = new ArrayList<>();
        yearID = new ArrayList<>();

        univercityName.add("Select University");
        campusName.add(" Select Your Primary Campus");
        majorName.add("Select Major");
        yearName.add("Select Year");

        classId.add("*");
        Uid.add("*");
        campusID.add("*");
        majorID.add("*");
        yearID.add("*");


        Bundle bundle = getArguments();
        name = bundle.getString("name");
        mid = bundle.getString("mid");
        uid = bundle.getString("uid");
        yid = bundle.getString("year_id");
        cpnm = bundle.getString("cpname");

        spnUniversity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    spnUniversity.setSelection(Integer.parseInt(uid));

                } else {
                    strUid = Uid.get(position);
                    System.out.println("the Uid :" + strUid);
                    arrayList.clear();
                    new PostDataTOSession_Get_by_Uni().execute();
                    new image().execute();

                }


            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        spnTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    campusName.add(0, " Select Your Primary Campus");//rakesh
                    spnTwo.setSelection(Integer.parseInt(cpnm));


                } else {

                    //String Name= spnTwo.getSelectedItem().toString();
                    strCampusid = campusID.get(position);
                    System.out.println("the campus id  :" + strCampusid);

                    campus_Name = spnTwo.getSelectedItem().toString();
                    Log.e("campus name is", campus_Name);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spnMajor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnMajor.setSelection(Integer.parseInt(mid));

                } else {
                    strMajorid = majorID.get(position);
                    System.out.println("the major id  :" + strMajorid);

                    majorname = spnMajor.getSelectedItem().toString();
                    Log.e("campus name is", majorname);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnYear.setSelection(Integer.parseInt(yid));
                } else {
                    strYear = yearID.get(position);
                    System.out.println("the year id  :" + strYear);
                    yeare = spnYear.getSelectedItem().toString();
                    Log.e("campus name is", yeare);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (strUid == null)
                    Toast.makeText(getActivity(), "Select University", Toast.LENGTH_SHORT).show();
                else if (strMajorid == null) {
                    Toast.makeText(getActivity(), "Select Major", Toast.LENGTH_SHORT).show();

                } else if (strYear == null) {
                    Toast.makeText(getActivity(), "Select Year", Toast.LENGTH_SHORT).show();
                } else if (strCampusid == null) {
                    Toast.makeText(getActivity(), "Select campus name", Toast.LENGTH_SHORT).show();
                } else {
                    String name = editName.getText().toString().trim();
                    sp.edit().putString(Constant.StrCampus_name, campus_Name).commit();
                    sp.edit().putString(Constant.campus_id, strCampusid).commit();
                    sp.edit().putString(Constant.Strmajorname, majorname).commit();
                    sp.edit().putString(Constant.Stryeare, yeare).commit();
                    new PostDataTOServer().execute();

                }

            }
        });

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();

            }
        });
        new PostDataTOSession().execute();
        new GetMajorData().execute();
        new GetYEarData().execute();
        return convertView;
    }

    private void selectImage() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, 0);

                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, 1);

                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == 0) {
            Bitmap bm1 = null;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");


                byte imageInByte[] = bytes.toByteArray();
                imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);

                //imgPath = destination.getAbsolutePath();
                circleImageView.setImageBitmap(bm1);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        else if (requestCode == 1) {
            Bitmap bm = null;
            Bitmap bm1 = null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    Matrix matrix = new Matrix();
                    // matrix.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte imageInByte[] = stream.toByteArray();
                    imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                    bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            circleImageView.setImageBitmap(bm1);
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (null != data) {

            switch (requestCode) {

                case 0:

                    Bitmap imageBitmap = null;

                    if (resultCode == RESULT_OK) {
                        Bundle extras = data.getExtras();
                        if (extras != null) {
                            imageBitmap = (Bitmap) extras.get("data");

                        }

                        Uri tempUri = getImageUri(getActivity(), imageBitmap);
                        File finalFile = new File(getRealPathFromURI(tempUri));
                        imagePath = finalFile.toString();

                        //Toast.makeText(getApplicationContext(), picturePath, Toast.LENGTH_LONG).show();
                        circleImageView.setImageURI(Uri.fromFile(finalFile));
                        System.out.println(finalFile);
                    }

                    break;

                case 1:


                    if (resultCode == RESULT_OK) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getActivity().getContentResolver()
                                .query(selectedImage, filePathColumn, null, null,
                                        null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imagePath = cursor.getString(columnIndex);
                        circleImageView.setImageURI(selectedImage);

                        //Toast.makeText(getApplicationContext(), picturePath, Toast.LENGTH_LONG).show();
                        cursor.close();
                    }
                    break;

            }
        }
    }*/
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    /*public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }


    //get_univercity_value

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    private class PostDataTOSession extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getunivercities.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
           /* if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            univercityName.add(j.getString("univercity_name"));
                            Log.i("universityname", j.getString("univercity_name"));

                            // sessionName.add(j.getString("session_name"));


                            Uid.add(j.getString("uid"));//report id
                        }


                        spinnerArrayUnivercity = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, univercityName);
                        spinnerArrayUnivercity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnUniversity.setAdapter(spinnerArrayUnivercity);
                        spinnerArrayUnivercity.notifyDataSetChanged();

                        if (spinnerArrayUnivercity != null) {

                            spnUniversity.setAdapter(spinnerArrayUnivercity);
                            spinnerArrayUnivercity.notifyDataSetChanged();
                        } else {

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }

    private class GetMajorData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getmajors.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            majorName.add(j.getString("majors_name"));
                            // sessionName.add(j.getString("session_name"));


                            majorID.add(j.getString("mid"));//report id
                        }


                        spinnerMajor = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, majorName);
                        spinnerMajor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnMajor.setAdapter(spinnerMajor);
                        spinnerMajor.notifyDataSetChanged();


                        if (spinnerMajor != null) {

                            spnMajor.setAdapter(spinnerMajor);

                            spinnerMajor.notifyDataSetChanged();


                        } else {
                            Toast.makeText(getActivity(), "Call", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }

    private class PostDataTOSession_Get_by_Uni extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getcampusbyuniversity.php";

            campusName.clear();

            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("uid", "" + strUid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @SuppressLint("ApplySharedPref")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        if (success.getString("success").equals("1")) {
                            try {
                                JSONArray jsonArrayClass = success.getJSONArray("posts");
                                for (int i = 0; i < jsonArrayClass.length(); i++) {
                                    JSONObject j = jsonArrayClass.getJSONObject(i);
                                    campusName.add(j.getString("camp_name"));

                                    campusID.add(j.getString("camp_id"));
                                }


                                spinnerCampus = new ArrayAdapter<String>(getActivity(),
                                        android.R.layout.simple_spinner_item, campusName);
                                spinnerCampus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spnTwo.setAdapter(spinnerCampus);
                                spinnerCampus.notifyDataSetChanged();

                                if (spinnerCampus != null) {

                                    spnTwo.setAdapter(spinnerCampus);

                                    spinnerCampus.notifyDataSetChanged();


                                } else {
                                    Toast.makeText(getActivity(), "Call", Toast.LENGTH_SHORT).show();

                                }

                            } catch (JSONException e) {

                                System.out.println("the title exe is :" + e);
                                e.printStackTrace();
                            }
                            // pDialog.hide();
                        } else if (success.getString("success").equals("0")) {
                            // pDialog.hide();
                        } else {

                        }
                    } catch (Exception e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                        //pDialog.hide();
                    }
                    //  pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    //pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }
    }

    private class GetYEarData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getschoolyears.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            yearName.add(j.getString("year_name"));
                            // sessionName.add(j.getString("session_name"));
                            yearID.add(j.getString("year_id"));//report id
                        }


                        spinnerYear = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, yearName);
                        spinnerYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnYear.setAdapter(spinnerYear);
                        spinnerYear.notifyDataSetChanged();
                        if (spinnerYear != null) {

                            spnYear.setAdapter(spinnerYear);

                            spinnerYear.notifyDataSetChanged();

                        } else {
                            Toast.makeText(getActivity(), "Call", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Updating");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "edituser_profile.php";
            // String serverUrl="http://www.smartbaba.in/gomobile/api/get_teacherleavenotes.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {

                JSONObject jo = new JSONObject();

                jo.put("rid", "" + sp.getString(Constant.rid, ""));
                jo.put("name", "" + editName.getText().toString());
                jo.put("uid", "" + strUid);
                jo.put("mid", "" + strMajorid);
                jo.put("campusname", "" + strCampusid);
                jo.put("year_id", "" + strYear);
                jo.put("image", "" + imagePath);
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);

            } catch (Exception e) {
                System.out.println("Exception in profile edit : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1"))

                {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my update : " + j);


                    {
                        System.out.println("the updated data " + j);

                        // String rid=j.getString("rid");

                        String rid = j.getString("rid");

                        editor.putString("rid", rid).commit();
                        //sp.edit().putString(Constant.rid, rid).commit();
                        String profile = j.getString("image");

                        String name = j.getString("name");
                        String campusname = j.getString("campusname");
                        String uid = j.getString("uid");

                        sp.edit().putString(Constant.username, name).commit();
                        sp.edit().putString(Constant.campus_id, campusname).commit();
                        sp.edit().putString(Constant.uni_ID, uid).commit();

                        //sp.edit().putString(Constant.uid,name).commit();
                        //sp.edit().putString(Constant.year_id,name).commit();
                        //sp.edit().putString(Constant.mid,name).commit();
                        //sp.edit().putString(Constant.campus_name,name).commit();
                        if (profile.length() > 1) {

                            sp.edit().putString(Constant.profile, profile).commit();

                            Log.e("null", "nullok");


                        } else {
                            //sp.edit().putString(Constant.profile,profile).commit();
                            Log.e("image", "");


                        }


                        Log.e("name", name);
                        Log.e("rid is", rid);

                        Toast.makeText(getActivity(), "Profile Updated", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        startActivity(intent);
                        getActivity().finish();


                       /* category.setId(catObj.getString("tech_assignmentid"));
                        category.setTeacher(catObj.getString("teachername"));
                        category.setSubject(catObj.getString("sub_name"));
                        category.setDate(catObj.getString("date"));


                        category.setAssingnment(catObj.getString("teacher_assignment"));
                        category.settId(catObj.getString("tid"));




                        arrayList.add(category);*/
                    }

                   /* Assignment_list_Adapter event_adapter = new Assignment_list_Adapter(getActivity(), arrayList);
                    List.setAdapter(event_adapter);
                    event_adapter.notifyDataSetChanged();*/


                } else if (success.getString("success").equals("0")) {


                    Log.e("error in", "Edit Profile Fail");


                    pDialog.dismiss();


                } else {
                    pDialog.dismiss();

                }

            } catch (Exception e) {
                pDialog.dismiss();
            }

        }


    }

    private class image extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_univercity_logo.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("uid", "" + strUid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        if (success.getString("success").equals("1")) {
                            try {
                                JSONArray jsonArrayClass = success.getJSONArray("posts");
                                for (int i = 0; i < jsonArrayClass.length(); i++) {
                                    JSONObject j = jsonArrayClass.getJSONObject(i);

                                    img = (j.getString("univercity_logo"));
                                    sp.edit().putString(Constant.campus_image, img).commit();

                                }

                                try {

                                    img = img.replaceAll(" ", "%20");
                                    URL sourceUrl = new URL(img);
                                    Picasso.with(getActivity())
                                            .load("" + sourceUrl)
                                            .into(imgUniversityLogo);


                                } catch (Exception e) {

                                }


                            } catch (JSONException e) {

                                System.out.println("the title exe is :" + e);
                                e.printStackTrace();
                            }
                            // pDialog.hide();
                        } else if (success.getString("success").equals("0")) {
                            // pDialog.hide();
                        } else {

                        }
                    } catch (Exception e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                        //pDialog.hide();
                    }
                    //  pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    //pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }


    }


    /*private class updatesave extends AsyncTask<Void, Void, Void>{
        protected void onPreExecute() {
            super.onPreExecute();
           *//* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*//*
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_details.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                jo.put("rid", "" + sp.getString(Constant.rid,""));



                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            *//*if(pDialog.isShowing())
                pDialog.dismiss();*//*

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        if (success.getString("success").equals("1")) {
                            try {
                                JSONArray jsonArrayClass = success.getJSONArray("posts");
                                for (int i = 0; i < jsonArrayClass.length(); i++) {
                                    JSONObject j = jsonArrayClass.getJSONObject(i);

                                     Strname=(j.getString("name"));
                                     Struid=Integer.parseInt(j.getString("uid"));
                                     Stryer=Integer.parseInt(j.getString("year_id"));
                                     Strmid=Integer.parseInt(j.getString("mid"));
                                   // Strimg=Integer.parseInt(j.getString("image"));


                                }

                                *//*try {


                                    Strimg = Strimg.replaceAll(" ", "%20");
                                    URL sourceUrl = new URL(Strimg);
                                    Picasso.with(getActivity())
                                            .load(""+sourceUrl)
                                            .into(circleImageView);



                                }catch (Exception e)
                                {

                                }*//*


                            } catch (JSONException e) {

                                System.out.println("the title exe is :" + e);
                                e.printStackTrace();
                            }
                            // pDialog.hide();
                        } else if (success.getString("success").equals("0")) {
                            // pDialog.hide();
                        } else {

                        }
                    } catch (Exception e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                        //pDialog.hide();
                    }
                    //  pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    //pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }




    }*/


   /* @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_leave);
        item.setVisible(false);
    }*/
}


