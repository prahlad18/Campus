package app.livecampus.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.SellBookModel_new;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.adapter.SellBookListAdapter_new;


public class SearchBookFragment extends Fragment {

    static String strVal = "one";
    public TextView txtSearch, txtTextbook, txtHome;
    public EditText etxt_search;
    public Button txt_search_1;
    public AutoCompleteTextView autoComplete;
    public ArrayAdapter<String> aAdapter;
    public String myValue = "";
    ArrayList<SellBookModel_new> listOfBook;
    JSONObject catObj;
    SellBookModel_new category;
    JSONArray jsonArrayCategory;
    //public AutoCompleteTextView etxt_search;
    String receiver_id;
    String search;
    JSONObject success;
    SharedPreferences sp;
    String Prefrence = "Prefrence";
    ListView searchList;
    Bundle bundle;
    private ProgressDialog pDialog;
    private String strSearch = "";
    private SharedPreferences.Editor editor;

    public SearchBookFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_book, container, false);
        setHasOptionsMenu(true);
        SharedPreferences app_preferences = getActivity().getSharedPreferences("livecampus", Context.MODE_PRIVATE);

        sp = getActivity().getSharedPreferences(Prefrence, Context.MODE_PRIVATE);

        txtHome = (TextView) view.findViewById(R.id.txt_home);
        searchList = (ListView) view.findViewById(R.id.searchList);
        //txtSearch = (TextView)view.findViewById(R.id.txt_search_text);
        txtTextbook = (TextView) view.findViewById(R.id.txt_my_text_book);
        txt_search_1 = (Button) view.findViewById(R.id.txt_search);
        etxt_search = (EditText) view.findViewById(R.id.etxt_search);
        //etxt_search.setThreshold(1);


        bundle = getArguments();
        // String yes = bundle.getString("yes");


        txtHome.setText(R.string.home);
        txtTextbook.setText(R.string.mytextbook);
        //txtSearch.setText(R.string.searchtext);
        listOfBook = new ArrayList<SellBookModel_new>();

        search = sp.getString(Constant.SEARCHTEX, "");

        txtHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(homeIntent);
                getActivity().finish();
            }
        });

        /*txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
        txtTextbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SellTextFragment());
                ft.commit();
            }
        });

        // String getvalue=getArguments().getString("yes");


        if (bundle != null) {
            Boolean getValue = getArguments().getBoolean("BOOLEAN_VALUE");
            if (getValue) {

                //Toast.makeText(getActivity(),"next",Toast.LENGTH_SHORT).show();
                etxt_search.setText("");


            } else {
                //Toast.makeText(getActivity(),"back",Toast.LENGTH_SHORT).show();

                listOfBook.clear();
                etxt_search.setText(search);
                strSearch = etxt_search.getText().toString().trim();
                new PostDataTOServer().execute();

            }
        }


        txt_search_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                listOfBook.clear(); // this list which you hava passed in Adapter for your listview
                //SellBookListAdapter_new.notifyDataSetChanged();

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txt_search_1.getWindowToken(), 0);

                strSearch = etxt_search.getText().toString().trim();

                if (etxt_search.getText().toString().length() > 0) {
                    txt_search_1.setText("Search");
                    new PostDataTOServer().execute();
                    myValue = etxt_search.getText().toString();
                    //Toast.makeText(getActivity(),""+myValue,Toast.LENGTH_SHORT).show();
                    sp.edit().putString(Constant.SEARCHTEX, myValue).commit();

                }
               /* else if(etxt_search.getText().toString().length()<1)
                {
                    txt_search_1.setText("Search");
                    searchList.setVisibility(View.GONE);

                }*/
                else {
                    Toast.makeText(getActivity(), "Enter ISBN and book name", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Searching results");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String serverUrl = Constant.MAIN_URL + "search.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("search", "" + strSearch);
                //jo.put("rid",sp.getString(Constant.rid,""));


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @SuppressLint("ApplySharedPref")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            //txt_search_1.setVisibility(View.GONE);
            searchList.setVisibility(View.VISIBLE);

            try {
                if (success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");

                    for (int i = 0; i < jsonArrayCategory.length(); i++) {
                        catObj = jsonArrayCategory.getJSONObject(i);

                        category = new SellBookModel_new();
                        catObj = jsonArrayCategory.getJSONObject(i);
//                        category.setId(catObj.getString("course_id"));

                        //receiver_id = getString(Integer.parseInt("rid"));
                        // sp.edit().putString(Constant.Recever_id, receiver_id).commit();
                        category.setBookNane(catObj.getString("name"));
                        category.setBookname(catObj.getString("cNAME1"));
                        category.setBookImage(catObj.getString("image1"));
                        category.setUserimage(catObj.getString("image"));
                        category.setUnivervity(catObj.getString("univercity_name"));
                        category.setMajor(catObj.getString("majors_name"));
                        category.setYear(catObj.getString("year_name"));
                        category.setRating(catObj.getString("rating"));
                        category.setPrice(catObj.getString("price"));
                        category.setIsbn(catObj.getString("cISBN1"));
                        category.setRid(catObj.getString("rid"));
                        category.setRecever_id(catObj.getString("CourseId"));
                        category.setStatus(catObj.getString("Status"));
                        category.setImages1(catObj.getString("images1"));
                        category.setImages2(catObj.getString("images2"));
                        category.setImages3(catObj.getString("images3"));
                        category.setBlub(catObj.getString("blub"));
                        category.setDate(catObj.getString("newdate"));

                        listOfBook.add(category);

                    }
                    SellBookListAdapter_new adapter = new SellBookListAdapter_new(getActivity(), listOfBook, getActivity());
                    searchList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } else if (success.getString("success").equals("0")) {
                    pDialog.dismiss();
                    Toast.makeText(getActivity(), "Record not found", Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                pDialog.dismiss();
                System.out.println("the title exeption is :" + e);
                e.printStackTrace();
            }
        }
    }


}
