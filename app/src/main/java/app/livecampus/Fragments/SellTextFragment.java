package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import app.livecampus.Activity.HomeActivity;
import app.livecampus.Pojo.SellBookModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.adapter.SellBookListAdapter;

public class SellTextFragment extends Fragment {

    private static final int RESULT_OK = 1;
    public ProgressDialog pDialog;
    TextView txt_home, txt_search, back,txtError;
    GridView gridViewListOfBook;
    JSONObject catObj;
    SellBookModel category;
    JSONArray jsonArrayCategory;
    String book_id, price;
    JSONObject success;
    SharedPreferences sp;
    ArrayList<SellBookModel> listOfBook = new ArrayList<>();
    SellBookListAdapter adapter;

    String inputPattern = "yyyy-MM-dd";
    final SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
    String outputPattern = "dd-MM-yyyy";
    final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);


    // ArrayList<SellBookModel> list=new ArrayList<>();

    SharedPreferences app_sharedprefrence;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setHasOptionsMenu(true);
        sp = getActivity().getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

        // Inflate the layout for this fragment
        View convertView = inflater.inflate(R.layout.fragment_sell_text, container, false);
       /* final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);*/
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        app_sharedprefrence = getActivity().getSharedPreferences("livecampus", Context.MODE_PRIVATE);

        listOfBook = new ArrayList<>();

        back = (TextView) convertView.findViewById(R.id.btnEdit);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), HomeActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });
        txt_home = (TextView) convertView.findViewById(R.id.txt_sell_home);
        txt_home.setText(R.string.home);

        txt_search = (TextView) convertView.findViewById(R.id.txt_sell_search);
        txtError = (TextView) convertView.findViewById(R.id.txtError);
        //txt_search.setText(R.string.search);


        gridViewListOfBook = (GridView) convertView.findViewById(R.id.grid_book);

        new Get_Gallery().execute();

        txt_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(homeIntent);
                getActivity().finish();
            }
        });
        txt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent searchIntent = new Intent(getActivity().getApplicationContext(), SearchBookActivity.class);
                startActivity(searchIntent);
                getActivity().finish();*/
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                SearchBookFragment addPriceOfBookFragment = new SearchBookFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("BOOLEAN_VALUE", true);
                addPriceOfBookFragment.setArguments(bundle);
                ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                ft.commit();
            }
        });
        return convertView;
    }

    public String getTitleOfBook(String title) {
        String titleBook = "";
        String book[] = title.split(" ");
        for (int i = 0; i < 2; i++) {

            titleBook += book[i] + " ";
        }
        return titleBook;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();


        }
        return true;

    }

    /* @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_leave);
        item.setVisible(false);
    }
*/
    @Override
    public void onPause() {
        super.onPause();

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    private class Get_Gallery extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = ProgressDialog.show(getActivity(), "", "Please wait...",
                    true);
           /* pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_course.php";

            System.out.println("the get course url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", sp.getString(Constant.rid, ""));


                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss();
            }
            try {
                if (success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");
                    txtError.setVisibility(View.GONE);
                    gridViewListOfBook.setVisibility(View.VISIBLE);
                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel();
                        catObj = jsonArrayCategory.getJSONObject(i);
                        category.setCourse_id(catObj.getString("course_id"));
                        category.setTitle(catObj.getString(Constant.CNAME1));
                        category.setIsbn(catObj.getString(Constant.cISBN1));
                        category.setImage(catObj.getString(Constant.IMAGE1));
                        category.setRating(catObj.getString("rating"));
                        category.setFinalprice(catObj.getString("FinalPrice"));
                        category.setOffercount(catObj.getString("BookCount"));
                        category.setBuyername(catObj.getString("buyername"));
                        Log.i("rating", catObj.getString("rating"));
                        if (catObj.getString(Constant.PRICE).equals("")) {
                            category.setPrice("0");
                        } else {
                            category.setPrice((catObj.getString(Constant.PRICE)));
                        }

                        Date date = null;
                        String strStart = null;

                        try {
                            date = inputFormat.parse(catObj.getString(Constant.DATE));

                            strStart = outputFormat.format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        category.setDate(strStart);

//                            category.setDate(catObj.getString(Constant.DATE));
                        listOfBook.add(category);
                    }
                    adapter = new SellBookListAdapter(getActivity(), listOfBook, getActivity());
                    gridViewListOfBook.setAdapter(adapter);


                } else if (success.getString("success").equals("0")) {
                    gridViewListOfBook.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText(""+success.getString("posts"));
//                    Toast.makeText(getActivity(), ""+success.getString("posts"), Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                pDialog.dismiss();
                System.out.println("the title exeption is :" + e);
                e.printStackTrace();
            }
        }
    }
}


