package app.livecampus.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Activity.ActivityNotificationList;
import app.livecampus.Activity.CampusList;
import app.livecampus.Activity.Campuschat_activity;
import app.livecampus.Activity.Chat_Tabbar;
import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.adapter.Campuschatadaptor;
import de.hdodenhof.circleimageview.CircleImageView;


public class Menu_Activity extends Fragment implements View.OnClickListener {

    private static final float BLUR_RADIUS = 25f;
    private static final long DOUBLE_PRESS_INTERVAL = 250; // in millis
    static int mNotifCount = 0;
    private static int pendingNotificationsCount = 0;
    public LinearLayout edit_post;
    public String campus_name;
    public CircleImageView bookimage;
    public boolean call = true;
    public Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8;
    public String userID = "4";
    public Thread t;
    public TextView title;
    public TextView tv_sender, doubletext, booknotif, tvCampus;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList = new ArrayList<>();
    ArrayList<DataModel> arrayList_count = new ArrayList<>();
    DataModel category;
    AsyncTask<Void, Void, Void> SendData;
    TextView textCartItemCount, textCartItemCount_bell;
    int mCartItemCount = 10;
    Handler mHandler;
    String tomsg, msg, groupMsg;
    int count;
    int value = -1;
    private ImageView imgCram;
    private ImageView imgSell;
    private ImageView imgSearch;
    private TextView txtUserName;
    private CircleImageView imgProfile;
    private String userName, profileImage;
    private SharedPreferences app_sharedprefrence;
    private boolean mHasDoubleClicked = false;
    private long lastPressTime;
    private String messageBody, img;
    private JSONObject success;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.exam_sell_cram, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        sp = getActivity().getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        getActivity().setTitle(R.string.app_name);
        FragmentManager fm = getFragmentManager();

        setHasOptionsMenu(true);

        app_sharedprefrence = getActivity().getSharedPreferences("livecampus", Context.MODE_PRIVATE);
        userName = app_sharedprefrence.getString("name", "");
        profileImage = app_sharedprefrence.getString("image", "");


        booknotif = (TextView) convertView.findViewById(R.id.booknotif);
        tvCampus = (TextView) convertView.findViewById(R.id.tvCampus);

        booknotif.setVisibility(View.INVISIBLE);


        imgCram = (ImageView) convertView.findViewById(R.id.image_cram);
        //btn1 = (Button) convertView.findViewById(R.id.btn1);
        //btn2 = (Button) convertView.findViewById(R.id.btn2);
        btn3 = (Button) convertView.findViewById(R.id.btn3);
        btn4 = (Button) convertView.findViewById(R.id.btn4);
        btn5 = (Button) convertView.findViewById(R.id.btn5);
        btn6 = (Button) convertView.findViewById(R.id.btn6);
        btn7 = (Button) convertView.findViewById(R.id.btn7);
        btn8 = (Button) convertView.findViewById(R.id.btn8);
        //btn1.setOnClickListener(this);
        //btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);

//        btn1.setOnClickListener(this);
        // btn1.setOnClickListener(this);
        listView = (RecyclerView) convertView.findViewById(R.id.listview);
        edit_post = (LinearLayout) convertView.findViewById(R.id.edit_post);
        imgSell = (ImageView) convertView.findViewById(R.id.image_sell);
        imgSearch = (ImageView) convertView.findViewById(R.id.image_search);
        imgProfile = (CircleImageView) convertView.findViewById(R.id.cir_img_profile);
        txtUserName = (TextView) convertView.findViewById(R.id.txt_username);
        //doubletext = (TextView)convertView.findViewById(R.id.doubletext);

/*        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);*/

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(manager);
        listView.setHasFixedSize(true);


//        Toast.makeText(getActivity(),profileImage,Toast.LENGTH_LONG).show();

        final String profile = sp.getString(Constant.profile, "");
        String username = sp.getString(Constant.username, "");
        String uid = sp.getString(Constant.uid, "");
        String year_id = sp.getString(Constant.year_id, "");
        String mid = sp.getString(Constant.mid, "");
        campus_name = sp.getString(Constant.StrCampus_name, "");

        Log.e("uid", uid);


        Log.e("camous is", sp.getString(Constant.campus_name, ""));


        if (username.equals("")) {
            txtUserName.setText("Welcome, User");
            String chars = capitalize(username);
//textView.setText(chars);
            System.out.println("Output: " + chars);
        } else {
            String chars = capitalize(username);
//textView.setText(chars);
            System.out.println("Output: " + chars);

            txtUserName.setText("Welcome, " + chars);

        }

        if (profile.equals("")) {
            Picasso.with(getActivity())
                    .load(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .into(imgProfile);

        } else {
            Picasso.with(getActivity())
                    .load(profile)
                    .placeholder(R.drawable.user)
                    .into(imgProfile);
        }


       /* if (profile.equals(""))
        {
            Picasso.with(getContext())
                    .load(R.drawable.user)
                    .into(imgProfile);
        }
        else
        {
            Picasso.with(getContext())
                    .load(profile)
                    .into(imgProfile);
        }*/


        Log.e("name", username);
        Log.e("profie", profile);

        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {


            }
        });

        imgCram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTimer();
               /* Intent intent = new Intent(getActivity().getApplicationContext(), ActivityExam_Cram.class);
                startActivity(intent);*/

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new app.livecampus.Fragments.ActivityExam_Cram());
                ft.commit();

            }
        });
        imgSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent = new Intent(getActivity().getApplicationContext(), DemoAct.class);
                startActivity(intent);*/
                stopTimer();

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SellTextFragment());
                ft.commit();
            }
        });
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTimer();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                SearchBookFragment addPriceOfBookFragment = new SearchBookFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("BOOLEAN_VALUE", true);
                addPriceOfBookFragment.setArguments(bundle);
                ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                ft.commit();
            }
        });

        listView.setOnTouchListener(new View.OnTouchListener() {

            private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    //asyncFetch.cancel(true);
                    //timerTask.cancel();
                    stopTimer();

                    Intent i = new Intent(getActivity(), Campuschat_activity.class);
                    //i.putExtra("campusname",""+campus_name);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
//                    getActivity().finish();
                    return super.onDoubleTap(e);
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent event) {
                    Log.d("TEST", "onSingleTap");
                    // Toast.makeText(getActivity(),"double click",Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });


        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custom_dialog);


                ImageView image = (ImageView) dialog.findViewById(R.id.image);

                ImageView cancle = (ImageView) dialog.findViewById(R.id.ib_close);
                cancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                if (profile.equals("")) {
                    Picasso.with(getContext())
                            .load(R.drawable.user)
                            .into(image);
                } else {
                    Picasso.with(getContext())
                            .load(profile)
                            .into(image);
                }


                dialog.show();
            }
        });

        tvCampus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopTimer();
                Intent i = new Intent(getActivity(), CampusList.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
//        callAsynchronousTask();


        return convertView;
    }


    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.btn3:

                messageBody = btn3.getText().toString().toUpperCase();
                sendTextMessage("" + messageBody);
                new Receivemessage().execute();

                break;
            case R.id.btn4:
                messageBody = btn4.getText().toString().toUpperCase();
                sendTextMessage("" + messageBody);
                new Receivemessage().execute();

                break;
            case R.id.btn5:
                messageBody = btn5.getText().toString().toUpperCase();
                sendTextMessage("" + messageBody);
                new Receivemessage().execute();
                break;
            case R.id.btn6:
                messageBody = btn6.getText().toString().toUpperCase();
                sendTextMessage("" + messageBody);
                new Receivemessage().execute();
                break;
            case R.id.btn7:
                messageBody = btn7.getText().toString().toUpperCase();
                sendTextMessage("" + messageBody);
                new Receivemessage().execute();
                break;
            case R.id.btn8:
                messageBody = btn8.getText().toString().toUpperCase();
                sendTextMessage("" + messageBody);
                new Receivemessage().execute();
                break;
        }
    }

    private void sendTextMessage(final String s) {


        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "input_campus_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("rid", sp.getString(Constant.rid, ""));
                    jo.put("campus", sp.getString(Constant.campus_id, "").toString());
                    jo.put("lid", sp.getString(Constant.uid, ""));
                    jo.put("message", messageBody);
                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");
                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();

                        category.setSenderID("" + success.getString("SenderId"));
                        category.setMsg("" + success.getString("Message"));
                        category.setImage("" + success.getString("SenderImage"));

                        arrayList.add(category);
                        adapter = new Campuschatadaptor(getActivity(), arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);

                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getActivity(), "Incorrect UserName and Password", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {

                    public void run() {
                        try {
                            new Receivemessage().execute();
                            adapter.notifyDataSetChanged();
                            new Notcount().execute();

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);// 1 second
    }

    public void stopTimer() {
        Log.e("calling", "ok");

        try {
            if (timerTask != null) {
                timerTask.cancel();
            } else {
                Log.e("notcancle", "notcancle");
            }
        } catch (Exception e) {

        }
    }



    public void onStart() {
        super.onStart();
        callAsynchronousTask();
        Log.e("onStart ", "start");
    }
    @Override
    public void onPause() {
        super.onPause();
        stopTimer();

    }

    public void onStop() {
        super.onStop();
        stopTimer();
        Log.e("onStop ", "stop");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here

        inflater.inflate(R.menu.menu_main, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_leave);
        final MenuItem menuItem_bell = menu.findItem(R.id.action_invitation);
        View actionView = MenuItemCompat.getActionView(menuItem);
        View actionView_bell = MenuItemCompat.getActionView(menuItem_bell);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);
        textCartItemCount.setVisibility(View.INVISIBLE);
        textCartItemCount_bell = (TextView) actionView_bell.findViewById(R.id.cart_badge);
        textCartItemCount_bell.setVisibility(View.INVISIBLE);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onOptionsItemSelected(menuItem);
            }
        });
        actionView_bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onOptionsItemSelected(menuItem_bell);
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_leave:

                Intent i = new Intent(getActivity(), Chat_Tabbar.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().finish();
                stopTimer();
                break;
            case R.id.action_invitation:
                Intent invite = new Intent(getActivity(), ActivityNotificationList.class);
                invite.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(invite);
                getActivity().finish();
                stopTimer();
                break;


        }
        return true;

    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_campus_chat.php";
            System.out.println("the url is :" + serverUrl);
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("campus", sp.getString(Constant.campus_id, ""));//userid
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {
                            if (isCancelled())
                                break;
                            if (i > value) {
                                value++;
                                category = new DataModel();
                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setReciever_name("" + success.getString("SenderName"));
                                category.setImage("" + success.getString("SenderImage"));
                                category.setDatetime("" + success.getString("DateTime"));
                                category.setMsg("" + success.getString("Message"));
                                arrayList_count.add(category);

                                if (arrayList_count != arrayList) {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new Campuschatadaptor(getActivity(), arrayList);
                                    listView.setAdapter(adapter);

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }
                            }
                        }
                        Log.e("arraylist size", "" + arrayList.size());

                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "campuschat_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                jo.put("from_user_id", sp.getString(Constant.rid, ""));
                jo.put("sender_name", sp.getString(Constant.username, ""));
                jo.put("lid", sp.getString(Constant.uid, ""));
                jo.put("message", messageBody);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }
        }
    }

    private class Notcount extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "display_count.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();
                jo.put("sell_id", sp.getString(Constant.rid, ""));
                System.out.println("the send data is of count :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (isCancelled())
                                break;
                            success = jsonArrayCategory.getJSONObject(i);
                            msg = success.getString("TotalCount");
                            tomsg = success.getString("TotalMessageCount");
                            groupMsg = success.getString("GroupMessageCount");
                            //Toast.makeText(getpp, msg, Toast.LENGTH_LONG).show();
                            Log.e("TotalCount ", "is " + msg);
                            Log.e("TotalMessageCount ", "is " + tomsg);
//                            textCartItemCount.getText().toString();
                            if (groupMsg.toString().equals("0")) {

                                textCartItemCount_bell.setVisibility(View.INVISIBLE);


                            } else {
                                textCartItemCount_bell.setVisibility(View.VISIBLE);
                                textCartItemCount_bell.setText(groupMsg);

                            }


                            if (msg.toString().equals("0")) {

                                booknotif.setVisibility(View.INVISIBLE);


                            } else {
                                booknotif.setVisibility(View.VISIBLE);
                                booknotif.setText(msg);

                            }
                            if (tomsg.toString().equals("0")) {

                                try {
                                    textCartItemCount.setVisibility(View.INVISIBLE);

                                } catch (Exception e) {

                                }

                            } else {
                                textCartItemCount.setVisibility(View.VISIBLE);
                                textCartItemCount.setText(tomsg);

                            }

                        }
                        Log.e("arraylist size", "" + arrayList.size());


                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }


        }
    }
}





