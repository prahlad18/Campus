package app.livecampus.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;

import app.livecampus.Activity.Mainloginandsigup;
import app.livecampus.R;


public class Logout extends android.support.v4.app.Fragment {
    public static final int progress_bar_type = 0;
    public String pos;
    ListView ListFile;
    ImageView my_image;
    JSONObject catObj;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    String inputPattern = "yyyy-MM-dd";
    final SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
    String outputPattern = "dd-MM-yyyy";
    final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
    private ProgressDialog pDialog;
    private TextView textView;
    private JSONObject success;
    private String TAG;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View convertView = inflater.inflate(R.layout.logout, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        sp = getActivity().getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        pref = getActivity().getSharedPreferences("contact", Context.MODE_PRIVATE);
        //textView = (TextView) convertView.findViewById(R.id.text);


        sp.edit().clear().commit();
        pref.edit().clear().commit();
        getActivity().setTitle("Logout");

        Intent i = new Intent(getActivity(), Mainloginandsigup.class);
        startActivity(i);
        getActivity().finish();
        return convertView;
    }



    /*private class PostDataTOServer extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_teacherleavenotes.php";
           // String serverUrl="http://www.smartbaba.in/gomobile/api/get_teacherleavenotes.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {

                JSONObject jo = new JSONObject();

              //  jo.put("tid", "" + sp.getString(Constant.Id, ""));

                jo.put("school_id", "" + sp.getString(Constant.s_id,""));


                jo.put("class_id", "" + sp.getString(Constant.Class_id, ""));
                jo.put("section_id", "" + sp.getString(Constant.Section_id, ""));

                Log.e("class id is :",Constant.Class_id);
                Log.e("section is :",Constant.Section_id);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);

            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1")) {
                    try {

                        JSONArray jsonArrayCategory = success.getJSONArray("posts");

                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            category = new Absent_Note_Model();
                            JSONObject catObj = jsonArrayCategory.getJSONObject(i);

                            catObj = jsonArrayCategory.getJSONObject(i);

                            category.setId(catObj.getString("id"));
                            category.setStudent_name(catObj.getString("student_name"));
                            category.setClass_name(catObj.getString("class_name"));
                            category.setSection(catObj.getString("section_name"));

                            category.setReason(catObj.getString("reason"));


                            Date date = null;
                            Date date1=null;
                            Date date2=null;
                             String strStart = null;
                            String strStart1 = null;
                            String strStart2 = null;

                            try {
                                date = inputFormat.parse(catObj.getString("date_submission"));
                                date1 = inputFormat.parse(catObj.getString("date_absence"));

                                date2 = inputFormat.parse(catObj.getString("date_resumption"));

                                strStart = outputFormat.format(date);
                                strStart1 = outputFormat.format(date1);
                                strStart2 = outputFormat.format(date2);


                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            category.setDate(strStart);
                            category.setAbsence_date(strStart1);
                            category.setResumption_date(strStart2);

                            arrayList.add(category);
                        }

                        Absent_Note_Adapter event_adapter = new Absent_Note_Adapter(getActivity(), arrayList);
                        ListFile.setAdapter(event_adapter);
                        event_adapter.notifyDataSetChanged();


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
                    pDialog.hide();


                } else if (success.getString("success").equals("0")) {
                    pDialog.hide();

                } else {
                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                pDialog.hide();

            }
        }
    }
    */

    @Override
    public void onResume() {
        super.onResume();
      /*  arrayList.clear();
        categoryArrayList.clear();
        new PostDataTOServer().execute();*/
    }
}