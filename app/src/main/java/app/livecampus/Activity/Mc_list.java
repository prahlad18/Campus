package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 7/4/18.
 */

public class Mc_list extends AppCompatActivity {

    public CircleImageView bookimage;
    public boolean call = true;
    public String userID = "4";
    public Thread t;
    public TextView title;
    public TextView tv_sender;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList = new ArrayList<>();
    ArrayList<DataModel> hs = new ArrayList<>();
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    Handler mHandler;
    String book_id, book_name, book_image, rid;
    int value = -1;
    private String messageBody, img;
    private JSONObject success;
    private ProgressDialog pDialog;
    private Button mButtonSend, btn_ok;
    private EditText mEditTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mc_list);
        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);


        listView = (RecyclerView) findViewById(R.id.listview);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);

        new Receivemessage().execute();
    }


    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "personalchat_list.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("rid", sp.getString(Constant.rid, ""));//userid


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {


                            category = new DataModel();


                            success = jsonArrayCategory.getJSONObject(i);
                            //category.setSenderID("" + success.getString("pic"));
                            category.setRecieverID("" + success.getString("R_Id"));
                            category.setReciever_name("" + success.getString("Name"));
                            category.setImage("" + success.getString("Image"));
                            category.setMsg("" + success.getString("Message"));
                            category.setNotcount("" + success.getString("Unread"));
                            category.setTime("" + success.getString("Time"));

                            arrayList.add(category);


                        }
                        Log.e("arraylist size", "" + arrayList.size());
                        adapter = new RecyclerViewAdapter_chat(Mc_list.this, arrayList);
                        listView.setAdapter(adapter);

                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }


    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<Mc_list.RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;
        String buyerid;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public RecyclerViewAdapter_chat.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mc_list_items, parent, false);
            RecyclerViewAdapter_chat.ViewHolder viewHolder = new RecyclerViewAdapter_chat.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter_chat.ViewHolder holder, final int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            String userID = "4";
            TextView txtUserName, txtUserUniversity, booknotif, txtUserMajor, txtUserYear;
            LinearLayout username_lnr1;
            Button msg;
            CircleImageView circleImageView;

            public ViewHolder(View itemView) {

                super(itemView);
                txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
                txtUserUniversity = (TextView) itemView.findViewById(R.id.txtUserUniversity);
                txtUserMajor = (TextView) itemView.findViewById(R.id.txtUserMajor);
                txtUserYear = (TextView) itemView.findViewById(R.id.txtUserYear);
                username_lnr1 = (LinearLayout) itemView.findViewById(R.id.username_lnr1);

                msg = (Button) itemView.findViewById(R.id.msg);


                //booknotif.setVisibility(View.INVISIBLE);

                circleImageView = (CircleImageView) itemView.findViewById(R.id.userimg);

                msg.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                DataModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());


                switch (v.getId()) {
                    case R.id.msg:
                        Log.e("hello", "hello");
                        break;

//                    http://www.smartbaba.in/liveatcampus/api
                }
            }
        }


    }
}
