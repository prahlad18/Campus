package app.livecampus.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import app.livecampus.Pojo.DataModel;
import app.livecampus.Pojo.GroupModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 6/27/18.
 */

public class ActivityGroupChat extends AppCompatActivity implements View.OnClickListener {

    public TextView title, tv_campusName;
    public String charsf, charsr, cname;
    public CircleImageView bookimage;
    public CircleImageView img_book;
    public TextView txtblub, blubtext, edate, tv_courseName, tvCram;
    public TextView tv_sender;
    public String bookimage1, blub, date, course, strCram, rid, groupID, strBookImage, strBookImagePred;
    public Button btn_List;
    public ImageView img_Bulb, img_Calendar;
    ArrayList<DataModel> arrayList_count = new ArrayList<>();
    int value = -1;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList = new ArrayList<>();
    ArrayList<GroupModel> arrayList_Member = new ArrayList<>();
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    AsyncTask<Void, Void, Void> groupInfo;
    AsyncTask<Void, Void, Void> groupInfo_member;
    String Prefrence = "Prefrence";
    Receivemessage asyncFetch;
    private String messageBody;
    private boolean loading = true;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();
    private JSONObject success;
    private Button mButtonSend;
    private EditText mEditTextMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examchat);
        getSupportActionBar().hide();
        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);

        btn_List = (Button) findViewById(R.id.btn_List);

        strBookImage = sp.getString("groupImage", "");
        listView = (RecyclerView) findViewById(R.id.listview);
        mButtonSend = (Button) findViewById(R.id.btn_send);
        img_Bulb = (ImageView) findViewById(R.id.img_Bulb);
        img_Calendar = (ImageView) findViewById(R.id.img_Calendar);
        txtblub = (TextView) findViewById(R.id.tv_Bulb);
        tvCram = (TextView) findViewById(R.id.tvCram);
        tv_courseName = (TextView) findViewById(R.id.tv_courseName);
        img_book = (CircleImageView) findViewById(R.id.img_book);
        mEditTextMessage = (EditText) findViewById(R.id.et_message);

        Intent i = getIntent();
        if (i.hasExtra("groupid")) {

            groupID = i.getStringExtra("groupid");
            sp.edit().putString("groupid", groupID).commit();

        }


     /*   if (strBookImage.equalsIgnoreCase("")) {
            Picasso.with(ActivityGroupChat.this)
                    .load(R.drawable.main_ic)
                    .placeholder(R.drawable.main_ic)
                    .into(img_book);
        } else {
            Picasso.with(ActivityGroupChat.this)
                    .load(strBookImage)
                    .placeholder(R.drawable.main_ic)
                    .into(img_book);
        }*/
        btn_List.setOnClickListener(this);
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditTextMessage.getText().length() > 1) {

                    messageBody = mEditTextMessage.getText().toString();
                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                    //new Pushnotification().execute();


                } else {


                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");

            }
        });


        getGroupInfo("" + sp.getString("groupid", ""));
        getGroupMemberCount("" + sp.getString("groupid", ""));

        asyncFetch = new Receivemessage();
        asyncFetch.execute();
        callAsynchronousTask();


        LinearLayoutManager manager = new LinearLayoutManager(this);
        listView.setLayoutManager(manager);
        listView.setHasFixedSize(true);


        img_Bulb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(v.getContext());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.blubpopup);

                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                //TextView btnback = (TextView) dialog.findViewById(R.id.btnback);
                Button exam = (Button) dialog.findViewById(R.id.exam);
                blubtext = (TextView) dialog.findViewById(R.id.blubtext);
                Button assigment = (Button) dialog.findViewById(R.id.assigment);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                blubtext.setText(blub);

                dialog.show();


            }
        });


        img_Calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(v.getContext());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.datepopup);

                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                //TextView btnback = (TextView) dialog.findViewById(R.id.btnback);
                Button exam = (Button) dialog.findViewById(R.id.exam);
                edate = (TextView) dialog.findViewById(R.id.edate);
                Button assigment = (Button) dialog.findViewById(R.id.assigment);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                edate.setText(date);

                dialog.show();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getGroupInfo(final String s) {

        groupInfo = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "group_info.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("group_id", sp.getString("groupid", ""));

                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception group chat : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                {
                    super.onPostExecute(result);

                    try {


                        if (success.getString("success").equals("1")) {
                            jsonArrayCategory = success.getJSONArray("posts");
                            for (int i = 0; i < jsonArrayCategory.length(); i++) {
                                success = jsonArrayCategory.getJSONObject(i);

                                bookimage1 = success.getString("images1");
                                blub = success.getString("blub");
                                date = success.getString("date");
                                course = success.getString("coursename");
                                strCram = success.getString("status");

                                txtblub.setText(blub);
                                tv_courseName.setText(course);
                                tvCram.setText("" + strCram);

                                if (bookimage1.equals("")) {
                                    Picasso.with(ActivityGroupChat.this)
                                            .load(R.drawable.main_ic)
                                            .placeholder(R.drawable.main_ic)
                                            .into(img_book);
                                } else if (bookimage1.contains("https://")) {
                                    Picasso.with(ActivityGroupChat.this)
                                            .load(bookimage1)
                                            .placeholder(R.drawable.main_ic)
                                            .into(img_book);
                                } else {
                                    Picasso.with(ActivityGroupChat.this)
                                            .load(Constant.MAIN_URL + bookimage1)
                                            .placeholder(R.drawable.main_ic)
                                            .into(img_book);
                                }
                            }


                        } else if (success.getString("success").equals("0")) {
                            // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        System.out.println("the title exeption is :" + e);
                        e.printStackTrace();
                    }
                }
            }
        };
        groupInfo.execute();
    }


    @SuppressLint("StaticFieldLeak")
    private void sendTextMessage(final String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "input_group_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("from_user_id", sp.getString(Constant.rid, ""));
                    jo.put("group_id", groupID);
                    jo.put("message", messageBody);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");
                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();


                        category.setSenderID("" + sp.getString(Constant.rid, ""));
                        category.setMsg("" + messageBody);

                        arrayList.add(category);
                        adapter = new Campuschatinside(ActivityGroupChat.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect charactor", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_List:
                stopTimer();
                Intent i = new Intent(getApplicationContext(), ActivityGroupChatList.class);
                i.putExtra("groupid", groupID);

                startActivity(i);

                break;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getGroupMemberCount(final String s) {

        groupInfo_member = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "group_members.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("group_id", sp.getString("groupid", ""));

                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                {
                    super.onPostExecute(result);

                    try {


                        if (success.getString("success").equals("1")) {
                            jsonArrayCategory = success.getJSONArray("post");
                            for (int i = 0; i < jsonArrayCategory.length(); i++) {


                                GroupModel category = new GroupModel();


                                success = jsonArrayCategory.getJSONObject(i);
                                category.setRid("" + success.getString("rid"));
                                category.setProfile_name("" + success.getString("profile_name"));
                                category.setProfile_image("" + success.getString("profile_image"));
                                category.setMajors("" + success.getString("majors"));
                                category.setYear("" + success.getString("year"));

                                arrayList_Member.add(category);


                            }
                            btn_List.setText("" + arrayList_Member.size());
                        } else if (success.getString("success").equals("0")) {
                            // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        System.out.println("the title exeption is :" + e);
                        e.printStackTrace();
                    }
                }
            }
        };
        groupInfo_member.execute();
    }

    private void stopTimer() {

        asyncFetch.cancel(true);
        if (timerTask != null) {
            timerTask.cancel();
            //handler.removeCallbacks(timer);

        } else {
            Log.e("notcancle", "notcancle");


        }
    }

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            asyncFetch = new Receivemessage();
                            asyncFetch.execute();

                            adapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);//500,2000

    }

    @Override
    public void onBackPressed() {

        // super.onBackPressed();

        Intent i = new Intent(getApplicationContext(), Chat_Tabbar.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        startActivity(i);
        stopTimer();
        asyncFetch.cancel(true);
        //i.putExtra("asyntask",""+asyncFetch);
        finish();

    }

    private String capitalize(String text) {
        if (null != text && text.length() != 0) {
            text = text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();

        }
        return text;
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_group_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                jo.put("group_id", sp.getString("groupid", ""));//userid
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();


                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setImage("" + success.getString("SenderImage"));
                                category.setReciever_name("" + success.getString("SenderName"));
                                category.setMajor("" + success.getString("MajorName"));
                                category.setYear("" + success.getString("YearName"));
                                category.setMsg("" + success.getString("Message"));

                                /*
//                                category.setRecieverID("" + success.getString("SenderId"));
                                category.setReciever_name("" + success.getString("SenderName"));*/

                                arrayList.add(category);
                                listView.scrollToPosition(arrayList.size() - 1);

                                arrayList_count.add(category);

                                if (arrayList_count != arrayList) {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new Campuschatinside(ActivityGroupChat.this, arrayList);
                                    listView.setAdapter(adapter);

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }

                            }

                        }


                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    public class Campuschatinside extends RecyclerView.Adapter<Campuschatinside.ViewHolder> {

        private static final float BLUR_RADIUS = 25f;
        public CircleImageView bookimage;
        public boolean call = true;
        public String userID = "4";
        public Thread t;
        public TextView title;
        public TextView tv_sender;
        public String user_id, reciver_id;
        public String ruserImage, sendername, majorname, yeares, username;
        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;
        RecyclerView listView;
        JSONArray jsonArrayCategory;
        RecyclerView.Adapter adapter;
        ArrayList<DataModel> arrayList;
        ArrayList<DataModel> hs = new ArrayList<>();
        DataModel category;
        GridLayoutManager mLayoutManager;
        AsyncTask<Void, Void, Void> SendData;
        Handler mHandler;
        String book_id, book_name, book_image, rid;
        int value = -1;
        private String messageBody, img;
        private JSONObject success;
        private ProgressDialog pDialog;
        private Button mButtonSend, btn_ok;
        private EditText mEditTextMessage;

        public Campuschatinside(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.campuschatlist, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;


        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username = sp.getString(Constant.username, "");
            String profile = sp.getString(Constant.profile, "");
            user_id = sp.getString(Constant.uid, "");
            ruserImage = getDataAdapter1.getImage();


            //String rusername = getDataAdapter1.getReciever_name();

            // String chars = capitalize(username);

            rid = sp.getString(Constant.rid, "");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);


            if (ruserImage.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.logo)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.receiver);
            } else {
                Picasso.with(context)
                        .load(Constant.MAIN_URL + ruserImage)
                        .resize(125, 125)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.receiver);
            }


            if (profile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.logo)
                        .placeholder(R.drawable.logo)
                        .into(holder.sender);
            } else {
                Picasso.with(context)
                        .load(profile)
                        .resize(125, 125)
                        .placeholder(R.drawable.logo)
                        .into(holder.sender);
            }


            if (rid.equals(getDataAdapter1.getSenderID())) {

                String name = getDataAdapter1.getMsg();

                charsf = capitalize(name);
                holder.tv_sender.setText(charsf);
                //holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                //holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                //holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.receiver.setVisibility(View.GONE);
                holder.sender.setVisibility(View.VISIBLE);

            } else {
                String name = getDataAdapter1.getMsg();
                charsr = capitalize(name);
                //holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_receiver.setText(charsr);
                holder.tv_sender.setVisibility(View.GONE);
                //holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                //holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.receiver.setVisibility(View.VISIBLE);
                holder.sender.setVisibility(View.GONE);

            }


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            String userID = "4";
            TextView tv_sender, tv_receiver, tv_receiver_name, tv_sender_name;
            LinearLayout linearLayout;
            CircleImageView receiver, sender;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                //tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
                // tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);
                linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
                receiver = (CircleImageView) itemView.findViewById(R.id.userImage);
                sender = (CircleImageView) itemView.findViewById(R.id.senderimage);


                linearLayout.setOnClickListener(this);


            }

            @Override
            public void onClick(View v) {

                final DataModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());


                switch (v.getId()) {
               /* case R.id.linearLayout:
                    Log.e("click","click");
                    Log.e("receiver_id",getDataAdapter1.getSenderID());
                    Log.e("sendername",getDataAdapter1.getReciever_name());
                    Intent i = new Intent(context, Personalchat_activity.class);
                    i.putExtra("receiver_id",getDataAdapter1.getSenderID());
                    i.putExtra("sendername",getDataAdapter1.getReciever_name());
                    context.startActivity(i);

                    break;*/
                    case R.id.linearLayout:
                        // Do something
                    {
                        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context);
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

                        View layout = inflater.inflate(R.layout.popup,
                                (ViewGroup) ActivityGroupChat.this.findViewById(R.id.viewgroup));
                        ImageView image = (ImageView) layout.findViewById(R.id.imageFull);
                        TextView name = (TextView) layout.findViewById(R.id.name);
                        TextView mjor = (TextView) layout.findViewById(R.id.mjorname);
                        TextView yeare = (TextView) layout.findViewById(R.id.yeare);
                        ImageView btnChat = (ImageView) layout.findViewById(R.id.btnLink);
                        imageDialog.setView(layout);
                        btnChat.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                                imageDialog.show().dismiss();
                                Intent i = new Intent(context, Personalchat_activity.class);
                                i.putExtra("receiver_id", getDataAdapter1.getSenderID());
                                i.putExtra("sendername", getDataAdapter1.getReciever_name());
                                i.putExtra("senderimage", getDataAdapter1.getImage());
                                i.putExtra("list", "list");
                                context.startActivity(i);
                                stopTimer();
                                finish();


                            }
                        });


                        ruserImage = getDataAdapter1.getImage();
                        sendername = getDataAdapter1.getReciever_name();
                        majorname = getDataAdapter1.getMajor();
                        username = sp.getString(Constant.username, "");
                        yeares = getDataAdapter1.getYear();

                        String charsn = capitalize(sendername);

                        name.setText("" + charsn);
                        yeare.setText("" + yeares);
                        mjor.setText("" + majorname);

                        Log.e("name", " is " + name);
                        Log.e("yeare", " is " + yeares);
                        Log.e("mjor", " is " + majorname);


                        if (ruserImage.equals("")) {
                            Picasso.with(context)
                                    .load(R.drawable.logo)
                                    .placeholder(R.drawable.main_ic)
                                    .into(image);
                        } else {
                            Picasso.with(context)
                                    .load(Constant.MAIN_URL + ruserImage)
                                    .placeholder(R.drawable.main_ic)
                                    .into(image);
                        }

                        imageDialog.create();
                        imageDialog.show();
                        break;


                    }


                }

            }
        }


    }


}
