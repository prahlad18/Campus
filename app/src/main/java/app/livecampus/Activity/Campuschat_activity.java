package app.livecampus.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 4/12/18.
 */

public class Campuschat_activity extends AppCompatActivity {

    public CircleImageView bookimage;
    public TextView title, tv_campusName;
    public String charsf, charsr, cname;
    public TextView tv_sender;
    SharedPreferences sp;
    RecyclerView listView;
    String campusid;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList = new ArrayList<>();
    ArrayList<DataModel> arrayList_count = new ArrayList<>();
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    String rid;
    Receivemessage asyncFetch;
    int value = -1;
    ImageView senderimage;
    String universityLogo;
    private String messageBody;
    private JSONObject success;
    private boolean loading = true;
    private Button mButtonSend;
    private EditText mEditTextMessage;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.campuschat);
        getSupportActionBar().hide();

        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        tv_campusName = (TextView) findViewById(R.id.tv_campusName);
        senderimage = findViewById(R.id.senderimage);

        Intent intent = getIntent();
        //book_id = intent.getStringExtra("book_id");
//        cname = intent.getStringExtra("campusname");
        campusid = sp.getString(Constant.campus_id, "");
        cname = sp.getString(Constant.StrCampus_name, "");

        listView = (RecyclerView) findViewById(R.id.listview);
        mButtonSend = (Button) findViewById(R.id.btn_send);
        mEditTextMessage = (EditText) findViewById(R.id.et_message);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        listView.setLayoutManager(manager);
        listView.setHasFixedSize(true);

        universityLogo = sp.getString(Constant.campus_image, "");

        Log.e("camous is", sp.getString(Constant.campus_name, ""));
       /* if (sp.getString(Constant.campus_image, "").equalsIgnoreCase("")) {
            Picasso.with(Campuschat_activity.this)
                    .load(R.drawable.user)
                    .into(senderimage);
        } else {
            Picasso.with(Campuschat_activity.this)
                    .load(sp.getString(Constant.campus_image, ""))
                    .into(senderimage);
        }*/
        String temp = universityLogo;
        temp = temp.replaceAll(" ", "%20");
        try {
            URL sourceUrl = new URL(temp);

            if (universityLogo.equalsIgnoreCase("")) {
                Picasso.with(Campuschat_activity.this)
                        .load(R.drawable.user)
                        .into(senderimage);
            } else {
                Picasso.with(Campuschat_activity.this)
                        .load("" + sourceUrl)
                        .into(senderimage);
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        mButtonSend.setAlpha(0.4f);


        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    mButtonSend.setAlpha(1f);

                }
            }


            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() <= 0) {
                    mButtonSend.setAlpha(0.4f);

                }
            }
        });


        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditTextMessage.getText().length() > 1) {

                    messageBody = mEditTextMessage.getText().toString();
                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                    //new Pushnotification().execute();


                } else {


                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");

            }
        });


        asyncFetch = new Receivemessage();
        asyncFetch.execute();
        tv_campusName.setText("" + cname);//cname
        callAsynchronousTask();
        //Log.e("campusname",sp.getString(Constant.StrCampus_name, ""));

    }

    @SuppressLint("StaticFieldLeak")
    private void sendTextMessage(final String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "input_campus_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("rid", sp.getString(Constant.rid, ""));
//                    jo.put("campus", sp.getString(Constant.campus_id, "").toString());
                    jo.put("campus", campusid);
                    jo.put("lid", sp.getString(Constant.uid, ""));
                    jo.put("message", messageBody);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");
                        Log.e("hiii", "hiii");


                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();


                        category.setSenderID("" + sp.getString(Constant.rid, ""));
//                        category.setRecieverID("" + sp.getString(Constant.campus_name,""));
                        category.setMsg("" + messageBody);

                        arrayList.add(category);
                        adapter = new Campuschatinside(Campuschat_activity.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect charactor", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private void stopTimer() {

        asyncFetch.cancel(true);
        if (timerTask != null) {
            //Log.e("cancle","cancle");
            timerTask.cancel();
            //handler.removeCallbacks(timer);

        } else {
            Log.e("notcancle", "notcancle");


        }
    }

    /*public void onStop() {
        super.onStop();
        stopTimer();
        Log.e("stop ","start");

    }*/
    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            asyncFetch = new Receivemessage();
                            asyncFetch.execute();

                            adapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);//500,2000

    }

    @Override
    public void onBackPressed() {

        // super.onBackPressed();

        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        startActivity(i);
        stopTimer();
        asyncFetch.cancel(true);
        //i.putExtra("asyntask",""+asyncFetch);
        finish();

    }

    private String capitalize(String text) {
        if (null != text && text.length() != 0) {
            text = text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();

        }
        return text;
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_campus_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();
                //Thread.sleep(DateUtils.SECOND_IN_MILLIS * 5);
                //jo.put("lid", sp.getString(Constant.uid, ""));
//                jo.put("campus", sp.getString(Constant.campus_id, ""));//userid
                jo.put("campus", campusid);//userid

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();


                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setImage("" + success.getString("SenderImage"));
                                category.setMajor("" + success.getString("MajorName"));
                                category.setYear("" + success.getString("YearName"));
                                category.setDatetime("" + success.getString("DateTime"));
                                category.setReciever_name("" + success.getString("SenderName"));
                                category.setMsg("" + success.getString("Message"));

                                /*arrayList.add(category);
                                listView.scrollToPosition(arrayList.size() - 1);*/

                                arrayList_count.add(category);

                                if (arrayList_count != arrayList) {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new Campuschatinside(Campuschat_activity.this, arrayList);
                                    listView.setAdapter(adapter);

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }

                            }

                        }
                        Log.e("arraylist size", "" + arrayList.size());


                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "campuschat_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("from_user_id", sp.getString(Constant.rid, ""));
                jo.put("sender_name", sp.getString(Constant.username, ""));
                jo.put("lid", sp.getString(Constant.uid, ""));
                jo.put("message", messageBody);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    public class Campuschatinside extends RecyclerView.Adapter<Campuschatinside.ViewHolder> {

        private static final float BLUR_RADIUS = 25f;
        public CircleImageView bookimage;
        public boolean call = true;
        public String userID = "4";
        public Thread t;
        public TextView title;
        public TextView tv_sender;
        public String user_id, reciver_id;
        public String ruserImage, sendername, majorname, yeares, username;
        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;
        RecyclerView listView;
        JSONArray jsonArrayCategory;
        RecyclerView.Adapter adapter;
        ArrayList<DataModel> arrayList;
        ArrayList<DataModel> hs = new ArrayList<>();
        DataModel category;
        GridLayoutManager mLayoutManager;
        AsyncTask<Void, Void, Void> SendData;
        Handler mHandler;
        String book_id, book_name, book_image, rid;
        int value = -1;
        private String messageBody, img;
        private JSONObject success;
        private ProgressDialog pDialog;
        private Button mButtonSend, btn_ok;
        private EditText mEditTextMessage;

        public Campuschatinside(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public Campuschatinside.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.campuschatlist, parent, false);
            Campuschatinside.ViewHolder viewHolder = new Campuschatinside.ViewHolder(v);
            return viewHolder;


        }

        @Override
        public void onBindViewHolder(Campuschatinside.ViewHolder holder, int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username = sp.getString(Constant.username, "");
            String profile = sp.getString(Constant.profile, "");
            //Log.e("image", profile);
            String senderdt = getDataAdapter1.getDatetime();
            String receverdt = getDataAdapter1.getDatetime();
            user_id = sp.getString(Constant.uid, "");
            ruserImage = getDataAdapter1.getImage();


            //String rusername = getDataAdapter1.getReciever_name();

            // String chars = capitalize(username);

            rid = sp.getString(Constant.rid, "");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);
            // holder.tv_sender_name.setVisibility(View.VISIBLE);
            //holder.tv_receiver_name.setVisibility(View.VISIBLE);


            if (ruserImage.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.logo)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.receiver);
            } else {
                Picasso.with(context)
                        .load(Constant.MAIN_URL + ruserImage)
                        .resize(125, 125)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.receiver);
            }

            if (profile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.logo)
                        .placeholder(R.drawable.logo)
                        .into(holder.sender);
            } else {
                Picasso.with(context)
                        .load(profile)
                        .resize(125, 125)
                        .placeholder(R.drawable.logo)
                        .into(holder.sender);
            }

            holder.senderdt.setText(senderdt);
            holder.receiverdt.setText(receverdt);
            if (rid.equals(getDataAdapter1.getSenderID())) {

                String name = getDataAdapter1.getMsg();

                charsf = capitalize(name);
                holder.tv_sender.setText(charsf);
                //holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                //holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                //holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.receiver.setVisibility(View.GONE);
                holder.sender.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);
            } else {
                String name = getDataAdapter1.getMsg();
                charsr = capitalize(name);
                //holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_receiver.setText(charsr);
                holder.tv_sender.setVisibility(View.GONE);
                //holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                //holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.receiver.setVisibility(View.VISIBLE);
                holder.sender.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);

            }


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            String userID = "4";
            TextView tv_sender, tv_receiver, senderdt, receiverdt;
            LinearLayout linearLayout;
            CircleImageView receiver, sender;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                senderdt = (TextView) itemView.findViewById(R.id.senderdt);
                receiverdt = (TextView) itemView.findViewById(R.id.receiverdt);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);
                linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
                receiver = (CircleImageView) itemView.findViewById(R.id.userImage);
                sender = (CircleImageView) itemView.findViewById(R.id.senderimage);


                linearLayout.setOnClickListener(this);


            }

            @Override
            public void onClick(View v) {

                final DataModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());


                switch (v.getId()) {

                    case R.id.linearLayout: {
                        final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context);
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

                        View layout = inflater.inflate(R.layout.popup,
                                (ViewGroup) Campuschat_activity.this.findViewById(R.id.viewgroup));
                        ImageView image = (ImageView) layout.findViewById(R.id.imageFull);
                        TextView name = (TextView) layout.findViewById(R.id.name);
                        TextView mjor = (TextView) layout.findViewById(R.id.mjorname);
                        TextView yeare = (TextView) layout.findViewById(R.id.yeare);
                        ImageView btnChat = (ImageView) layout.findViewById(R.id.btnLink);
                        imageDialog.setView(layout);
                        btnChat.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                                imageDialog.show().dismiss();

                                Log.e("click", "click");
                                Log.e("receiver_id", getDataAdapter1.getSenderID());
                                Log.e("sendername", getDataAdapter1.getReciever_name());
                                Intent i = new Intent(context, Personalchat_activity.class);
                                i.putExtra("receiver_id", getDataAdapter1.getSenderID());
                                i.putExtra("sendername", getDataAdapter1.getReciever_name());
                                i.putExtra("senderimage", getDataAdapter1.getImage());
                                context.startActivity(i);
                                stopTimer();
                                finish();


                            }
                        });


                        ruserImage = getDataAdapter1.getImage();
                        sendername = getDataAdapter1.getReciever_name();
                        majorname = getDataAdapter1.getMajor();
                        username = sp.getString(Constant.username, "");
                        yeares = getDataAdapter1.getYear();

                        String charsn = capitalize(sendername);

                        name.setText("" + charsn);
                        yeare.setText("" + yeares);
                        mjor.setText("" + majorname);

                        if (ruserImage.equals("")) {
                            Picasso.with(context)
                                    .load(R.drawable.logo)
                                    .placeholder(R.drawable.main_ic)
                                    .into(image);
                        } else {
                            Picasso.with(context)
                                    .load(Constant.MAIN_URL + ruserImage)
                                    .placeholder(R.drawable.main_ic)
                                    .into(image);
                        }

                        imageDialog.create();
                        imageDialog.show();


                        break;


                    }


                }


            }

        }


    }


}