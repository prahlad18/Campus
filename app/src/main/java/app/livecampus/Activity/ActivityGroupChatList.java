package app.livecampus.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.GroupModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 6/27/18.
 */

public class ActivityGroupChatList extends AppCompatActivity {

    public TextView txtblub, blubtext, edate, tv_courseName, tvCram;
    public ImageView img_Bulb, img_Calendar;
    public CircleImageView img_book;
    public Button btn_List;
    public String bookimage1, blub, date, course, strCram;
    RecyclerView listView;
    GridLayoutManager mLayoutManager;
    String Prefrence = "Prefrence";
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    SharedPreferences sp;
    ArrayList<GroupModel> arrayList = new ArrayList<>();
    String rid, groupID;
    AsyncTask<Void, Void, Void> groupInfo;
    ProgressDialog pDialog;
    private JSONObject success;
    private Button mButtonSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examchat_namelist);
        getSupportActionBar().hide();


        mButtonSend = (Button) findViewById(R.id.btn_send);
        img_Bulb = (ImageView) findViewById(R.id.img_Bulb);
        img_Calendar = (ImageView) findViewById(R.id.img_Calendar);
        tvCram = (TextView) findViewById(R.id.tvCram);
        tv_courseName = (TextView) findViewById(R.id.tv_courseName);
        img_book = (CircleImageView) findViewById(R.id.img_book);
        btn_List = (Button) findViewById(R.id.btn_List);


        Intent i = getIntent();
        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);

        if (i.hasExtra("groupid")) {
            groupID = i.getStringExtra("groupid");

            Log.e("groupid is", "ok " + groupID);
            getGroupInfo("" + groupID);

        }

        if (sp.getString("groupImage", "").equalsIgnoreCase("")) {
            Picasso.with(ActivityGroupChatList.this).load(R.drawable.main_ic).placeholder(R.drawable.main_ic).into(img_book);

        } else {
            Picasso.with(ActivityGroupChatList.this).load(sp.getString("groupImage", "")).placeholder(R.drawable.main_ic).into(img_book);

        }
        /*blub = sp.getString("blub","");
        date = sp.getString("date","");
        course = sp.getString("cource","");
        strCram = sp.getString("cram","");

*/


        listView = (RecyclerView) findViewById(R.id.listview);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);


        img_Bulb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(v.getContext());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.blubpopup);

                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                //TextView btnback = (TextView) dialog.findViewById(R.id.btnback);
                Button exam = (Button) dialog.findViewById(R.id.exam);
                blubtext = (TextView) dialog.findViewById(R.id.blubtext);
                Button assigment = (Button) dialog.findViewById(R.id.assigment);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                blubtext.setText(blub);

                dialog.show();


            }
        });
        img_Calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(v.getContext());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.datepopup);

                ImageView image = (ImageView) dialog.findViewById(R.id.image);
                //TextView btnback = (TextView) dialog.findViewById(R.id.btnback);
                Button exam = (Button) dialog.findViewById(R.id.exam);
                edate = (TextView) dialog.findViewById(R.id.edate);
                Button assigment = (Button) dialog.findViewById(R.id.assigment);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                edate.setText(date);

                dialog.show();
            }
        });
        new Receivemessage().execute();


    }

    @SuppressLint("StaticFieldLeak")
    private void getGroupInfo(final String s) {

        groupInfo = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(ActivityGroupChatList.this);
                pDialog.setMessage("Please wait...");
                pDialog.show();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "group_info.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("group_id", groupID);

                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                {
                    if (pDialog.isShowing())
                        pDialog.dismiss();


                    try {


                        if (success.getString("success").equals("1")) {
                            jsonArrayCategory = success.getJSONArray("posts");
                            for (int i = 0; i < jsonArrayCategory.length(); i++) {
                                success = jsonArrayCategory.getJSONObject(i);

                                bookimage1 = success.getString("images");
                                blub = success.getString("blub");
                                date = success.getString("date");
                                course = success.getString("coursename");
                                strCram = success.getString("status");

                                tv_courseName.setText("" + course);
                                tvCram.setText("" + strCram);


                            }
                            Log.e("arraylist size", "" + arrayList.size());
                            pDialog.dismiss();


                        } else if (success.getString("success").equals("0")) {
                            // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                            pDialog.dismiss();

                        }

                    } catch (JSONException e) {
                        System.out.println("the title exeption is :" + e);
                        pDialog.dismiss();

                        e.printStackTrace();
                    }
                }
            }
        };
        groupInfo.execute();
    }

    private String capitalize(final String text) {

        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }

    @Override
    public void onBackPressed() {
    /*    super.onBackPressed();
        finish();*/
        Intent i = new Intent(getApplicationContext(), ActivityGroupChat.class);
        startActivity(i);
        finish();


    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "group_members.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("group_id", sp.getString("groupid", ""));//userid

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("post");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {


                            GroupModel category = new GroupModel();

                            success = jsonArrayCategory.getJSONObject(i);
                            category.setRid("" + success.getString("rid"));
                            category.setProfile_name("" + success.getString("profile_name"));
                            category.setProfile_image("" + success.getString("profile_image"));
                            category.setMajors("" + success.getString("majors"));
                            category.setYear("" + success.getString("year"));

                            arrayList.add(category);


                        }
                        Log.e("arraylist size", "" + arrayList.size());
                        btn_List.setText("" + arrayList.size());
                        adapter = new RecyclerViewAdapter_chat(ActivityGroupChatList.this, arrayList);
                        listView.setAdapter(adapter);

                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(ActivityGroupChatList.this, "" + success.getString("post"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<GroupModel> getDataAdapter;
        SharedPreferences sp;
        String buyerid;

        public RecyclerViewAdapter_chat(Context context, ArrayList<GroupModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_chat_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter_chat.ViewHolder holder, final int position) {

            GroupModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String user_id = sp.getString(Constant.rid, "");
            String rusername = getDataAdapter1.getProfile_name();
            String charsr = capitalize(rusername);

            String strProfile = getDataAdapter1.getProfile_image();
            String strMajor = getDataAdapter1.getMajors();
            String strYear = getDataAdapter1.getYear();
            if (user_id.equalsIgnoreCase(getDataAdapter1.getRid()))

            {
                holder.tv_UserName.setText("You");
                holder.tv_MajorName.setText("" + strMajor);
                holder.tv_YearName.setText("" + strYear);
                holder.imgChat.setVisibility(View.INVISIBLE);
                holder.imgChat.setEnabled(true);
            } else {
                holder.tv_UserName.setText("" + charsr);
                holder.tv_MajorName.setText("" + strMajor);
                holder.tv_YearName.setText("" + strYear);

            }


            Log.e("userImage", strProfile);
            if (strProfile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.user1)
                        .placeholder(R.drawable.user1)
                        .into(holder.circleImageView);
            } else {
                Picasso.with(context)
                        .load(Constant.MAIN_URL + strProfile)
                        .placeholder(R.drawable.user1)
                        .into(holder.circleImageView);
            }

        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView tv_UserName, tv_MajorName, tv_YearName;
            ImageView imgChat;
            CircleImageView circleImageView;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_UserName = (TextView) itemView.findViewById(R.id.tv_UserName);
                tv_MajorName = (TextView) itemView.findViewById(R.id.tv_MajorName);
                tv_YearName = (TextView) itemView.findViewById(R.id.tv_YearName);

                circleImageView = (CircleImageView) itemView.findViewById(R.id.userImage);
                imgChat = (ImageView) itemView.findViewById(R.id.imgChat);

                imgChat.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                GroupModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());


                switch (v.getId()) {
                    case R.id.imgChat:
                        buyerid = getDataAdapter1.getRid();
                        Log.e("recever_is", buyerid);

                        new RecyclerViewAdapter_chat.Clearenotification().execute();
                        String recevername = capitalize(getDataAdapter1.getProfile_name());
                        Intent i = new Intent(context, Personalchat_activity.class);//Msg_actvity_fromChat
                        i.putExtra("receiver_id", getDataAdapter1.getRid());
                        i.putExtra("sendername", recevername);
                        i.putExtra("senderimage", getDataAdapter1.getProfile_image());
                        i.putExtra("list", "list");
                        context.startActivity(i);
                        finish();

                        break;

//                    http://www.smartbaba.in/liveatcampus/api
                }
            }
        }


        private class Clearenotification extends AsyncTask<Void, Void, Void> {


            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "clear_push_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);


                try {
                    JSONObject jo = new JSONObject();


                    jo.put("rid", buyerid);
                    //jo.put("book_id", book_id);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                {
                    super.onPostExecute(result);
                    try {


                        if (success.getString("success").equals("1")) {


                        } else if (success.getString("success").equals("0")) {
                            // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        System.out.println("the title exeption is :" + e);
                        e.printStackTrace();
                    }
                }


            }
        }
    }

}

