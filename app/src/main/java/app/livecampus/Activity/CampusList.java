package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.CampusListPojo;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 6/27/18.
 */

public class CampusList extends AppCompatActivity implements View.OnClickListener {

    public TextView txtblub, blubtext, edate, tv_courseName, tvCram, txt_NoData;
    public ImageView img_Back, img_Calendar;
    public CircleImageView img_book;
    public String bookimage1, blub, date, course, strCram;
    RecyclerView listView;
    GridLayoutManager mLayoutManager;
    String Prefrence = "Prefrence";
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    SharedPreferences sp;
    ArrayList<CampusListPojo> arrayList = new ArrayList<>();
    String rid, groupID, strUid;
    ProgressDialog pDialog;
    private JSONObject success;
    private Button btn_Home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groupchat);
        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

        strUid = sp.getString(Constant.uni_ID, "");

        getSupportActionBar().hide();
        listView = (RecyclerView) findViewById(R.id.recycler_view);
        txt_NoData = (TextView) findViewById(R.id.txt_NoData);
        img_Back = findViewById(R.id.img_Back);
        btn_Home = findViewById(R.id.btn_Home);

        mLayoutManager = new GridLayoutManager(CampusList.this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);
        img_Back.setOnClickListener(this);
        btn_Home.setOnClickListener(this);

        new Receivemessage().execute();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Home:
                Intent i = new Intent(CampusList.this, HomeActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.img_Back:
                Intent intent = new Intent(CampusList.this, HomeActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CampusList.this);
            pDialog.setMessage("Please wait...");
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "get_campus_list.php";
            System.out.println("the url is :" + serverUrl);
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                jo.put("campusname", sp.getString(Constant.campus_id, ""));//userid

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                if (pDialog.isShowing())
                    pDialog.dismiss();
                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            CampusListPojo category = new CampusListPojo();

                            success = jsonArrayCategory.getJSONObject(i);
                            category.setRid("" + success.getString("rid"));
                            category.setCamp_id("" + success.getString("camp_id"));
                            category.setCamp_name("" + success.getString("camp_name"));
                            category.setImage("" + success.getString("image"));
                            category.setUnivercity_name("" + success.getString("univercity_name"));
                            category.setUsername("" + success.getString("username"));
                            category.setUid("" + success.getString("uid"));
                            category.setMajorname("" + success.getString("majors_name"));
                            category.setMid("" + success.getString("mid"));
                            category.setUniversityLogo("" + success.getString("univercity_logo"));


                            if (strUid.equalsIgnoreCase(success.getString("uid"))) {
                                arrayList.add(category);
                            } else {

                            }


                        }
                        pDialog.dismiss();

                        adapter = new RecyclerViewAdapter_chat(CampusList.this, arrayList);
                        listView.setAdapter(adapter);

                    } else if (success.getString("success").equals("0")) {
                        txt_NoData.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        pDialog.dismiss();

                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                    pDialog.dismiss();

                }
            }

        }
    }

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<CampusListPojo> getDataAdapter;
        SharedPreferences sp;
        String buyerid;
        String strGroupName, strGroupID;

        public RecyclerViewAdapter_chat(Context context, ArrayList<CampusListPojo> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.campus_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter_chat.ViewHolder holder, final int position) {

            CampusListPojo getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String image = getDataAdapter1.getImage();
            holder.tv_campusName.setText("" + getDataAdapter1.getCamp_name());
            holder.tv_University.setText("" + getDataAdapter1.getUnivercity_name());
            holder.tv_UserName.setText("" + getDataAdapter1.getUsername());
            holder.tv_MajorName.setText("" + getDataAdapter1.getMajorname());
            holder.itemView.setVisibility(View.VISIBLE);

            if (image.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.main_ic)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.groupImage);
            } else {
                Picasso.with(context)
                        .load(image)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.groupImage);
            }


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CircleImageView groupImage;
            TextView tv_UserName, tv_University, tv_campusName, tv_MajorName;

            public ViewHolder(View itemView) {

                super(itemView);
                groupImage = (CircleImageView) itemView.findViewById(R.id.groupImage);
                tv_UserName = (TextView) itemView.findViewById(R.id.tv_UserName);
                tv_University = (TextView) itemView.findViewById(R.id.tv_University);
                tv_campusName = (TextView) itemView.findViewById(R.id.tv_campusName);
                tv_MajorName = (TextView) itemView.findViewById(R.id.tv_MajorName);


                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CampusListPojo getDataAdapter1 = getDataAdapter.get(getAdapterPosition());

                        buyerid = getDataAdapter1.getCamp_id();
                        Intent i = new Intent(context, Campuschat_activity_all.class);//Msg_actvity_fromChat
                        i.putExtra("campusid", getDataAdapter1.getCamp_id());
                        i.putExtra("cname", getDataAdapter1.getCamp_name());
                        i.putExtra("uid", getDataAdapter1.getUid());
                        i.putExtra("ulogo", getDataAdapter1.getUniversityLogo());
                        i.putExtra("campuslist", "campuslist");
                        context.startActivity(i);
                    }
                });


            }
        }


    }


}

