package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;
//import app.livecampus.adapter.RecyclerViewAdapter_chat;

public class Msg_actvity extends AppCompatActivity {

    public CircleImageView bookimage;
    public Thread t;
    public TextView title1;
    public TextView tv_sender;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList, arrayList_count;
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    Handler mHandler;
    String book_id, book_name, book_image, rid;
    int value = -1;
    int value_for_rec = 1;
    Receivemessage asyncFetch;
    private String messageBody;
    private JSONObject success;
    private ProgressDialog pDialog;
    private Button mButtonSend, btn_ok;
    private EditText mEditTextMessage;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainchat);
        getSupportActionBar().hide();
        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

        listView = (RecyclerView) findViewById(R.id.listview);

        mButtonSend = (Button) findViewById(R.id.btn_send);
        bookimage = (CircleImageView) findViewById(R.id.bookimage);
        title1 = (TextView) findViewById(R.id.title1);
        // get the reference of Toolbar
        Intent intent = getIntent();
        //book_id = intent.getStringExtra("book_id");
        rid = intent.getStringExtra("user_chk");
        Log.e("receiverid", rid);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);

        book_name = sp.getString("bookName", "");
        book_id = sp.getString("book_id", "");
        book_image = sp.getString("bookImage", "");
        mEditTextMessage = (EditText) findViewById(R.id.et_message);

        if (book_name.length() > 19) {

            book_name = book_name.substring(0, 18) + "...";

            title1.setText(book_name);
        } else {

            title1.setText(book_name); //Dont do any change

        }

        if (rid.equals(sp.getString(Constant.rid, ""))) {
            Intent intent1 = new Intent(Msg_actvity.this, Chatnamelist.class);
            startActivity(intent1);
            finish();
        } else {
            //callAsynchronousTask();

        }
        mButtonSend.setAlpha(0.4f);

        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    mButtonSend.setAlpha(1f);

                }
            }


            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() <= 0) {
                    mButtonSend.setAlpha(0.4f);

                }
            }
        });

//        btn_ok = (Button) findViewById(R.id.btn_ok);

/*
        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);*/


        //setTitle(""+book_name);
        // title1.setText(book_name+"....");
        //title1.setMinEms(5);
        if (book_image.equals("")) {
            Picasso.with(this)
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.main_ic)
                    .into(bookimage);
        } else {
            Picasso.with(this)
                    .load(book_image)
                    .placeholder(R.drawable.main_ic)
                    .into(bookimage);
        }
        arrayList = new ArrayList<>();
        arrayList_count = new ArrayList<>();

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String message = mEditTextMessage.getText().toString();

                // Log.e("hiii", String.valueOf(mListView));


                if (mEditTextMessage.getText().length() > 1) {
                    messageBody = mEditTextMessage.getText().toString();
                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                    new Pushnotification().execute();


                } else {

                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");

            }
        });
        callAsynchronousTask();
        asyncFetch = new Receivemessage();
        asyncFetch.execute();

    }

    private void sendTextMessage(String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("rid", sp.getString(Constant.rid, ""));
                    jo.put("book_id", book_id);
                    jo.put("receiver_id", rid);
                    jo.put("message", messageBody);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");
                        Log.e("hiii", "hiii");


                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();


                        category.setSenderID("" + sp.getString(Constant.rid, ""));
                        category.setRecieverID("" + book_id);
//                        category.setReciever_name("" + success.getString("Name"));
                        category.setMsg("" + messageBody);

                        arrayList.add(category);
                        adapter = new RecyclerViewAdapter_chat(Msg_actvity.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect UserName and Password", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            new Receivemessage().execute();

                            adapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 0, 2000);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (asyncFetch != null && asyncFetch.getStatus() == AsyncTask.Status.RUNNING) {
            asyncFetch.cancel(true);
            Log.e("back", "back");
        }

        stopTimer();

    }


    //To start timer

    //To stop timer
    private void stopTimer() {

        if (timerTask != null) {
            Log.e("cancle", "cancle");
            timerTask.cancel();
            //handler.removeCallbacks(timer);

        } else {
            Log.e("notcancle", "notcancle");


        }
    }

    /*public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder>  {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;
        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;





        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username=sp.getString(Constant.username,"");


            String rusername=getDataAdapter1.getReciever_name();

            String chars = capitalize(username);
            //String rchars = capitalize(rusername);

            String rid= sp.getString(Constant.rid,"");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);
            holder.tv_sender_name.setVisibility(View.VISIBLE);
            holder.tv_receiver_name.setVisibility(View.VISIBLE);



            if (rid.equals(getDataAdapter1.getSenderID()))
            {
                String name=getDataAdapter1.getMsg();
                name=messageBody;
                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :"+chars);
                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);

            }
            else
            {
                holder.tv_receiver_name.setText(rusername+": ");
                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);


            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }
        public class ViewHolder extends RecyclerView.ViewHolder{
            String userID="4";
            TextView tv_sender,tv_receiver,tv_receiver_name,tv_sender_name;
            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
                tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);


            }
        }

    }*/
    public void onStop() {
        super.onStop();
        stopTimer();
        Log.e("stop ", "start");

    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", book_id);
                jo.put("rid", sp.getString(Constant.rid, ""));//userid
                jo.put("receiver_id", rid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();


                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setRecieverID("" + success.getString("BookId"));
                                category.setReciever_name("" + success.getString("Name"));
                                category.setMsg("" + success.getString("Message"));

//                                arrayList.add(category);

                                arrayList_count.add(category);

                                if (arrayList_count != arrayList) {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new RecyclerViewAdapter_chat(Msg_actvity.this, arrayList);
                                    listView.setAdapter(adapter);
                                    //adapter.notifyDataSetChanged();

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }

                            }


                        }
                        Log.e("arraylist size", "" + arrayList.size());

//                        listView.scrollToPosition(arrayList.size() - 1);




                        /*listView.addOnScrollListener(new CustomScrollListener());
                        listView.scrollToPosition(arrayList.size()-1);*/

                        //adapter.notifyDataSetChanged();

                       /* listView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                //callAsynchronousTask();
                                if (dy > 0) {

//                                    stopTimer();
                                    // Scrolling up
                                } else if(dy < 0){
//                                    stopTimer();
                                    // Scrolling down
                                }

                            }

                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);

                                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                                    // Do something
                                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                                    // Do something
                                } else {
                                    // Do something
                                }
                            }
                        });
*/

                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "bookchat_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", sp.getString(Constant.rid, ""));
                jo.put("book_id", book_id);
                jo.put("receiver_id", rid);
                jo.put("message", messageBody);
                //jo.put("sender_name",sp.getString(Constant.username,""));


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<Msg_actvity.RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public RecyclerViewAdapter_chat.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            RecyclerViewAdapter_chat.ViewHolder viewHolder = new RecyclerViewAdapter_chat.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter_chat.ViewHolder holder, final int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username = sp.getString(Constant.username, "");
            String rusername = getDataAdapter1.getReciever_name();
            String chars = capitalize(username);
            //String rchars = capitalize(rusername);

            String rid = sp.getString(Constant.rid, "");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);
            holder.tv_sender_name.setVisibility(View.VISIBLE);
            holder.tv_receiver_name.setVisibility(View.VISIBLE);


            if (rid.equals(getDataAdapter1.getSenderID())) {
                String name = getDataAdapter1.getMsg();
                name = messageBody;
                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);
                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);

            } else {
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);


            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            String userID = "4";
            TextView tv_sender, tv_receiver, tv_receiver_name, tv_sender_name;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
                tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);


            }
        }

    }


}
