package app.livecampus.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

public class Exam_Sell_Cram_Activity extends AppCompatActivity {

    ImageView s1, s2;
    TextView txt_login;
    SharedPreferences.Editor editor;
    private CircleImageView circleImageView;
    private SharedPreferences app_sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.exam_sell_cram);
        app_sharedPreferences = getApplicationContext().getSharedPreferences("livecampus", Context.MODE_PRIVATE);

        SharedPreferences app_preferences = getSharedPreferences("livecampus", MODE_PRIVATE);
        editor = app_preferences.edit();

        getSupportActionBar().hide();
        s1 = (ImageView) findViewById(R.id.image_cram);
        s2 = (ImageView) findViewById(R.id.image_sell);
        circleImageView = (CircleImageView) findViewById(R.id.cir_img_profile);

        Picasso.with(getApplicationContext())
                .load(app_preferences.getString(Constant.profile, ""))
                .into(circleImageView);


        s1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), ActivityExam_Cram.class);
                startActivity(i); //pp
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        s2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               /* Intent i=new Intent(getApplicationContext(),SellTextActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/


            }
        });

    }
}
