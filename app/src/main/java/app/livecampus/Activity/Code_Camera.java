package app.livecampus.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.frosquivel.magicaltakephoto.MagicalTakePhoto;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.livecampus.R;

public class Code_Camera extends Activity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static File imagePath;
    IntentIntegrator integrator;
    Activity mActivity;
    String frm;
    ImageView imageView;
    int mWidth = 50, mHeight = 50, mLeft = 50, mTop = 50, mAreaWidth = 50, mAreaHeight = 50;
    MagicalTakePhoto magicalTakePhoto;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageView = (ImageView) findViewById(R.id.imageView);
        checkAndRequestPermissions();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    101);
            return;
        }
        magicalTakePhoto = new MagicalTakePhoto(this);
        scan();
        //takeScreenshot();
        //setContentView(R.layout.activity_main);

        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //      WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    public void scan() {
        integrator = new IntentIntegrator(this);
        integrator.setPrompt("Scan a barcode or QRcode");
        Intent intent = new Intent(String.valueOf(integrator.setCameraId(0)));
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(true);
        integrator.setOrientationLocked(false);
        imagePath = new File(android.os.Environment.getExternalStorageDirectory().toString() + "/" + "one.jpeg");
        //String mPath = android.os.Environment.getExternalStorageDirectory().toString() + "/" +"one.png";

        integrator.initiateScan();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imagePath);

        /* startActivityForResult(intent, 1);
        finish();*/
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
        //Toast.makeText(getApplicationContext(),"hello",Toast.LENGTH_LONG).show();

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + "temp" + ".png";

            // create bitmap screen capture
            View v1 = null;

            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int READ = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;


    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    public void saveBitmap(Bitmap bitmap) {
        imagePath = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
        //Toast.makeText(getApplicationContext(),"Insert image",Toast.LENGTH_LONG).show();
    }

    private void shareIt() {
        Uri uri = Uri.fromFile(imagePath);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("image/*");
        String shareBody = frm;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Age Count");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(getApplicationContext(),"1",Toast.LENGTH_LONG).show();

        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        magicalTakePhoto.resultPhoto(requestCode, resultCode, data);
        //Toast.makeText(getApplicationContext(),""+magicalTakePhoto, Toast.LENGTH_LONG).show();
        frm = result.getContents();

        if (result != null) {
            if (result.getContents() == null) {
                //llSearch.setVisibility(View.GONE);

                //Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {


            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            magicalTakePhoto.resultPhoto(requestCode, resultCode, data);
            imageView.setImageBitmap(magicalTakePhoto.getMyPhoto());
        }

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(Code_Camera.this, MainActivity.class);
        startActivity(i);
        super.onBackPressed();
    }
}
