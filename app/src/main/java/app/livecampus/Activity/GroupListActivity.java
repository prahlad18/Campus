package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.GroupModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 6/27/18.
 */

public class GroupListActivity extends AppCompatActivity {

    RecyclerView listView;
    GridLayoutManager mLayoutManager;
    String Prefrence = "Prefrence";
    JSONArray jsonArrayCategory;
    private JSONObject success;
    RecyclerView.Adapter adapter;
    SharedPreferences sp;
    ArrayList<GroupModel> arrayList = new ArrayList<>();
    String rid, groupID, value;
    public TextView txt_NoData, blubtext, edate, tv_courseName, tvCram;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_notification_activity);
        getSupportActionBar().setTitle("Cram List");

        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);

        listView = (RecyclerView) findViewById(R.id.listview);
        txt_NoData = (TextView) findViewById(R.id.txt_NoData);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);


        new Receivemessage().execute();

    }


    public class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(GroupListActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "rid_group.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("rid",sp.getString(Constant.rid,""));//userid

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                if (pDialog.isShowing())
                    pDialog.dismiss();

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            GroupModel category = new GroupModel();

                            success = jsonArrayCategory.getJSONObject(i);
                            category.setGroupID("" + success.getString("groupid"));
                            category.setGruopName("" + success.getString("groupname"));
                            category.setBulb("" + success.getString("groupblub"));
                            category.setDate("" + success.getString("groupdate"));
                            category.setStatus("" + success.getString("returnstatus"));
                            category.setGroupImage("" + success.getString("groupimage1"));
                            category.setJoin_status("" + success.getString("status"));

                            if (success.getString("status").equalsIgnoreCase("0"))
                            {
                                arrayList.add(category);
                            }
                            /*else
                            {
                                Log.e("arraylist "," size  ggg");
                                listView.setVisibility(View.GONE);
                                txt_NoData.setVisibility(View.VISIBLE);
                            }*/


                        }
                        adapter = new MyAdapter(GroupListActivity.this, arrayList);
                        listView.setAdapter(adapter);
                        pDialog.dismiss();


                    } else if (success.getString("success").equals("0")) {

                        listView.setVisibility(View.GONE);
                        txt_NoData.setVisibility(View.VISIBLE);
                        pDialog.dismiss();

//                         Toast.makeText(ActivityNotificationList.this, "No group requst", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                    pDialog.dismiss();

                }
            }

        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        //respond to menu item selection
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent=new Intent(GroupListActivity.this,HomeActivity.class);
                startActivity(intent);
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>  {
        ProgressDialog pDialog;
        Context context;
        ArrayList<GroupModel> getDataAdapter;
        SharedPreferences sp;


        public MyAdapter(Context context, ArrayList<GroupModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_group_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }




        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            GroupModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String strProfile =getDataAdapter1.getGroupImage();

            holder.mGroupNameTv.setText("" + getDataAdapter1.getGruopName());
            holder.mGroupBlubTv.setText("" + getDataAdapter1.getBulb());
            holder.mGroupStatusTv.setText("" + getDataAdapter1.getStatus());
            holder.mGroupDateTv.setText("" + getDataAdapter1.getDate());
            holder.mGroupBlubTv.setText("" + getDataAdapter1.getBulb());



                Log.e("if part"," status is "+getDataAdapter1.getJoin_status());
                holder.itemView.setVisibility(View.VISIBLE);

                if (getDataAdapter1.getStatus().equalsIgnoreCase("Exam"))
                {
                    holder.linearColor.setBackgroundColor(getResources().getColor(R.color.red));
                }
                else
                {
                    holder.linearColor.setBackgroundColor(getResources().getColor(R.color.lightblue));

                }

            if (strProfile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.main_ic)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.mGroupImage);
            } else if (strProfile.contains("https://")) {
                Picasso.with(context)
                        .load(strProfile)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.mGroupImage);
            } else {
                Picasso.with(context)
                        .load(Constant.MAIN_URL + strProfile)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.mGroupImage);
            }






        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }



        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView mGroupImage;
            private TextView mGroupNameTv;
            private TextView mGroupBlubTv;
            private TextView mGroupStatusTv;
            private TextView mGroupDateTv;
            private Button mBtnAccept;
            private LinearLayout linearColor,linearMain;
            public ViewHolder(View itemView) {

                super(itemView);
                mGroupImage = (ImageView) itemView.findViewById(R.id.groupImage);
                mGroupNameTv = (TextView) itemView.findViewById(R.id.tv_groupName);
                mGroupBlubTv = (TextView) itemView.findViewById(R.id.tv_groupBlub);
                mGroupStatusTv = (TextView) itemView.findViewById(R.id.tv_groupStatus);
                mGroupDateTv = (TextView) itemView.findViewById(R.id.tv_groupDate);
                mBtnAccept = (Button) itemView.findViewById(R.id.btnAccept);
                linearColor = (LinearLayout) itemView.findViewById(R.id.linearColor);
                linearMain = (LinearLayout) itemView.findViewById(R.id.linearMain);


                mBtnAccept.setOnClickListener(this);


            }

            @Override
            public void onClick(View v) {
                GroupModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());

                switch (v.getId()) {
                    case R.id.btnAccept:
                        value = "1";
                        groupID = getDataAdapter1.getGroupID();

                        new AcceptDecline().execute();

                        break;


                }
            }
        }


        private class AcceptDecline extends AsyncTask<Void, Void, Void> {


            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(true);
                pDialog.show();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "invite_request.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);


                try {
                    JSONObject jo = new JSONObject();

                    jo.put("rid", sp.getString(Constant.rid, ""));
                    jo.put("status", "Update");
                    jo.put("group_id", "" + groupID);
                    jo.put("value", value);

                    System.out.println("the send invite data " + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                    pDialog.dismiss();

                } catch (Exception e) {
                    pDialog.dismiss();

                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (pDialog.isShowing())
                    pDialog.dismiss();

                try {


                    if (success.getString("success").equals("1")) {
                        adapter.notifyDataSetChanged();
                        if (value.equalsIgnoreCase("1")) {
                            Intent invite = new Intent(context, Chat_Tabbar.class);
                            invite.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(invite);
                            finish();
                        } else {
                            Intent decline = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(decline);
                            finish();
                        }

                        Log.e("arraylist size", "" + arrayList.size());


                    } else if (success.getString("success").equals("0")) {
                        pDialog.dismiss();

                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the invite exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }


    @Override
    public void onBackPressed() {

        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(i);
        finish();


    }

}

