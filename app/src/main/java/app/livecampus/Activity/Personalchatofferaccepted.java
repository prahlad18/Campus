package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import app.livecampus.Fragments.SellTextFragment;
import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 5/31/18.
 */

public class Personalchatofferaccepted extends AppCompatActivity {

    private static final long delay = 2000L;
    public CircleImageView sendrimage;
    public ImageView imageBook;
    public Thread t;
    public TextView title;
    public TextView tv_sender, bookname, offer;
    public Intent intent;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList, arrayList_count;
    ArrayList<DataModel> hs = new ArrayList<>();
    boolean backpress = false;
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    Handler mHandler;
    String book_id, book_name, book_image, rid, offeract, offerreject, receiver_id, sendername, senderimage, checktrue, senderimage1, charnm, searchuser, book_price;
    int value = -1;
    Receivemessage asyncFetch;
    private String messageBody, img;
    private JSONObject success;
    private boolean backPressedOnce = false;
    private Handler statusUpdateHandler;
    private Runnable statusUpdateRunnable;
    private ProgressDialog pDialog;
    private Button mButtonSend, btn_ok;
    private EditText mEditTextMessage;
    private boolean mRecentlyBackPressed = false;
    private Handler mExitHandler = new Handler();
    private Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personalchatofferaccepted);
        getSupportActionBar().hide();
        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

        arrayList = new ArrayList<>();
        arrayList_count = new ArrayList<>();
        listView = (RecyclerView) findViewById(R.id.listview);
        mButtonSend = (Button) findViewById(R.id.btn_send);
        sendrimage = (CircleImageView) findViewById(R.id.senderimage);
        title = (TextView) findViewById(R.id.title);
        /*final DataModel model = new DataModel();

        Log.e("nm",model.getBookName());*/


        bookname = (TextView) findViewById(R.id.bookname);
        offer = (TextView) findViewById(R.id.title1);
        imageBook = (ImageView) findViewById(R.id.image);
        // get the reference of Toolbar
        intent = getIntent();

        receiver_id = intent.getStringExtra("receiver_id");
        sendername = intent.getStringExtra("sendername");
        senderimage = intent.getStringExtra("senderimage");
        checktrue = intent.getStringExtra("counter");
        //searchuser = intent.getStringExtra("searchuser");


        book_id = intent.getStringExtra("book_id");
        book_name = intent.getStringExtra("name");
        book_image = intent.getStringExtra("image");
        book_price = intent.getStringExtra("price");
        rid = intent.getStringExtra("buyer_id");
        offeract = intent.getStringExtra("offer_acceted");
        offerreject = intent.getStringExtra("offer_rejected");


       /* offer.setText(offeract);
        if(offer.getText().toString().equals(offeract)){
            offer.setTextColor(getResources().getColor(R.color.lime_lite));

            new Buybook().execute();
            //Log.e("offer accepted","offer accepted");

        }else{
            offer.setText(offerreject);
            offer.setTextColor(getResources().getColor(R.color.red));

            //Log.e("offer rejected","offer rejected");

        }*/


        String chars = capitalize(sendername);

        title.setText("" + chars);


//        btn_ok = (Button) findViewById(R.id.btn_ok);
        mEditTextMessage = (EditText) findViewById(R.id.et_message);


       /* mEditTextMessage.setInputType(
                InputType.TYPE_CLASS_TEXT|
                        InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        );*/

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);


        mButtonSend.setAlpha(0.4f);


        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    mButtonSend.setAlpha(1f);

                }
            }


            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() <= 0) {
                    mButtonSend.setAlpha(0.4f);

                }
            }
        });


        if (senderimage.equals("")) {
            Picasso.with(this)
                    .load(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .into(sendrimage);
        } else {
            Picasso.with(this)
                    .load(senderimage)
                    .placeholder(R.drawable.user)
                    .into(sendrimage);
        }

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //String message = mEditTextMessage.getText().toString();

                // Log.e("hiii", String.valueOf(mListView));


                if (mEditTextMessage.getText().length() > 1) {
                    messageBody = mEditTextMessage.getText().toString();
                    charnm = capitalize(sp.getString(Constant.username, ""));

                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                    new Pushnotification().execute();


                } else {

                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");

            }
        });


        asyncFetch = new Receivemessage();
        asyncFetch.execute();

        callAsynchronousTask();

    }

    private void sendTextMessage(String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "input_personal_chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();

                    jo.put("from_user_id", sp.getString(Constant.rid, ""));
                    jo.put("to_user_id", receiver_id);
                    jo.put("message", messageBody);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");


                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();


                        category.setSenderID("" + sp.getString(Constant.rid, ""));
                        category.setRecieverID("" + receiver_id);
                        category.setMsg("" + messageBody);

                        arrayList.add(category);
                        adapter = new RecyclerViewAdapter_chat(Personalchatofferaccepted.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect Charactor", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            new Receivemessage().execute();
                            adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);// 1 second

    }

    @Override
    public void onBackPressed() {


        if (asyncFetch != null && asyncFetch.getStatus() == AsyncTask.Status.RUNNING) {
            asyncFetch.cancel(true);


        }
       /* else if (intent.hasExtra("counter")) {
            stopTimer();
            asyncFetch.cancel(true);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            SellAndReject addPriceOfBookFragment=new SellAndReject();
            Bundle bundle = new Bundle();
            //bundle.putBoolean("BOOLEAN_VALUE",false);
            addPriceOfBookFragment.setArguments(bundle);
            ft.replace(R.id.container,addPriceOfBookFragment,"Sell");
            ft.commit();
            finish();
        }*/


        else {

            stopTimer();
            asyncFetch.cancel(true);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            SellTextFragment addPriceOfBookFragment = new SellTextFragment();
            Bundle bundle = new Bundle();
            //bundle.putBoolean("BOOLEAN_VALUE",false);
            addPriceOfBookFragment.setArguments(bundle);
            ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
            ft.commit();


            /*
            Intent i = new Intent(Personalchatofferaccepted.this, Campuschat_activity.class);
            startActivity(i);
            stopTimer();
            asyncFetch.cancel(true);
            Log.e("back2","back2");
            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
            finish();*/


        }
    }

    //To stop timer
    private void stopTimer() {

        if (timerTask != null) {
            Log.e("cancle", "cancle");
            timerTask.cancel();
            //handler.removeCallbacks(timer);

        } else {
            Log.e("notcancle", "notcancle");


        }
    }


    //To start timer

    private String capitalize(final String text) {

        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }


 /*   @Override
    public void onBackPressed() {
        super.onBackPressed();
        if ( asyncFetch != null && asyncFetch.getStatus() == AsyncTask.Status.RUNNING ){
            asyncFetch.cancel(true);
            Log.e("back","back");

        }
        Intent i = new Intent(Personalchat_activity.this, Campuschat_activity.class);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        this.finish();
        stopTimer();


    }*/

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_personal_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("from_user_id", sp.getString(Constant.rid, ""));
                jo.put("to_user_id", receiver_id);


                System.out.println("the send data is from this:" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();


                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setRecieverID("" + success.getString("ReceiverId"));
                                // category.setReciever_name("" + success.getString("Name"));
                                category.setMsg("" + success.getString("Message"));
                                category.setBookName("" + success.getString("BookName"));
                                category.setBookImage("" + success.getString("BookImage"));
                                category.setStatus("" + success.getString("Status"));


                                Log.e("booknm", category.getBookName());
                                Log.e("bookimg", category.getBookImage());
                                Log.e("status", category.getStatus());

                                String bnm = category.getBookName();
                                String bim = category.getBookImage();
                                String st = category.getStatus();


                                if (st.equals("Accept")) {
                                    offer.setTextColor(getResources().getColor(R.color.lime_lite));
                                    offer.setText("Offer Accepted:");
                                    new Buybook().execute();
                                    //Log.e("offer accepted","offer accepted");

                                } else if (st.equals("Reject")) {
                                    offer.setText("Offer Rejected:");
                                    offer.setTextColor(getResources().getColor(R.color.red));

                                    //Log.e("offer rejected","offer rejected");

                                }
                                if (bnm.length() > 19) {

                                    bnm = bnm.substring(0, 18) + "...";

                                    bookname.setText(bnm);
                                } else {

                                    bookname.setText(bnm); //Dont do any change

                                }

                                if (bim.equals("")) {
                                    Picasso.with(Personalchatofferaccepted.this)
                                            .load(R.drawable.main_ic)
                                            .placeholder(R.drawable.main_ic)
                                            .into(imageBook);
                                } else if (bim.contains("https://")) {
                                    Picasso.with(Personalchatofferaccepted.this)
                                            .load(bim)
                                            .placeholder(R.drawable.main_ic)
                                            .into(imageBook);
                                } else {
                                    Picasso.with(Personalchatofferaccepted.this)
                                            .load(Constant.MAIN_URL + bim)
                                            .placeholder(R.drawable.main_ic)
                                            .into(imageBook);
                                }
                                Log.e("books ", "is " + bim);


                                arrayList_count.add(category);

                                if (arrayList_count != arrayList) {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new RecyclerViewAdapter_chat(Personalchatofferaccepted.this, arrayList);
                                    listView.setAdapter(adapter);

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }

                            }


                        }


                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "personalchat_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("from_user_id", sp.getString(Constant.rid, ""));
                jo.put("to_user_id", receiver_id);
                jo.put("message", messageBody);
                jo.put("sender_name", charnm);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Buybook extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Personalchatofferaccepted.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "sellit.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", "" + book_id);
                jo.put("price", "" + book_price);
                jo.put("status", "Sold");
                jo.put("rid", "" + sp.getString(Constant.rid, ""));
                // Log.e("book_id get_course api",book_idc);
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {
                    pDialog.dismiss();

                  /*  FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    SellTextFragment addPriceOfBookFragment=new SellTextFragment();
                    ft.replace(R.id.container,addPriceOfBookFragment,"Sell");
                    ft.addToBackStack(null);
                    ft.commit();*/

                } else if (success.getString("success").equals("0")) {
                    pDialog.dismiss();

                    // Toast.makeText(context,"you have already counter value add this book",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }
    /*@Override
    protected void onDestroy() {
        asyncFetch.cancel(true);
        Log.e("destroed","destroied");
        super.onDestroy();
    }
*/
   /* public void onStop() {
        super.onStop();
        stopTimer();
        Log.e("stop ","start");

    }*/

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<Personalchatofferaccepted.RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public RecyclerViewAdapter_chat.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            RecyclerViewAdapter_chat.ViewHolder viewHolder = new RecyclerViewAdapter_chat.ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter_chat.ViewHolder holder, final int position) {

            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            String username = sp.getString(Constant.username, "");
            String rusername = getDataAdapter1.getReciever_name();
            String chars = capitalize(username);
            String charr = capitalize(sendername);

            //String rchars = capitalize(rusername);

            String rid = sp.getString(Constant.rid, "");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);
            holder.tv_sender_name.setVisibility(View.VISIBLE);
            holder.tv_receiver_name.setVisibility(View.VISIBLE);


            if (rid.equals(getDataAdapter1.getSenderID())) {
                String name = getDataAdapter1.getMsg();
                String charsf = capitalize(name);
                //name=messageBody;
                holder.tv_sender.setText(charsf);
                holder.tv_sender_name.setText(" :" + chars);
                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);

            } else {
                String name = getDataAdapter1.getMsg();
                String charsr = capitalize(name);
                holder.tv_receiver_name.setText(charr + ": ");
                holder.tv_receiver.setText(charsr);
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);


            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            String userID = "4";
            TextView tv_sender, tv_receiver, tv_receiver_name, tv_sender_name;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
                tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);


            }
        }

    }


}

