package app.livecampus.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import app.livecampus.R;
import app.livecampus.Utils.Constant;

public class SplashActivity extends AppCompatActivity {

    boolean isNetworkSatate = true;
    boolean isNetworkEnabled;
    Boolean isFirstTime, isLogged;
    SharedPreferences sp;
    String yes;
    private ProgressBar progressBar;
    private LocationManager locationManager;
    private SharedPreferences.Editor editor;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        yes = sp.getString(Constant.yes, "");


        progressBar = (ProgressBar) findViewById(R.id.splash_progressbar);
        locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        SharedPreferences app_preferences = getSharedPreferences("livecampus", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = app_preferences.edit();


        Log.i("Network is enabled", "network enabled");
        // isNetworkConnectionAvailable();

        new Thread(new Runnable() {
            @Override
            public void run() {
                doWork();
                startApp();
                finish();
            }
        }).start();


    }

    private void doWork() {
        for (int progress = 0; progress < 100; progress += 5) {
            try {
                Thread.sleep(200);
                progressBar.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startApp() {
        //Intent intent = new Intent(this, MainActivity.class);
      /*  Intent intent = new Intent(this, Mainloginandsigup.class);
        startActivity(intent);*/


        if (yes.equals("yes")) {


            Intent i = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


        } else {

            Intent i = new Intent(getApplicationContext(), Mainloginandsigup.class);
            startActivity(i);
            finish();

        }


    }


    // Private class isNetworkAvailable
    private boolean isNetworkAvailable() {
        // Using ConnectivityManager to check for Network Connection
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void checkNetworkConnection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
            Log.d("Network", "Connected");
            return true;
        } else {
            checkNetworkConnection();
            Log.d("Network", "Not Connected");
            return false;
        }
    }


}
