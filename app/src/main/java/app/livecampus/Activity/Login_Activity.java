package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.R;
import app.livecampus.Services.GPSTracker;
import app.livecampus.Utils.Constant;
import app.livecampus.fcm.Config;


/**
 * Created by isquare3 on 22/09/17.
 */

public class Login_Activity extends AppCompatActivity {

    final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String TAG = "Login_Activity";
    String inputPattern = "yyyy-MM-dd";
    final SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
    String rid;
    Boolean isFirstTime, isLogged, isscan;
    String regId, profile;
    String outputPattern = "dd-MM-yyyy";
    final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
    TextView Login, sing;
    EditText UserName, Password;
    SharedPreferences sp;
    String Prefrence = "Prefrence";
    GPSTracker gpsTracker;
    CircularImageView circleImageView;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String strusername = "", strpassword = "";
    private ProgressDialog pDialog;
    private JSONObject success;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);


        gpsTracker = new GPSTracker(this);
        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        }
        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        SharedPreferences app_preferences = getSharedPreferences("livecampus", Context.MODE_PRIVATE);
        editor = app_preferences.edit();

        isscan = app_preferences.getBoolean("isscan", true);
        Log.i(TAG, app_preferences.getString("CountryName", ""));

        getSupportActionBar().hide();

        profile = sp.getString(Constant.profile, "");


        circleImageView = (CircularImageView) findViewById(R.id.profile_image);
        UserName = (EditText) findViewById(R.id.input_username);
        Password = (EditText) findViewById(R.id.input_password);

        Login = (TextView) findViewById(R.id.btn_login);


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strusername = UserName.getText().toString().trim();
                strpassword = Password.getText().toString().trim();
                if (UserName.getText().toString().length() == 0) {
                    UserName.setError("Enter Email");
                    UserName.requestFocus();
                } else if (!strusername.matches(emailPattern)) {
                    UserName.setError("Please Enter proper Email Address");
                } else if (Password.getText().toString().length() == 0) {
                    Password.setError("Enter Password");
                    Password.requestFocus();
                } else {
                    new PostDataTOServer().execute();
                }

            }
        });


        if (profile.equals("")) {
            Picasso.with(this)
                    .load(R.drawable.user)
                    .placeholder(R.drawable.user)
                    .into(circleImageView);

        } else {
            Picasso.with(this)
                    .load(profile)
                    .placeholder(R.drawable.user)
                    .into(circleImageView);
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");


                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();


                }
            }
        };
        displayFirebaseRegId();


    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);

        regId = pref.getString("regId", null);

        Log.e("tag", "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId))
            System.out.println("the reg id :" + regId);
        else
            System.out.println("the reg id not get:" + regId);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(Login_Activity.this, Mainloginandsigup.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login_Activity.this);
            pDialog.setMessage("Please wait...");
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "login.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("email", "" + strusername);
                jo.put("password", "" + strpassword);
                jo.put("token_id", "" + regId);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
                pDialog.dismiss();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {
                    JSONObject j = success.getJSONObject("post");
                    System.out.println("my login JSONObject : " + j);
                    {
                        editor.putString("rid", j.getString("rid")).commit();
                        editor.putString("uid", j.getString("uid")).commit();
                        editor.putString("mid", j.getString("mid")).commit();
                        editor.putString("campusname", j.getString("camp_id")).commit();
                        editor.putString("year_id", j.getString("year_id")).commit();
                        editor.putString("name", j.getString("name")).commit();
                        editor.putString("email", j.getString("email")).commit();
                        editor.putString("image", j.getString("image")).commit();
                        editor.putBoolean("isLogged", true).commit();

                        String profile = j.getString("image");

                        String name = j.getString("name");
                        String camp_id = j.getString("camp_id");
                        String uid = j.getString("uid");

                        rid = j.getString("rid");


                        sp.edit().putString(Constant.profile, profile).commit();
                        sp.edit().putString(Constant.campus_id, camp_id).commit();
                        sp.edit().putString(Constant.uni_ID, uid).commit();

                        sp.edit().putString(Constant.username, name).commit();
                        sp.edit().putString(Constant.rid, rid).commit();
                        sp.edit().putString(Constant.StrCampus_name, j.getString("camp_name")).commit();
                        sp.edit().putString(Constant.campus_image, j.getString("univercity_logo")).commit();

                        sp.edit().putString(Constant.yes, "yes").commit();
                        new Scancecheck().execute();


                    }
                } else if (success.getString("success").equals("0")) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Incorrect UserName and Password", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("the exx" + e);
            }
        }
    }

    private class Scancecheck extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "scan.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", rid);

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if (success.getString("success").equals("1")) {
                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);
                    {
                        String scan = j.getString("scan");
                        Log.e("scan value", scan);
                        if (scan.equals("yes")) {
                            Intent i = new Intent(getBaseContext(), HomeActivity.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else {
                            Intent intent = new Intent(getBaseContext(), Barcode_Activity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();

                        }
                    }
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(getApplicationContext(), "Incorrect UserName and Password", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }
}
