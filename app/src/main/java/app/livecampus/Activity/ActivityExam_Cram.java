package app.livecampus.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import app.livecampus.Fragments.Menu_Activity;
import app.livecampus.Pojo.SellBookModel;
import app.livecampus.Pojo.Univercity_Model;
import app.livecampus.R;
import app.livecampus.Utils.Constant;

/**
 * Created by isquare3 on 2/28/18.
 */

public class ActivityExam_Cram extends AppCompatActivity {


    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public ImageView assigment, exam;
    public Button post;
    public RecyclerView image;
    public TextView invite, btnView;
    public EditText txtName, txt_message;
    public ImageView img1, img2, imgnext, imgbck, image1;
    JSONObject catObj;
    JSONArray jsonArrayCategory;
    SellBookModel category;
    ArrayList<SellBookModel> listOfBook = new ArrayList<>();
    ExamCramAdapter adapter;
    String strcoursenm, Strbook_id, strcourc;
    Boolean flg = true;
    Point p;
    String img;
    String strstatus;
    String countryName, editcourse;
    String selectedDate;
    ArrayList<String> imagesEncodedList;
    Calendar myCalendar = Calendar.getInstance();
    ProgressDialog pDialog;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    String ba1, blur, txtcourenter;
    SharedPreferences app_sharedprefrence;
    String inputPattern = "yyyy-MM-dd";
    final SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
    String outputPattern = "dd-MM-yyyy";
    final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
    private Bitmap bitmap;
    private InputStream inputStreamImg;
    private JSONObject success;
    private ArrayList<Univercity_Model> arrayList;
    private String imagePath, blb;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityexam_cram);
//        app_sharedprefrence = getSharedPreferences("livecampus", Context.MODE_PRIVATE);
        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);


        String rid = sp.getString(Constant.rid, "");
        listOfBook = new ArrayList<>();
        arrayList = new ArrayList<>();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @SuppressLint("WrongConstant")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel();
            }


        };

        image = (RecyclerView) findViewById(R.id.image);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        //img3=(ImageView)findViewById(R.id.img3);
        imgnext = (ImageView) findViewById(R.id.imgnext);
        imgbck = (ImageView) findViewById(R.id.imgbck);
        invite = (TextView) findViewById(R.id.invite);
        btnView = (TextView) findViewById(R.id.tv_view);
        txtName = (EditText) findViewById(R.id.txtName);

        txtName.setSelection(txtName.getText().length());

        txtName.setVisibility(View.GONE);


        image.setHasFixedSize(true);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ActivityExam_Cram.this, LinearLayoutManager.HORIZONTAL, false);
        image.setLayoutManager(horizontalLayoutManagaer);


        new ActivityExam_Cram.Get_Gallery().execute();

        imgnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //image.smoothScrollToPosition(adapter.getItemCount());
                image.smoothScrollBy(100, 0);
            }
        });
        imgbck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.smoothScrollBy(-100, 0);
            }
        });

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ckick", "ok ");
                startActivity(new Intent(ActivityExam_Cram.this, GroupListActivity.class));

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];


                InputMethodManager imm = (InputMethodManager) ActivityExam_Cram.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                //Initialize the Point with x, and y positions
                p = new Point();
                p.x = location[0];
                p.y = location[1];
                if (p != null) {
                    showPopup(ActivityExam_Cram.this, p);
                }
            }
        });


        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DatePickerDialog mDate = new DatePickerDialog(ActivityExam_Cram.this, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                mDate.show();
            }
        });

    }

    private void showPopup(final Activity context, Point p) {
        int popupWidth = 450;
        int popupHeight = 450;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.examblub, viewGroup);

        txt_message = (EditText) layout.findViewById(R.id.textView2);
        post = (Button) layout.findViewById(R.id.post);
        image1 = (ImageView) layout.findViewById(R.id.image);
        txt_message.setText("");

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 240;
        int OFFSET_Y = 480;

        blb = txt_message.getText().toString().trim();

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                // blur=txt_message.getText().toString().trim();

            }
        });
        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();

            }
        });
        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        Log.i("Pixels", p.x + " " + p.y);
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);
    }

    /*@Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tv_view:
                Log.e("ckick","ok ");
                startActivity(new Intent(ActivityExam_Cram.this,GroupListActivity.class));


                break;
        }
    }
*/

    public String getTitleOfBook(String title) {
        String titleBook = "";
        String book[] = title.split(" ");
        for (int i = 0; i < 2; i++) {

            titleBook += book[i] + " ";
        }
        return titleBook;

    }

    private void updateLabel() {
        String myFormat = "dd MMMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        selectedDate = sdf.format(myCalendar.getTime());
        Log.i("selected date is ", selectedDate);
        //edittext.setText(sdf.format(myCalendar.getTime()));
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();

        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void selectImage() {
        try {
            PackageManager pm = this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            Bitmap bm1 = null;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");


                byte imageInByte[] = bytes.toByteArray();
                imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Bitmap bm = null;
            Bitmap bm1 = null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    Matrix matrix = new Matrix();
                    // matrix.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte imageInByte[] = stream.toByteArray();
                    imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                    bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //circleImageView.setImageBitmap(bm1);
        }
    }

    public void onBackPressed() {
        //super.onBackPressed();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, new Menu_Activity());
        ft.commit();
        finish();
        //finish();
    }

    private class Get_Gallery extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ActivityExam_Cram.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_course.php";

            System.out.println("the get course url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", sp.getString(Constant.rid, ""));

                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");

                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel();
                        catObj = jsonArrayCategory.getJSONObject(i);
                        category.setCourse_id(catObj.getString("course_id"));
                        category.setRid(catObj.getString("rid"));
                        category.setTitle(catObj.getString(Constant.CNAME1));
                        category.setIsbn(catObj.getString(Constant.cISBN1));
                        category.setImage(catObj.getString(Constant.IMAGE1));
                        category.setRating(catObj.getString("rating"));
                        category.setCoursename(catObj.getString("CourseName"));
                        Log.i("rating", catObj.getString("rating"));
                        if (catObj.getString(Constant.PRICE).equals("")) {
                            category.setPrice("0");
                        } else {
                            category.setPrice((catObj.getString(Constant.PRICE)));
                        }


                        if (flg) {
                            editcourse = txtName.getText().toString().trim().toUpperCase();


                        } else {

                            editcourse = category.getCoursename();

                        }
                        invite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                if (selectedDate == null) {
                                    Toast.makeText(getApplicationContext(), "Please select date", Toast.LENGTH_LONG).show();
                                }
                                try {
                                    new PostDataTOServer().execute();
                                } catch (Exception e) {
                                    Toast toast = Toast.makeText(ActivityExam_Cram.this, "Please enter blub value", Toast.LENGTH_SHORT);
                                    toast.show();
                                }


                            }
                        });


                        Date date = null;
                        String strStart = null;

                        try {
                            date = inputFormat.parse(catObj.getString(Constant.DATE));

                            strStart = outputFormat.format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        category.setDate(strStart);

//                            category.setDate(catObj.getString(Constant.DATE));
                        listOfBook.add(category);
                    }
                    adapter = new ExamCramAdapter(ActivityExam_Cram.this, listOfBook);
                    image.setAdapter(adapter);
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(ActivityExam_Cram.this, "Record not found", Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                pDialog.dismiss();
                System.out.println("the title exeption is :" + e);
                e.printStackTrace();
            }
        }
    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ActivityExam_Cram.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "create_group.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();
                blb = txt_message.getText().toString().trim();
                jo.put("rid", sp.getString(Constant.rid, ""));
                jo.put("course_id", category.getCourse_id());
                jo.put("bookimage", category.getImages());
                jo.put("course_name", txtcourenter);
                jo.put("blub", blb);
                jo.put("date", selectedDate);
                jo.put("status", strstatus);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception send data : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);
                    {
                        System.out.println("the vb login j " + j);
                        blb = txt_message.getText().toString().trim();
                        Intent i = new Intent(getApplicationContext(), ActivityGroupChat.class);
                        i.putExtra("groupid", j.getString("groupid"));
                        i.putExtra("bookimage", img);
                        i.putExtra("blub", blb);
                        i.putExtra("cource", txtcourenter);
                        i.putExtra("date", selectedDate);
                        i.putExtra("cram", strstatus);

                        sp.edit().putString("groupid", j.getString("groupid")).commit();
                        sp.edit().putString("groupImage", img).commit();

                        startActivity(i);
                        new pushNotification().execute();

                        finish();
                    }
                } else if (success.getString("success").equals("0")) {
                    blb = txt_message.getText().toString().trim();


                    Toast.makeText(ActivityExam_Cram.this, "" + success.getString("posts"), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    private class pushNotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "create_group_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("senderid", sp.getString(Constant.rid, ""));
                jo.put("course_name", txtcourenter);
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);
                try {


                    if (success.getString("success").equals("1")) {


                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }


        }
    }

    public class ExamCramAdapter extends RecyclerView.Adapter<ActivityExam_Cram.ExamCramAdapter.ViewHolder> {

        public String id;
        Context context;
        JSONObject success;
        ProgressDialog pDialog;
        SharedPreferences sp;
        String book_id;
        String strcoursenm, Strbook_id;
        JSONArray jsonArrayCategory;
        ArrayList<SellBookModel> getDataAdapter;


        public ExamCramAdapter(Context context, ArrayList<SellBookModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.examcram_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            SellBookModel getDataAdapter1 = getDataAdapter.get(position);
            String image = getDataAdapter1.getImage();

            strcoursenm = getDataAdapter1.getCoursename();


            if (strcoursenm.equals("") || strcoursenm.equals("null")) {

                holder.coursenm.setVisibility(View.INVISIBLE);
            } else {

                holder.coursenm.setVisibility(View.VISIBLE);

                holder.coursenm.setText("" + strcoursenm);

            }

            if (image.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.logo)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.imgBook);
            } else {
                Picasso.with(context)
                        .load(image)
                        .resize(150, 230)
                        .placeholder(R.drawable.main_ic)
                        .into(holder.imgBook);
            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            ImageView imgBook;
            LinearLayout ln1;
            TextView coursenm, txtDelete;
            View view1, view2;

            public ViewHolder(View itemView) {

                super(itemView);
                imgBook = (ImageView) itemView.findViewById(R.id.img_book);
                coursenm = (TextView) itemView.findViewById(R.id.coursenm);
                //txtDelete = (TextView) itemView.findViewById(R.id.txt_delete);
                ln1 = (LinearLayout) itemView.findViewById(R.id.ln1);

                ln1.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                SellBookModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());

                strcourc = getDataAdapter1.getCoursename();


                if (strcourc.equals("") || strcourc.equals("null")) {

                    flg = true;
                    txtName.setVisibility(View.GONE);
                    txtName.setText("");


                } else {
                    flg = false;
                    txtName.setVisibility(View.GONE);
                    txtName.setText(strcourc);


                }

                switch (v.getId()) {


                    case R.id.ln1:

                        final Dialog dialog = new Dialog(v.getContext());
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.crampopup);

                        ImageView image = (ImageView) dialog.findViewById(R.id.image);
                        //TextView btnback = (TextView) dialog.findViewById(R.id.btnback);
                        exam = (ImageView) dialog.findViewById(R.id.exam);
                        assigment = (ImageView) dialog.findViewById(R.id.assigment);

                        image.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        img = getDataAdapter1.getImage();
                        txtcourenter = getDataAdapter1.getCoursename();

                        exam.setOnClickListener(new View.OnClickListener() {
                            @SuppressLint({"ResourceAsColor", "NewApi"})
                            @Override
                            public void onClick(View v) {

                                strstatus = "Exam";
                                ln1.setBackgroundColor(getResources().getColor(R.color.red));
                                dialog.dismiss();

                            }
                        });

                        assigment.setOnClickListener(new View.OnClickListener() {
                            @SuppressLint({"ResourceAsColor", "NewApi"})
                            @Override
                            public void onClick(View v) {
                                strstatus = "Assigment";

                                ln1.setBackgroundColor(getResources().getColor(R.color.lightblue));
                                dialog.dismiss();

                            }
                        });
                        dialog.show();

                        break;


                }

            }
        }
    }

}

