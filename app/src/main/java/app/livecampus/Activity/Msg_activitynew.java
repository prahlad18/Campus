package app.livecampus.Activity;

/**
 * Created by isquare3 on 4/20/18.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

public class Msg_activitynew extends AppCompatActivity {

    public boolean call = true;
    public CircleImageView bookimage;
    public TextView title1;
    public String userID = "4";
    public Thread t;
    public TextView tv_sender;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList, arrayList_count;
    ArrayList<DataModel> hs = new ArrayList<>();
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    Handler mHandler;
    String book_id, book_name, book_image, rid, Sellerid, reciverid;
    int value = -1;
    Receivemessage asyncFetch;
    private String messageBody, img;
    private JSONObject success;
    private ProgressDialog pDialog;
    private Button mButtonSend, btn_ok;
    private EditText mEditTextMessage;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainchat);
        getSupportActionBar().hide();

        listView = (RecyclerView) findViewById(R.id.listview);
        mButtonSend = (Button) findViewById(R.id.btn_send);
        bookimage = (CircleImageView) findViewById(R.id.bookimage);
        title1 = (TextView) findViewById(R.id.title1);
        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);


//        btn_ok = (Button) findViewById(R.id.btn_ok);
        mEditTextMessage = (EditText) findViewById(R.id.et_message);


        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);


        Intent intent = getIntent();
        rid = intent.getStringExtra("user_chk");

      /*  if(intent.getStringExtra("user_chk")!=null){
            rid = intent.getStringExtra("user_chk");


        }else {
            rid = intent.getStringExtra("seller_id");



        }*/
        //book_id = intent.getStringExtra("book_id");
        //book_name = intent.getStringExtra("book_name");

        book_name = sp.getString("bookName", "");
        book_id = sp.getString("book_id", "");
        book_image = sp.getString("bookImage", "");
        setTitle("" + book_name);
        Log.e("book_name", book_name);
        arrayList = new ArrayList<>();
        arrayList_count = new ArrayList<>();

        if (book_name.length() > 19) {

            book_name = book_name.substring(0, 18) + "...";

            title1.setText(book_name);
        } else {

            title1.setText(book_name); //Dont do any change

        }
        if (book_image.equals("")) {
            Picasso.with(this)
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.main_ic)
                    .into(bookimage);
        } else {
            Picasso.with(this)
                    .load(book_image)
                    .placeholder(R.drawable.main_ic)
                    .into(bookimage);
        }
        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String message = mEditTextMessage.getText().toString();

                // Log.e("hiii", String.valueOf(mListView));


                if (mEditTextMessage.getText().length() > 1) {
                    messageBody = mEditTextMessage.getText().toString();
                    call = false;
                    sendTextMessage("" + messageBody);
                    new Receivemessage().execute();
                    new Pushnotification().execute();

                } else {

                    System.out.println("the enter text");

                }
                mEditTextMessage.setText("");

            }
        });
        mButtonSend.setAlpha(0.4f);

        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 1) {
                    mButtonSend.setAlpha(1f);

                }
            }


            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s.length() <= 0) {
                    mButtonSend.setAlpha(0.4f);

                }
            }
        });

        if (rid.equals(sp.getString(Constant.rid, ""))) {
            Intent intent1 = new Intent(Msg_activitynew.this, Chatnamelist.class);
            startActivity(intent1);
            finish();
        } else {
            callAsynchronousTask();

        }

        asyncFetch = new Receivemessage();
        asyncFetch.execute();
        callAsynchronousTask();
    }

    private void sendTextMessage(String s) {

        SendData = new AsyncTask<Void, Void, Void>() {

            protected void onPreExecute() {
                super.onPreExecute();


            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "chat.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);

                try {
                    JSONObject jo = new JSONObject();
                    jo.put("rid", sp.getString(Constant.rid, ""));
                    jo.put("book_id", book_id);
                    jo.put("receiver_id", rid);
                    jo.put("message", messageBody);


                    System.out.println("the send data is :" + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(org.apache.http.protocol.HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                } catch (Exception e) {
                    System.out.println("Exception inProfileActivity : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {
                        JSONObject j = success.getJSONObject("posts");
                        Log.e("hiii", "hiii");


                        System.out.println("my login JSONObject : " + j);
                        value++;
                        category = new DataModel();


                        category.setSenderID("" + sp.getString(Constant.rid, ""));
                        category.setRecieverID("" + book_id);
//                        category.setReciever_name("" + success.getString("Name"));
                        category.setMsg("" + messageBody);

                        arrayList.add(category);
                        adapter = new RecyclerViewAdapter_chat(Msg_activitynew.this, arrayList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);

                        // call=true;


                    } else if (success.getString("success").equals("0")) {
                        Toast.makeText(getApplicationContext(), "Incorrect UserName and Password", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                    System.out.println("the exx" + e);
                }
            }
        };
        SendData.execute();
    }

    private void callAsynchronousTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {


                handler.post(new Runnable() {

                    public void run() {
                        try {
                            //new Receivemessage().execute();
                            asyncFetch = new Receivemessage();
                            asyncFetch.execute();
                            adapter.notifyDataSetChanged();


                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 500, 2000);
         /*handler = new Handler();
            Timer timer = new Timer();

            TimerTask doAsynchronousTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {

                        public void run() {
                            try {


                               new  Receivemessage().execute();

                                adapter.notifyDataSetChanged();



                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                            }
                        }
                    });
                }
            };
            timer.schedule(doAsynchronousTask, 0, 1000); //execute in every 50000 ms*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (asyncFetch != null && asyncFetch.getStatus() == AsyncTask.Status.RUNNING) {
            asyncFetch.cancel(true);
            stopTimer();
        }

        stopTimer();
        //call=false;

    }


    //To start timer

    //To stop timer
    private void stopTimer() {
        asyncFetch.cancel(true);
        if (timerTask != null) {
            //Log.e("cancle","cancle");
            timerTask.cancel();
            //handler.removeCallbacks(timer);

        } else {
            Log.e("notcancle", "notcancle");


        }
    }

    private String capitalize(final String text) {

        return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
    }

    private class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "output_chat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", book_id);
                jo.put("rid", sp.getString(Constant.rid, ""));//userid
                jo.put("receiver_id", rid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);


            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            if (i > value) {
                                value++;
                                category = new DataModel();


                                success = jsonArrayCategory.getJSONObject(i);
                                category.setSenderID("" + success.getString("SenderId"));
                                category.setRecieverID("" + success.getString("BookId"));
                                category.setReciever_name("" + success.getString("Name"));
                                category.setMsg("" + success.getString("Message"));

                                arrayList.add(category);
                           /*     arrayList_count.add(category);

                                if (arrayList_count!=arrayList)
                                {

                                    arrayList.clear();
                                    arrayList.addAll(arrayList_count);

                                    adapter = new RecyclerViewAdapter_chat(Msg_activitynew.this, arrayList);
                                    listView.setAdapter(adapter);

                                    listView.scrollToPosition(arrayList.size() - 1);

                                }*/


                            }


                        }
                        Log.e("arraylist size", "" + arrayList.size());
                        adapter = new RecyclerViewAdapter_chat(Msg_activitynew.this, arrayList);
                        listView.setAdapter(adapter);
                        //adapter.notifyDataSetChanged();

                        listView.scrollToPosition(arrayList.size() - 1);


                    } else if (success.getString("success").equals("0")) {
                        //Toast.makeText(Msg_activitynew.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "bookchat_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", sp.getString(Constant.rid, ""));
                jo.put("book_id", book_id);
                jo.put("receiver_id", rid);
                jo.put("message", messageBody);
                //jo.put("sender_name",sp.getString(Constant.username,""));


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }
   /* private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }*/

    public class RecyclerViewAdapter_chat extends RecyclerView.Adapter<RecyclerViewAdapter_chat.ViewHolder> {

        Context context;
        ArrayList<DataModel> getDataAdapter;
        SharedPreferences sp;

        public RecyclerViewAdapter_chat(Context context, ArrayList<DataModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {


            DataModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
            String username = sp.getString(Constant.username, "");
            String chars = capitalize(username);

            String rid = sp.getString(Constant.rid, "");
            holder.tv_receiver.setVisibility(View.VISIBLE);
            holder.tv_sender.setVisibility(View.VISIBLE);
            holder.tv_sender_name.setVisibility(View.VISIBLE);
            holder.tv_receiver_name.setVisibility(View.VISIBLE);


            if (rid.equals(getDataAdapter1.getSenderID())) {
                String name = getDataAdapter1.getMsg();
                String charsf = capitalize(name);
                //name=messageBody;
                holder.tv_sender.setText(charsf);
                holder.tv_sender_name.setText(" :" + chars);
                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);

            } else {

                String name = getDataAdapter1.getMsg();
                String charsr = capitalize(name);
                holder.tv_receiver_name.setText(getDataAdapter1.getReciever_name() + ": ");
                holder.tv_receiver.setText(charsr);
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);


            }
        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            String userID = "4";
            TextView tv_sender, tv_receiver, tv_receiver_name, tv_sender_name;

            public ViewHolder(View itemView) {

                super(itemView);
                tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
                tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
                tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
                tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);

            }
        }

    }


}
