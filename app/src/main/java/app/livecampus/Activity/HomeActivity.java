package app.livecampus.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.livecampus.Fragments.Change_ISBN_Course;
import app.livecampus.Fragments.Edit_Profile2;
import app.livecampus.Fragments.Menu_Activity;
import app.livecampus.R;
import app.livecampus.Services.GPSTracker;
import app.livecampus.Utils.Constant;


public class HomeActivity extends AppCompatActivity {
    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    String img, Strname, asyntask, text;
    String Struid;
    String Stryer;
    String Strmid;
    String Strimg;
    TextView textCartItemCount;
    int mCartItemCount = 10;
    String Strcpnm;
    SharedPreferences.Editor editor;
    GPSTracker gpsTracker;
    ProgressDialog pDialog;
    String countryName;
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    Intent intent = getIntent();
    private TextView drawer_option_home,
            drawer_option_our_app,
            drawer_option_share_app,
            home,
            drawer_option_rate_me,
            drawer_option_feedback,
            drawer_option_about_us, tv_name, tv_email;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentTransaction fragmentTransaction;
    private Handler handler = new Handler();
    private int c = 0;
    private SharedPreferences app_preferences;
    private JSONObject success;
    private String res;
    private String Test;

    public static String getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryName();
            }
        } catch (IOException ioe) {
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        checkAndRequestPermissions();

        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        pref = getSharedPreferences("contact", Context.MODE_PRIVATE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        new updatesave().execute();
        app_preferences = getSharedPreferences("livecampus", MODE_PRIVATE);
        editor = app_preferences.edit();


        Intent i = getIntent();

        if (i.getExtras() != null) {
            text = i.getStringExtra("TextBox");

        }


        gpsTracker = new GPSTracker(this);
        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        } else {

            countryName = getCountryName(getApplicationContext(), gpsTracker.getLatitude(), gpsTracker.getLongitude());
            //Log.i("country Name ",countryName);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setDrawerOptionTypeface();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new Menu_Activity(), "hi");
        fragmentTransaction.addToBackStack("MainFragment");
        fragmentTransaction.commit();
        setDrawerOptionUnselected();
        home.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private boolean checkAndRequestPermissions() {
        int internet = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int READ = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int read_state = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int read_sms = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (read_state != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (read_sms != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }

        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void setDrawerOptionTypeface() {

        drawer_option_our_app = (TextView) findViewById(R.id.drawer_option_our_app);
        home = (TextView) findViewById(R.id.home);
        drawer_option_share_app = (TextView) findViewById(R.id.drawer_option_share_app);
        drawer_option_about_us = (TextView) findViewById(R.id.drawer_option_about_us);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("In the home activity", "In the home activity");
    }*/
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100 && resultCode == RESULT_OK) {
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce == true) {
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @SuppressLint("NewApi")
                        public void onClick(DialogInterface dialog, int id) {
                            finishAffinity();
                            System.exit(0);
                            //finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, " Click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                doubleBackToExitPressedOnce = false;

            }
        }, 2000);
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @SuppressLint("NewApi")
    public void onDrawerOptionClicked(View view) {
        switch (view.getId()) {

            case R.id.home:
                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(i);
                finish();

                break;

            case R.id.drawer_option_our_app:

                Menu_Activity menu_activity = new Menu_Activity();
                menu_activity.stopTimer();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, new Change_ISBN_Course());
                fragmentTransaction.addToBackStack("MainFragment");
                Bundle bundle1 = new Bundle();
                bundle1.putString("TextBox", text);
                setDrawerOptionUnselected();
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                fragmentTransaction.commit();
                mDrawerLayout.closeDrawers();

                break;
            case R.id.drawer_option_share_app:

                FragmentManager fm = ((AppCompatActivity) this).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Edit_Profile2 addPriceOfBookFragment = new Edit_Profile2();
                Bundle bundle = new Bundle();
                bundle.putString("name", Strname);
                bundle.putString("mid", Strmid);
                bundle.putString("uid", Struid);
                bundle.putString("image", Strimg);
                bundle.putString("year_id", Stryer);
                bundle.putString("cpname", Strcpnm);
                addPriceOfBookFragment.setArguments(bundle);
                ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                setDrawerOptionUnselected();
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                mDrawerLayout.closeDrawers();
                ft.commit();


                break;

            case R.id.drawer_option_about_us:
               /* fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, new Logout(), "Logout");
                fragmentTransaction.addToBackStack("MainFragment");
                fragmentTransaction.commit();
                setDrawerOptionUnselected();*/
                Intent intent = new Intent(getApplicationContext(), Mainloginandsigup.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                setDrawerOptionUnselected();
                ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                mDrawerLayout.closeDrawers();
                editor.remove("rid");
                editor.remove("isLogged");
                sp.edit().putString(Constant.yes, "no").commit();
                editor.commit();
                Toast.makeText(getApplicationContext(), "Logout succefully", Toast.LENGTH_LONG).show();

                break;
            default:
                Toast.makeText(this, "Unknown drawer option clicked", Toast.LENGTH_LONG).show();
        }
    }

    private void setDrawerOptionUnselected() {
        drawer_option_our_app.setTextColor(getResources().getColor(R.color.black));
        home.setTextColor(getResources().getColor(R.color.black));
        drawer_option_share_app.setTextColor(getResources().getColor(R.color.black));
        // drawer_option_rate_me.setTextColor(getResources().getColor(R.color.black));
        // drawer_option_feedback.setTextColor(getResources().getColor(R.color.black));
        drawer_option_about_us.setTextColor(getResources().getColor(R.color.black));
    }

    private class updatesave extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_details.php";


            System.out.println("the home url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                jo.put("rid", "" + sp.getString(Constant.rid, ""));
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);

                            Strname = (j.getString("name"));
                            Struid = (j.getString("uid"));
                            Stryer = (j.getString("year_id"));
                            Strmid = (j.getString("mid"));
                            Strcpnm = (j.getString("CampusId"));


                            Log.e("Uid1", Struid);
                            Log.e("mid1", Strmid);
                            Log.e("name1", Strname);
                            Log.e("year1", Stryer);
                            Log.e("CampusId", Strcpnm);
                            sp.edit().putString(Constant.uid, Struid).commit();
                            sp.edit().putString(Constant.campus_id, Strcpnm).commit();
//                            sp.edit().putString(Constant.StrCampus_name, Strcpnm).commit();
                            //Log.e("cpname",Strcpnm);


                        }
                    } catch (JSONException e) {

                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
                    // pDialog.hide ();
                } else if (success.getString("success").equals("0")) {
                    // pDialog.hide();
                } else {

                }

            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }

    }

}