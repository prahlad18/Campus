package app.livecampus.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import app.livecampus.R;

public class HigherInfo extends AppCompatActivity {

    LinearLayout linerBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_higher_info);

        getSupportActionBar().hide();

        linerBack = (LinearLayout) findViewById(R.id.linerBack);

        linerBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });


    }
}
