package app.livecampus.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

import app.livecampus.R;

public class DemoAct extends Fragment {
    ImageView dialogdone,imageCamera,imageView2;
    InputStream inputStreamImg;
    private final int PICK_IMAGE_CAMERA = 1;
    Bitmap bitmap;
    String imagePath;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.activity_demo, container, false);

        imageView2 = (ImageView) view.findViewById(R.id.imageView2);

        dialogdone = (ImageView) view.findViewById(R.id.done);
        imageCamera = (ImageView) view.findViewById(R.id.imageCamera);
        Log.e("custom ", " hello ");

        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });
        dialogdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }
    private void selectImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri mUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                "pic" + String.valueOf(System.currentTimeMillis()) + ".jpg"));

        Log.e("mUri", " is " + mUri);

        getActivity().startActivityForResult(intent, 111);
    }
    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        Toast.makeText(getActivity(),"Campus ",Toast.LENGTH_LONG).show();
        if (requestCode == 111) {
            Bitmap bm1 = null;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                byte imageInByte[] = bytes.toByteArray();
                imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);
                imageView2.setImageBitmap(bm1);

                Log.e("cam open","ok ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
