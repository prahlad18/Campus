package app.livecampus.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Fragments.SellTextFragment;
import app.livecampus.Pojo.SellBookModel_new;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import app.livecampus.adapter.Buy_sellit_adaptor;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 4/30/18.
 */

public class Buy_sellit_activity extends AppCompatActivity {
    public String id;
    JSONObject success;
    ProgressDialog pDialog;
    SharedPreferences sp;
    JSONObject catObj;
    SellBookModel_new category;
    ListView searchList;
    JSONArray jsonArrayCategory;
    String book_name, book_image, rid, buyer_id, book_price, buyusername, status;
    Activity activity;
    ArrayList<SellBookModel_new> listOfBook = new ArrayList<SellBookModel_new>();
    String book_id, price, rating, receiver_id;
    String image, image1;
    int book_rice;
    TextView txtUserName, txtUserUniversity, txtUserMajor, txtUserYear, txtUserprice, txtsellit, txtBookTitle, txtisbn, back, txt_home, txt_search;
    String issuccess;
    String strcunt = "yes";
    String strtextprice, book_idc, name, charnm;
    RatingBar ratingBar;
    CircleImageView imageUser;
    ImageView imageBook, cancle;
    Point p;
    ArrayList<String> imagesEncodedList;
    //public String Strprice ;
    int Strprice;
    private Activity context;
    private FragmentTransaction fragmentTransaction;
    private int PICK_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy_sellit_activity);

        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtUserUniversity = (TextView) findViewById(R.id.txtUserUniversity);
        txtUserMajor = (TextView) findViewById(R.id.txtUserMajor);
        imageBook = (ImageView) findViewById(R.id.imageBook);
        imageUser = (CircleImageView) findViewById(R.id.imageUser);
        txtUserYear = (TextView) findViewById(R.id.txtUserYear);
        txtBookTitle = (TextView) findViewById(R.id.txt_book_title);
        txtisbn = (TextView) findViewById(R.id.txt_book_isbn);
        back = (TextView) findViewById(R.id.btnEdit);
        searchList = (ListView) findViewById(R.id.searchList);

        //txt_home = (TextView) findViewById(R.id.txt_sell_home);


        //txt_search = (TextView) findViewById(R.id.txt_sell_search);
        txtUserprice = (TextView) findViewById(R.id.sellbook_price);
        txtsellit = (TextView) findViewById(R.id.sellit);
        cancle = (ImageView) findViewById(R.id.cancle);

        sp = getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        final SellBookModel_new model = new SellBookModel_new();
        new Get_detail().execute();

        Intent intent = getIntent();

        // String id = intent.getStringExtra("id");
        book_id = intent.getStringExtra("book_id");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SellTextFragment());
                ft.addToBackStack(null);
                ft.commit();
                finish();
            }
        });

       /* txt_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(getApplicationContext().getApplicationContext(), HomeActivity.class);
                startActivity(homeIntent);
                finish();
            }
        });
        txt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.container, new SearchBookFragment());
                ft.addToBackStack(null);
                ft.commit();
            }
        });*/


    }

    public String getTitleOfBook(String title) {
        String titleBook = "";
        String book[] = title.split(" ");
        for (int i = 0; i < 2; i++) {

            titleBook += book[i] + " ";
        }
        return titleBook;

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, new SellTextFragment());
        ft.addToBackStack(null);
        ft.commit();
        finish();
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private class Get_detail extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Buy_sellit_activity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "buy.php";

            System.out.println("the get course url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", book_id);

                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");

                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel_new();
                        catObj = jsonArrayCategory.getJSONObject(i);
                        category.setBookNane(catObj.getString("name"));
                        category.setBookname(catObj.getString("cNAME1"));
                        category.setBookImage(catObj.getString("image1"));
                        category.setUserimage(catObj.getString("image"));
                        category.setUnivervity(catObj.getString("univercity_name"));
                        category.setMajor(catObj.getString("majors_name"));
                        category.setYear(catObj.getString("year_name"));
                        category.setRating(catObj.getString("rating"));
                        category.setPrice(catObj.getString("price"));
                        category.setIsbn(catObj.getString("cISBN1"));
                        // category.setRid(catObj.getString("rid"));
                        category.setBuyerid(catObj.getString("Buyer_Id"));
                        category.setSellerid(catObj.getString("Seller_Id"));
                        category.setRecever_id(catObj.getString("CourseId"));




                       /* name=category.getBookNane();

                        String chars = capitalize(name);
                        txtUserName.setText(chars);

                        final String title = getTitleOfBook(category.getBookname());

                        txtBookTitle.setText(title);
                        txtisbn.setText("ISBN #"+category.getIsbn());
                        txtUserUniversity.setText(category.getUnivervity());
                        txtUserMajor.setText(category.getMajor());
                        txtUserYear.setText(category.getYear());
                        image = category.getBookImage();

                        if (image.equals(""))
                        {
                            Picasso.with(context)
                                    .load(R.drawable.logo)
                                    .placeholder(R.drawable.main_ic)
                                    .into(imageBook);
                        }
                        else
                        {
                            Picasso.with(context)
                                    .load(image)
                                    .placeholder(R.drawable.main_ic)
                                    .into(imageBook);
                        }



                        String imageuser = category.getUserimage();
                        if (imageuser.equals(""))
                        {
                            Picasso.with(context)
                                    .load(R.drawable.user)
                                    .placeholder(R.drawable.main_ic)
                                    .into(imageUser);
                        }
                        else
                        {
                            Picasso.with(context)
                                    .load(imageuser)
                                    .placeholder(R.drawable.user)
                                    .into(imageUser);
                        }

                        txtUserprice.setText("$" + category.getPrice() + ".00");


                        txtsellit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {



                                status="Accept";
                                new Pushnotification().execute();
                                charnm = capitalize(sp.getString(Constant.username,""));
                                book_name=category.getBookname();
                                book_image=category.getBookImage();
                                buyusername=category.getBookNane();
                                book_id=category.getRecever_id();
                                rid=category.getSellerid();
                                book_price=category.getPrice();
                                buyer_id=category.getBuyerid();
                                Intent i = new Intent(getApplicationContext(), Personalchatofferaccepted.class);
                                i.putExtra("receiver_id", category.getBuyerid());
                                i.putExtra("sendername", category.getBookNane());
                                i.putExtra("senderimage", category.getUserimage());
                                i.putExtra("counter", "counter");

                                i.putExtra("user_chk",rid);
                                i.putExtra("buyer_id",buyer_id);
                                i.putExtra("book_id",book_id);
                                i.putExtra("offer_acceted","Offer Accepted:");
                                i.putExtra("name",book_name);
                                i.putExtra("image",book_image);
                                i.putExtra("price",book_price);
                                //i.putExtra("rid",rid);
                                Log.e("user_chk",rid);
                                Log.e("book_id",book_id);
                                Log.e("buyer_id",buyer_id);

                                sp.edit().putString("bookName",book_name).commit();
                                sp.edit().putString("book_id",book_id).commit();
                                sp.edit().putString("bookImage",book_image).commit();

                                startActivity(i);
                                finish();

                                *//*status="Accept";
                                rid=category.getSellerid();
                                buyer_id=category.getBuyerid();
                                buyusername=category.getBookNane();
                                charnm = capitalize(sp.getString(Constant.username,""));
                                new Pushnotification().execute();
                                new Buybook().execute();*//*



                            }
                        });

                        cancle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                status="Reject";
                                new Pushnotification2().execute();
                                charnm = capitalize(sp.getString(Constant.username,""));
                                Log.e("receiver_image", category.getUserimage());
                                book_name=category.getBookname();
                                book_image=category.getBookImage();
                                buyusername=category.getBookNane();
                                book_id=category.getRecever_id();
                                rid=category.getSellerid();
                                book_price=category.getPrice();
                                buyer_id=category.getBuyerid();
                                // Log.e("sendername", getDataAdapter1.getReciever_name());
                                Intent i = new Intent(getApplicationContext(), Personalchatofferaccepted.class);
                                i.putExtra("receiver_id", category.getBuyerid());
                                i.putExtra("sendername", category.getBookNane());
                                i.putExtra("senderimage", category.getUserimage());
                                i.putExtra("counter", "counter");

                                i.putExtra("user_chk",rid);
                                i.putExtra("offer_rejected","Offer Rejected:");
                                i.putExtra("buyer_id",buyer_id);
                                i.putExtra("book_id",book_id);
                                i.putExtra("name",book_name);
                                i.putExtra("image",book_image);
                                i.putExtra("price",book_price);
                                startActivity(i);
                                finish();

                            }
                        });*/
                        listOfBook.add(category);
                    }
                    Buy_sellit_adaptor adapter = new Buy_sellit_adaptor(Buy_sellit_activity.this, listOfBook);
                    searchList.setAdapter(adapter);
                } else if (success.getString("success").equals("0")) {

                }

            } catch (JSONException e) {
                pDialog.dismiss();
                System.out.println("the title exeption is :" + e);
                e.printStackTrace();
            }
        }


    }

    private class Buybook extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Buy_sellit_activity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "sellit.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", "" + category.getRecever_id());
                jo.put("price", "" + category.getPrice());
                jo.put("status", "Sold");
                jo.put("rid", "" + sp.getString(Constant.rid, ""));
                // Log.e("book_id get_course api",book_idc);
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {

                    FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    SellTextFragment addPriceOfBookFragment = new SellTextFragment();
                    ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                    ft.addToBackStack(null);
                    ft.commit();
                    finish();

                } else if (success.getString("success").equals("0")) {
                    // Toast.makeText(context,"you have already counter value add this book",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("the exx" + e);
            }
        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "offer_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("buy_id", buyer_id);
                jo.put("sell_id", rid);
                jo.put("status", status);

                //jo.put("message", messageBody);
                jo.put("sell_name", charnm);
                jo.put("buy_name", buyusername);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private class Pushnotification2 extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "offer_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("buy_id", buyer_id);
                jo.put("sell_id", rid);
                jo.put("status", status);

                //jo.put("message", messageBody);
                jo.put("sell_name", charnm);
                jo.put("buy_name", buyusername);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }
}
