package app.livecampus.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import app.livecampus.R;
import app.livecampus.Services.GPSTracker;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by isquare3 on 22/09/17.
 */

public class Signup_Activity extends AppCompatActivity {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    static final int PICK_IMAGE_REQUEST = 1;
    public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public RadioGroup radiogroup;
    public Spinner spnage;
    public String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextView txt_next;
    String uid, mid, camp_id, year_id, edit1, edit2, name, password, email;
    EditText editName, editEmail, editPassword, editConfirmPassword;
    ImageView user_image;
    String encodedImage, strage, sp1, sp2;
    int PICK_IMAGE = 1;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    String inputPattern = "yyyy-MM-dd";
    final SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
    String outputPattern = "dd-MM-yyyy";
    final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
    CircleImageView circleImageView;
    SharedPreferences.Editor editor;
    String[] cod = new String[]{"18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32",
            "33", "34", "35", "36", "37", "38", "39", "40", "41",
            "42", "43", "44", "45", "46", "47", "48", "49", "50"};
    GPSTracker gpsTracker;
    private ProgressDialog pDialog;
    private String imagePath;
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private JSONObject success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.signup_activity);

        circleImageView = (CircleImageView) findViewById(R.id.profile_image);
//        checkAndRequestPermissions();
        gpsTracker = new GPSTracker(this);

        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        }

        SharedPreferences app_preferences = getSharedPreferences("livecampus", MODE_PRIVATE);
        editor = app_preferences.edit();

        getSupportActionBar().hide();

        Intent intent = getIntent();

        uid = intent.getStringExtra("University");
        camp_id = intent.getStringExtra("Campus");
        mid = intent.getStringExtra("Major");
        year_id = intent.getStringExtra("Year");

        Log.i("data of university", uid + " " + camp_id + " " + mid + " " + year_id);

        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        pref = getSharedPreferences("contact", Context.MODE_PRIVATE);

        System.out.print("data is " + sp.getString(Constant.year_id, ""));
        spnage = (Spinner) findViewById(R.id.spnage);
        txt_next = (TextView) findViewById(R.id.txt_next);
        editName = (EditText) findViewById(R.id.userName);
        radiogroup = (RadioGroup) findViewById(R.id.gender);
        editPassword = (EditText) findViewById(R.id.userPassword);
        editEmail = (EditText) findViewById(R.id.userEmail);
        editConfirmPassword = (EditText) findViewById(R.id.userConfirmPassword);
        user_image = (ImageView) findViewById(R.id.user_image);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();

            }
        });




        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cod);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnage.setAdapter(dataAdapter);
        spnage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // First item will be gray
                if (position == 0) {
                    ((TextView) view).setText("Select Age");
                    ((TextView) view).setTextColor(Color.GRAY);
                } else {

                    sp2 = (String) spnage.getItemAtPosition(position);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        txt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editName.getText().toString().length() == 0) {
                    editName.setError("Enter Name");
                    editName.requestFocus();
                } else if (!editEmail.getText().toString().matches(emailPattern)) {
                    editEmail.requestFocus();
                    editEmail.setError("Enter Valid Email");
                } else if (editPassword.getText().toString().length() == 0) {
                    editPassword.setError("Enter Password");
                    editPassword.requestFocus();
                } else if (sp2 == null) {
                    Toast.makeText(getApplicationContext(), "Please select Age", Toast.LENGTH_SHORT).show();


                } else if (radiogroup.getCheckedRadioButtonId() == -1) {

                    Toast.makeText(getApplicationContext(), "Please select Gender", Toast.LENGTH_SHORT).show();
                } else if (editConfirmPassword.getText().toString().length() == 0) {
                    editConfirmPassword.setError("Please confirm password");
                    editConfirmPassword.requestFocus();
                } else if (!editPassword.getText().toString().equals(editConfirmPassword.getText().toString())) {
                    editConfirmPassword.setError("Password Not matched");
                    editConfirmPassword.requestFocus();
                } else {
                    name = editName.getText().toString().trim();
                    email = editEmail.getText().toString().trim();
                    password = editPassword.getText().toString().trim();

                    if (imagePath != null) {
                        new PostDataTOServer().execute();

                    } else {
                        imagePath = "";

                        new PostDataTOServer().execute();
                        Log.e("image ", "ok ");

                    }
                }
            }
        });




       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, cod);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnage.setAdapter(adapter);*/


    }


    private boolean checkAndRequestPermissions() {
        int internet = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int READ = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (internet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (READ != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        System.out.println("yoo" + encodedImage);
        return encodedImage;
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void selectImage() {
        try {
            PackageManager pm = Signup_Activity.this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, Signup_Activity.this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Signup_Activity.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(Signup_Activity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(Signup_Activity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            Bitmap bm1 = null;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");


                byte imageInByte[] = bytes.toByteArray();
                imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);

                //imgPath = destination.getAbsolutePath();
                circleImageView.setImageBitmap(bm1);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Bitmap bm = null;
            Bitmap bm1 = null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(Signup_Activity.this.getContentResolver(), data.getData());
                    Matrix matrix = new Matrix();
                    // matrix.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte imageInByte[] = stream.toByteArray();
                    imagePath = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                    bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            circleImageView.setImageBitmap(bm1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(Signup_Activity.this, Second_Activity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Signup_Activity.this);
            pDialog.setMessage("Please wait...");
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "register.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("uid", uid);
                jo.put("mid", mid);
                jo.put("campusname", camp_id);
                jo.put("year_id", year_id);
                jo.put("name", name);
                jo.put("email", email);
                jo.put("password", password);
                jo.put("image", imagePath);

                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);

            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);


                    {
                        System.out.println("the vb login j " + j);
                        String rid = j.getString("rid");
                        editor.putString("rid", rid).commit();
                        editor.putString("uid", j.getString("uid")).commit();
                        editor.putString("mid", j.getString("mid")).commit();
                        editor.putString("campusname", j.getString("campusname")).commit();
                        editor.putString("year_id", j.getString("year_id")).commit();
                        editor.putString("name", j.getString("name")).commit();
                        editor.putString("email", j.getString("email")).commit();
                        editor.putString("image", j.getString("image")).commit();
                        editor.putBoolean("isLogged", true).commit();
                        String profile = j.getString("image");
                        String name = j.getString("name");
                        sp.edit().putString(Constant.profile, profile).commit();
                        sp.edit().putString(Constant.username, name).commit();
                        sp.edit().putString(Constant.uid, name).commit();
                        sp.edit().putBoolean(Constant.scan, true).commit();
                        String rid_id = j.getString("rid");
                        sp.edit().putString(Constant.rid, rid_id).commit();
                        editor.putBoolean("isFirstTime", false).commit();
                        Intent intent = new Intent(getApplicationContext(), Login_Activity.class);
                        startActivity(intent);
                        finish();
                    }

                } else if (success.getString("success").equals("0")) {

                    String msg = success.getString("message");
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    pDialog.dismiss();

                } else {
                    pDialog.dismiss();
                }

            } catch (Exception e) {
                pDialog.dismiss();
            }
        }
    }


}
