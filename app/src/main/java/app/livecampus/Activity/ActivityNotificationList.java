package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.GroupModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;

/**
 * Created by isquare3 on 6/27/18.
 */

public class ActivityNotificationList extends AppCompatActivity {

    public TextView txt_NoData, blubtext, edate, tv_courseName, tvCram;
    RecyclerView listView;
    GridLayoutManager mLayoutManager;
    String Prefrence = "Prefrence";
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    SharedPreferences sp;
    ArrayList<GroupModel> arrayList = new ArrayList<>();
    String rid, groupID, value;
    ProgressDialog pDialog;
    int position;
    private JSONObject success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_notification_activity);
        getSupportActionBar().setTitle("Notifications");

        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);

        listView = (RecyclerView) findViewById(R.id.listview);
        txt_NoData = (TextView) findViewById(R.id.txt_NoData);

        mLayoutManager = new GridLayoutManager(this, 1);
        listView.setLayoutManager(mLayoutManager);
        listView.setHasFixedSize(true);


        new Receivemessage().execute();
        new clearGroupCount().execute();

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        //respond to menu item selection
        switch (item.getItemId()) {
            case R.id.action_home:
                Intent intent = new Intent(ActivityNotificationList.this, HomeActivity.class);
                startActivity(intent);
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(i);
        finish();


    }

    public class Receivemessage extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ActivityNotificationList.this);
            pDialog.setMessage("Please wait...");
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "invite_request.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();

                jo.put("status", "Invite");//userid
                jo.put("rid", sp.getString(Constant.rid, ""));//userid

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception in invite_request : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                if (pDialog.isShowing())
                    pDialog.dismiss();

                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("post");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {

                            GroupModel category = new GroupModel();

                            success = jsonArrayCategory.getJSONObject(i);
                            category.setGroupID("" + success.getString("group_id"));
                            category.setGruopName("" + success.getString("group_name"));
                            category.setBulb("" + success.getString("group_blub"));
                            category.setDate("" + success.getString("group_date"));
                            category.setStatus("" + success.getString("group_type"));
                            category.setGroupImage("" + success.getString("group_image1"));


                            arrayList.add(category);
                        }
                        adapter = new MyAdapter(ActivityNotificationList.this, arrayList);
                        listView.setAdapter(adapter);
                        pDialog.dismiss();

                    } else if (success.getString("success").equals("0")) {

                        listView.setVisibility(View.GONE);
                        pDialog.dismiss();


//                         Toast.makeText(ActivityNotificationList.this, ""+success.getString("post"), Toast.LENGTH_LONG).show();
                        txt_NoData.setVisibility(View.VISIBLE);
                        txt_NoData.setText("" + success.getString("post"));

                    }

                } catch (JSONException e) {
                    System.out.println("the invite_request exeption is :" + e);
                    pDialog.dismiss();

                    e.printStackTrace();
                }
            }

        }
    }

    private class clearGroupCount extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "clear_push_groupchat.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();
                jo.put("rid", sp.getString(Constant.rid, ""));

                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);
                try {
                    if (success.getString("success").equals("1")) {
                    } else if (success.getString("success").equals("0")) {
                    }
                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }
        }
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        ProgressDialog pDialog;
        Context context;
        ArrayList<GroupModel> getDataAdapter;
        SharedPreferences sp;
        String buyerid;

        public MyAdapter(Context context, ArrayList<GroupModel> arrayList) {

            super();
            this.context = context;
            this.getDataAdapter = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_notification_list, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final MyAdapter.ViewHolder holder, final int position) {

            GroupModel getDataAdapter1 = getDataAdapter.get(position);
            sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

            holder.mGroupNameTv.setText("" + getDataAdapter1.getGruopName());
            holder.mGroupBlubTv.setText("" + getDataAdapter1.getBulb());
            holder.mGroupStatusTv.setText("" + getDataAdapter1.getStatus());
            holder.mGroupDateTv.setText("" + getDataAdapter1.getDate());
            holder.mGroupBlubTv.setText("" + getDataAdapter1.getBulb());

            holder.itemView.setVisibility(View.VISIBLE);

            if (getDataAdapter1.getStatus().equalsIgnoreCase("Exam")) {
                holder.linearColor.setBackgroundColor(getResources().getColor(R.color.red));
            } else {
                holder.linearColor.setBackgroundColor(getResources().getColor(R.color.lightblue));

            }

            if (getDataAdapter1.getGroupImage().equalsIgnoreCase("")) {

                Picasso.with(context).load(R.drawable.ic_launcher).placeholder(R.drawable.ic_launcher).into(holder.mGroupImage);

            } else {
                Picasso.with(context).load(getDataAdapter1.getGroupImage()).placeholder(R.drawable.ic_launcher).into(holder.mGroupImage);

            }


        }

        @Override
        public int getItemCount() {

            return getDataAdapter.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView mGroupImage;
            private TextView mGroupNameTv;
            private TextView mGroupBlubTv;
            private TextView mGroupStatusTv;
            private TextView mGroupDateTv;
            private Button mBtnAccept, btnDecline;
            private LinearLayout linearColor, linearMain;

            public ViewHolder(View itemView) {

                super(itemView);
                mGroupImage = (ImageView) itemView.findViewById(R.id.groupImage);
                mGroupNameTv = (TextView) itemView.findViewById(R.id.tv_groupName);
                mGroupBlubTv = (TextView) itemView.findViewById(R.id.tv_groupBlub);
                mGroupStatusTv = (TextView) itemView.findViewById(R.id.tv_groupStatus);
                mGroupDateTv = (TextView) itemView.findViewById(R.id.tv_groupDate);
                mBtnAccept = (Button) itemView.findViewById(R.id.btnAccept);
                btnDecline = (Button) itemView.findViewById(R.id.btnDecline);
                linearColor = (LinearLayout) itemView.findViewById(R.id.linearColor);
                linearMain = (LinearLayout) itemView.findViewById(R.id.linearMain);


                mBtnAccept.setOnClickListener(this);
                btnDecline.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                GroupModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());

                switch (v.getId()) {
                    case R.id.btnAccept:
                        value = "1";
                        groupID = getDataAdapter1.getGroupID();

                        new AcceptDecline().execute();

                        break;

                    case R.id.btnDecline:
                        value = "-1";
                        groupID = getDataAdapter1.getGroupID();
                        position=getAdapterPosition();
                        new AcceptDecline().execute();

                        break;
                }
            }
        }


        private class AcceptDecline extends AsyncTask<Void, Void, Void> {


            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(true);
                pDialog.show();

            }

            @Override
            protected Void doInBackground(Void... params) {
                String serverUrl = Constant.MAIN_URL + "invite_request.php";
                System.out.println("the url is :" + serverUrl);

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(serverUrl);


                try {
                    JSONObject jo = new JSONObject();

                    jo.put("rid", sp.getString(Constant.rid, ""));
                    jo.put("status", "Update");
                    jo.put("group_id", "" + groupID);
                    jo.put("value", value);

                    System.out.println("the send invite data " + jo);

                    StringEntity se = new StringEntity(jo.toString());
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    post.setEntity(se);
                    HttpResponse response = client.execute(post);
                    HttpEntity entity = response.getEntity();
                    String responseText = EntityUtils.toString(entity);
                    System.out.println("the response is :" + responseText);
                    success = new JSONObject(responseText);
                    pDialog.dismiss();

                } catch (Exception e) {
                    pDialog.dismiss();

                    System.out.println("Exception invite_request : " + e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (pDialog.isShowing())
                    pDialog.dismiss();

                try {


                    if (success.getString("success").equals("1")) {
                        adapter.notifyDataSetChanged();

                        if (value.equalsIgnoreCase("1")) {
                            Intent invite = new Intent(context, Chat_Tabbar.class);
                            invite.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(invite);
                            finish();
                        } else {
                            /*Intent decline = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(decline);
                            finish();*/
                            getDataAdapter.remove(position);
                            adapter.notifyDataSetChanged();

                        }

                        Log.e("arraylist size", "" + arrayList.size());


                    } else if (success.getString("success").equals("0")) {
                        pDialog.dismiss();

                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the invite_request exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }

}

