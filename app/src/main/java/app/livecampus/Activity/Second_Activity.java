package app.livecampus.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

import app.livecampus.Pojo.Univercity_Model;
import app.livecampus.R;
import app.livecampus.Services.GPSTracker;
import app.livecampus.Utils.Constant;

/**
 * Created by isquare3 on 22/09/17.
 */

public class Second_Activity extends AppCompatActivity {


    StringBuffer campusNameList = new StringBuffer();
    TextView txt_next;
    Spinner spnUniversity, spnMajor, spnYear;
    Spinner spnTwo;
    String img, campus_Name, majorname, yeare;
    ProgressDialog pDialog;
    Univercity_Model category;
    ArrayAdapter<String> spinnerArrayUnivercity;
    ArrayAdapter<String> spinnerMajor;
    ArrayAdapter<String> spinnerYear;
    ArrayAdapter<String> spinnerArrayCampus;
    String strUid, strMajorid, strCampus_id, strYear, strparent_id, strSession_id, parent_id;
    SharedPreferences sp, pref;
    String Prefrence = "Prefrence";
    EditText edtAssignment;
    GPSTracker gpsTracker;
    private ArrayList<String> selectedCampusList;
    private ImageView circleImageView;
    private ArrayList<String> univercityName;
    private ArrayList<String> campusName;
    private ArrayList<String> yearName;
    private ArrayList<String> univercity_logo;
    private ArrayList<String> majorName;
    private ArrayList<String> parentNameArrayList;
    private ArrayList<String> classId;
    private ArrayList<String> Uid;
    private ArrayList<String> campusID;
    private ArrayList<String> majorID;
    private ArrayList<String> yearID;
    private JSONObject success;
    private ArrayList<Univercity_Model> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.secondpage_activity);
        getSupportActionBar().hide();


        new PostDataTOSession().execute();
        new GetMajorData().execute();
        new GetYEarData().execute();
        gpsTracker = new GPSTracker(this);

        if (!gpsTracker.canGetLocation()) {
            gpsTracker.showSettingsAlert();
        }


        pDialog = new ProgressDialog(Second_Activity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(true);
        pDialog.show();


        sp = getSharedPreferences(Prefrence, Context.MODE_PRIVATE);
        pref = getSharedPreferences("contact", Context.MODE_PRIVATE);

        txt_next = (TextView) findViewById(R.id.txt_next);

        circleImageView = (ImageView) findViewById(R.id.ulogo);


        spnUniversity = (Spinner) findViewById(R.id.spnOne);
        spnMajor = (Spinner) findViewById(R.id.spnThree);
        spnYear = (Spinner) findViewById(R.id.spnFour);

        spnTwo = (Spinner) findViewById(R.id.spnTwo);


        arrayList = new ArrayList<>();
        univercityName = new ArrayList<>();
        campusName = new ArrayList<>();
        majorName = new ArrayList<>();
        yearName = new ArrayList<>();

        parentNameArrayList = new ArrayList<>();

        classId = new ArrayList<>();
        Uid = new ArrayList<>();
        campusID = new ArrayList<>();
        majorID = new ArrayList<>();
        yearID = new ArrayList<>();


        univercityName.add("Select University");
        campusName.add(" Select Your Primary Campus");
        majorName.add("Select Major");
        yearName.add("Select Year");
        campusName.add("Select campus");


        classId.add("*");
        Uid.add("*");
        campusID.add("*");
        majorID.add("*");
        yearID.add("*");

        //spninner

        spnUniversity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                } else {
                    strUid = Uid.get(position);
                    System.out.println("the Uid :" + strUid);
                    arrayList.clear();
                   /* parentNameArrayList.clear();
                    parentNameArrayList.removeAll(parentNameArrayList);
                    campusName.remove(campusNameList);*/
                    new PostDataTOSession_Get_by_Uni().execute();
                    new image().execute();


                }


            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    campusName.add(0, " Select Your Primary Campus");


                } else {
                    strCampus_id = campusID.get(position);
                    System.out.println("the campus id  :" + strCampus_id);

                    campus_Name = spnTwo.getSelectedItem().toString();
                    Log.e("campus name is", campus_Name);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

       /* spnTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {


                } else {


                    try {

                        parent_id = campusID.get(position);

                        Log.e("campus",parent_id);

                        selectedCampusList.clear();

                        try {
                            String xxx=spnTwo.getSelectedItemsAsString();

                            Log.e( "xxx",xxx );
                        }
                        catch (Exception e)
                        {

                        }


                        System.out.println("the campus id :" + parent_id);
                    }catch (Exception e)
                    {

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/

        spnMajor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    strMajorid = majorID.get(position);
                    System.out.println("the major id  :" + strMajorid);

                    majorname = spnMajor.getSelectedItem().toString();
                    Log.e("campus name is", majorname);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                } else {
                    strYear = yearID.get(position);
                    System.out.println("the year id  :" + strYear);

                    yeare = spnYear.getSelectedItem().toString();
                    Log.e("campus name is", yeare);
                }

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        txt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*try {
                    campusNameList.setLength(0);
                    for (String parentName : selectedCampusList) {

                        //my code
                        if (campusNameList.length() == 0) {

                            campusNameList.append(parentName);
                            campusNameList.append("");
                        }

                        else {
                            campusNameList.append("," + parentName);
                            campusNameList.append("");
                        }

                    }
                }catch (Exception e)
                {
                    System.out.print(""+e.getMessage());
                }
*/
                if (strUid == null)
                    Toast.makeText(Second_Activity.this, "Select University", Toast.LENGTH_SHORT).show();
                else if (strMajorid == null) {
                    Toast.makeText(Second_Activity.this, "Select Major", Toast.LENGTH_SHORT).show();
                } else if (strCampus_id == null) {
                    Toast.makeText(Second_Activity.this, "Select Your Primary Campus", Toast.LENGTH_SHORT).show();
                } else if (strYear == null) {
                    Toast.makeText(Second_Activity.this, "Select Year", Toast.LENGTH_SHORT).show();
                } else {

                    Intent intent = new Intent(Second_Activity.this, Signup_Activity.class);
                    intent.putExtra("University", strUid);
                    intent.putExtra("Campus", "" + strCampus_id);
                    intent.putExtra("Major", strMajorid);
                    intent.putExtra("Year", strYear);

                    sp.edit().putString(Constant.year_id, strYear).commit();
                    sp.edit().putString(Constant.mid, strMajorid).commit();
                    sp.edit().putString(Constant.campus_name, strCampus_id).commit();
                    sp.edit().putString(Constant.campus_id, strCampus_id).commit();
                    sp.edit().putString(Constant.uid, strUid).commit();

                    sp.edit().putString(Constant.StrCampus_name, campus_Name).commit();
                    sp.edit().putString(Constant.Strmajorname, majorname).commit();
                    sp.edit().putString(Constant.Stryeare, yeare).commit();

                    startActivity(intent);
                    finish();


                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(Second_Activity.this, MainActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private class PostDataTOSession extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getunivercities.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            univercityName.add(j.getString("univercity_name"));
                            // sessionName.add(j.getString("session_name"));


                            Uid.add(j.getString("uid"));//report id
                        }


                        spinnerArrayUnivercity = new ArrayAdapter<String>(Second_Activity.this,
                                android.R.layout.simple_spinner_item, univercityName);
                        spinnerArrayUnivercity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnUniversity.setAdapter(spinnerArrayUnivercity);
                        spinnerArrayUnivercity.notifyDataSetChanged();

//                        campusNameList.notify();
//                        selectedCampusList.notify();
//                        selectedCampusList.clear();


                        if (spinnerArrayUnivercity != null) {

                            spnUniversity.setAdapter(spinnerArrayUnivercity);

//                            sp.edit().putString( Constant.selected,"").clear();


                            spinnerArrayUnivercity.notifyDataSetChanged();


                        } else {

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
                    pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                pDialog.hide();
            }
        }

    }


    //get_univercity_value

    private class GetMajorData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getmajors.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            majorName.add(j.getString("majors_name"));
                            // sessionName.add(j.getString("session_name"));


                            majorID.add(j.getString("mid"));//report id
                        }


                        spinnerMajor = new ArrayAdapter<String>(Second_Activity.this,
                                android.R.layout.simple_spinner_item, majorName);
                        spinnerMajor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnMajor.setAdapter(spinnerMajor);
                        spinnerMajor.notifyDataSetChanged();


                        if (spinnerMajor != null) {

                            spnMajor.setAdapter(spinnerMajor);

                            spinnerMajor.notifyDataSetChanged();


                        } else {
                            Toast.makeText(Second_Activity.this, "Call", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }

    private class PostDataTOSession_Get_by_Uni extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getcampusbyuniversity.php";

            campusName.clear();

            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("uid", "" + strUid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        if (success.getString("success").equals("1")) {
                            try {
                                JSONArray jsonArrayClass = success.getJSONArray("posts");
                                for (int i = 0; i < jsonArrayClass.length(); i++) {
                                    JSONObject j = jsonArrayClass.getJSONObject(i);
                                   /* campusName.add(j.getString("camp_name"));
                                    campusID.add(j.getString("camp_id"));*/
                                    campusName.add(j.getString("camp_name"));
                                    // sessionName.add(j.getString("session_name"));


                                    campusID.add(j.getString("camp_id"));//report id
                                    String campusID = j.getString("camp_id");
                                    sp.edit().putString(Constant.campus_id, campusID).commit();
                                }


                                spinnerArrayCampus = new ArrayAdapter<String>(Second_Activity.this,
                                        android.R.layout.simple_spinner_item, campusName);
                                spinnerArrayCampus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spnTwo.setAdapter(spinnerArrayCampus);
                                spinnerArrayCampus.notifyDataSetChanged();


                                if (spinnerArrayCampus != null) {

                                    spnTwo.setAdapter(spinnerArrayCampus);

                                    spinnerArrayCampus.notifyDataSetChanged();


                                } else {
                                    Toast.makeText(Second_Activity.this, "Call", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {

                                System.out.println("the title exe is :" + e);
                                e.printStackTrace();
                            }
                            // pDialog.hide();
                        } else if (success.getString("success").equals("0")) {
                            // pDialog.hide();
                        } else {

                        }
                    } catch (Exception e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                        //pDialog.hide();
                    }
                    //  pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    //pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }

       /* @Override
        public void onItemsSelected(boolean[] selected) {
            sp.edit().putString( Constant.selected,""+campusNameList).commit();

        }

        @Override
        public void onItemsTextSelected(ArrayList<String> selectedItemText) {


                selectedCampusList = selectedItemText;*/




           /* sp.edit().putString( Constant.selected,""+campusNameList).commit();



            Log.e( "shared data",sp.getString( Constant.selected,"" ) );*/
           /* try {


                campusNameList.setLength(0);
                for (String parentName : selectedCampusList) {

                    //my code
                    if (campusNameList.length() == 0) {
                        campusNameList.append(parentName);
                        campusNameList.append("");


                        Log.e( "single",""+campusNameList );
//                        spnTwo.setItems( campusName,""+campusNameList,listener );


                    }

                    else {


                        campusNameList.append("," + parentName);
                        campusNameList.append("");

                        Log.e( "multi else",""+campusNameList );
                        spnTwo.setItems( campusName,""+campusNameList,listener );

                    }

                }

            }catch (Exception e)
            {
                System.out.print(""+e.getMessage());
            }
*/


    }

    private class image extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
           /* pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "get_univercity_logo.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("uid", "" + strUid);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if(pDialog.isShowing())
                pDialog.dismiss();*/

            try {
                if (success.getString("success").equals("1")) {
                    try {
                        if (success.getString("success").equals("1")) {
                            try {
                                JSONArray jsonArrayClass = success.getJSONArray("posts");
                                for (int i = 0; i < jsonArrayClass.length(); i++) {
                                    JSONObject j = jsonArrayClass.getJSONObject(i);

                                    img = (j.getString("univercity_logo"));
                                }

                                try {

                                    img = img.replaceAll(" ", "%20");
                                    URL sourceUrl = new URL(img);
                                    Picasso.with(Second_Activity.this)
                                            .load("" + sourceUrl)
                                            .into(circleImageView);


                                } catch (Exception e) {

                                }


                            } catch (JSONException e) {

                                System.out.println("the title exe is :" + e);
                                e.printStackTrace();
                            }
                            // pDialog.hide();
                        } else if (success.getString("success").equals("0")) {
                            // pDialog.hide();
                        } else {

                        }
                    } catch (Exception e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                        //pDialog.hide();
                    }
                    //  pDialog.hide();
                } else if (success.getString("success").equals("0")) {
                    //pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
                //pDialog.hide();
            }
        }


    }

    private class GetYEarData extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "getschoolyears.php";


            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(serverUrl);

            try {
                JSONObject jo = new JSONObject();
                System.out.println("the send data is :" + jo);
                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                //  post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
          /*  if(pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {
                    try {
                        JSONArray jsonArrayClass = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayClass.length(); i++) {
                            JSONObject j = jsonArrayClass.getJSONObject(i);
                            yearName.add(j.getString("year_name"));
                            // sessionName.add(j.getString("session_name"));
                            yearID.add(j.getString("year_id"));//report id
                        }


                        spinnerYear = new ArrayAdapter<String>(Second_Activity.this, android.R.layout.simple_spinner_item, yearName);
                        spinnerYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spnYear.setAdapter(spinnerYear);
                        spinnerYear.notifyDataSetChanged();


                        if (spinnerYear != null) {

                            spnYear.setAdapter(spinnerYear);

                            spinnerYear.notifyDataSetChanged();

                        } else {
                            Toast.makeText(Second_Activity.this, "Call", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        System.out.println("the title exe is :" + e);
                        e.printStackTrace();
                    }
//                      pDialog.hide();
                } else if (success.getString("success").equals("0")) {
//                    pDialog.hide();
                } else {

                }
            } catch (Exception e) {
                System.out.println("the title exe is :" + e);
                e.printStackTrace();
//                pDialog.hide();
            }
        }

    }
}
