package app.livecampus.Activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.frosquivel.magicaltakephoto.MagicalTakePhoto;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.livecampus.R;
import app.livecampus.Services.GPSTracker;
import app.livecampus.Utils.Constant;
import app.livecampus.Utils.GetCurrentGPSLocation;

import static android.view.View.INVISIBLE;
import static android.view.View.OnClickListener;
import static android.view.View.OnFocusChangeListener;
import static android.view.View.VISIBLE;

/**
 * Created by isquare3 on 22/09/17.
 */

public class Barcode_Four extends AppCompatActivity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static File imagePath;
    private final int PICK_IMAGE_CAMERA = 1;
    public View view1, view2;
    ImageView barcode_btn, done, cancel, isbn_code_image, bookimage, barcodeimage, imageCamera;
    IntentResult result;
    Dialog dialog;
    Button add_submit, back;
    TextView edit_courceNo, bookname;
    EditText editCName;
    String countryName,COURSENAME1,COURSENAME2,COURSENAME3, editcourse, urlOfImage, titleBook, IMAGE3, CNAME3, CISBN3, ISBNcode, frm, CNAME1, CISBN1, IMAGE1, ISBN_NO, IMG_URL, IMAGE2, CNAME2, CISBN2;
    boolean isAvailable = true;
    double longitude, latitude;
    String TAG = "Barcode_Four";
    IntentIntegrator integrator;
    ImageView imageView;
    MagicalTakePhoto magicalTakePhoto;
    Bitmap bitmap;
    InputStream inputStreamImg;
    ProgressDialog pDialog;
    JSONObject success;
    ProgressBar progressBar;
    SharedPreferences app_sharedPreferences;
    SharedPreferences.Editor editor;
    LocationManager locationManager;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    GPSTracker gpsTracker;
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location location, loc;
    double lat_search, lng_search;
    boolean isBase = false;
    String strBase1,strBase2,strBase3;

    public static String getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryName();
            }
        } catch (IOException ioe) {
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAndRequestPermissions();

        app_sharedPreferences = getSharedPreferences("livecampus", Context.MODE_PRIVATE);
        editor = app_sharedPreferences.edit();

        magicalTakePhoto = new MagicalTakePhoto(this);
        setContentView(R.layout.barcode_activity);

        Toast.makeText(getApplicationContext(), "Scan your textbook.", Toast.LENGTH_SHORT).show();

        getSupportActionBar().hide();

        gpsTracker = new GPSTracker(Barcode_Four.this);

        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        GetCurrentGPSLocation gps = new GetCurrentGPSLocation(Barcode_Four.this);

        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            lat_search = latitude;
            lng_search = longitude;

            countryName = getCountryName(this, lat_search, lng_search);


        } else {

            gps.showSettingsAlert();
        }

        Intent intent = getIntent();
        IMAGE1 = intent.getStringExtra(Constant.IMAGE1);
        CNAME1 = intent.getStringExtra(Constant.CNAME1);
        CISBN1 = intent.getStringExtra(Constant.cISBN1);
        COURSENAME1 = intent.getStringExtra(Constant.COURSENAME1);
        strBase1 = intent.getStringExtra("1");


        IMAGE2 = intent.getStringExtra(Constant.IMAGE2);
        CNAME2 = intent.getStringExtra(Constant.CNAME2);
        CISBN2 = intent.getStringExtra(Constant.cISBN2);
        COURSENAME2 = intent.getStringExtra(Constant.COURSENAME2);
        strBase2 = intent.getStringExtra("2");

        IMAGE3 = intent.getStringExtra(Constant.IMAGE3);
        CNAME3 = intent.getStringExtra(Constant.CNAME3);
        CISBN3 = intent.getStringExtra(Constant.cISBN3);
        COURSENAME3 = intent.getStringExtra(Constant.COURSENAME3);
        strBase3 = intent.getStringExtra("3");

        Log.e("get strBase1 ",strBase1 );
        Log.e("get strBase2 ",strBase2 );
        Log.e("get strBase3 ",strBase3 );

        back = (Button) findViewById(R.id.btn_back);
        add_submit = (Button) findViewById(R.id.btn_Add);

        edit_courceNo = (TextView) findViewById(R.id.edit_CourceNo);
        editCName = (EditText) findViewById(R.id.txtName);
        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);

        bookname = (TextView) findViewById(R.id.bookname);
        bookimage = (ImageView) findViewById(R.id.bookimage);
        barcodeimage = (ImageView) findViewById(R.id.barcodeimage);

        editCName.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        editCName.setSelection(editCName.getText().length());

        barcode_btn = (ImageView) findViewById(R.id.barcode_btn);

        barcode_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editCName.getText().toString().trim().equals("") && edit_courceNo.getText().toString().trim().equals("") && editCName.getText().toString().trim().equals("")) {
                    scan();

                } else {
                    Toast.makeText(getApplicationContext(), "You are enter ISBN No.", Toast.LENGTH_LONG).show();
                }
            }
        });
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Barcode_Four.this, Barcode_Three.class);
                startActivity(intent1);
            }
        });

        add_submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /* addBook = false;*/
                editcourse = editCName.getText().toString().trim().toUpperCase();


                if (editCName.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter Course Name", Toast.LENGTH_LONG).show();

                } else if (!editCName.getText().toString().trim().equals("") || !edit_courceNo.getText().toString().trim().equals("")) {
                    new Barcode_Four.PostDataTOServer1().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter or scan the isbn code", Toast.LENGTH_LONG).show();
                }

            }
        });
        edit_courceNo.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    barcode_btn.setEnabled(false);
                    barcode_btn.setClickable(false);
                }
            }
        });
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void scan() {
        integrator = new IntentIntegrator(this);
        integrator.setPrompt("Scan a barcode or ISBN Number");
        Intent intent = new Intent(String.valueOf(integrator.setCameraId(0)));
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(true);
        integrator.setOrientationLocked(true);
        imagePath = new File(android.os.Environment.getExternalStorageState().toString() + "one.jpeg");
        integrator.initiateScan();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imagePath);

    }

    private void selectImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        //Toast.makeText(getApplicationContext(),"1",Toast.LENGTH_LONG).show();
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            Bitmap bm1 = null;
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");


                byte imageInByte[] = bytes.toByteArray();
                urlOfImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);
                bm1 = BitmapFactory.decodeByteArray(imageInByte, 0, imageInByte.length);

                //imgPath = destination.getAbsolutePath();
                imageView.setImageBitmap(bm1);
                bookimage.setImageBitmap(bm1);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

            magicalTakePhoto.resultPhoto(requestCode, resultCode, data);

            frm = result.getContents();
            frm = result.getBarcodeImagePath();

            if (result != null) {
                if (result.getContents() == null) {
                    //llSearch.setVisibility(View.GONE);

                    //Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                } else {

                    dialog = new Dialog(Barcode_Four.this);
                    dialog.setContentView(R.layout.popup_notification_item);
                    dialog.setCancelable(false);

                    final TextView subtitle = (TextView) dialog.findViewById(R.id.txt_pop_subtitle);
                    final TextView title = (TextView) dialog.findViewById(R.id.txt_pop_title);

                    ImageView imageView = (ImageView) dialog.findViewById(R.id.imageView2);

                    ImageView done = (ImageView) dialog.findViewById(R.id.done);
                    ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
                    ImageView isbn_code_image = (ImageView) dialog.findViewById(R.id.isbn_code_image);

                    String titleISBN = "ISBN " + result.getContents();
                    progressBar = (ProgressBar) dialog.findViewById(R.id.progressbar);
                    title.setText(titleISBN);

                    titleBook = subtitle.getText().toString();

                    String urlLink = null;
                    Log.i("Country Name", countryName);
                    if (result.getContents().length() == 10) {
                        if (countryName.equals("India")) {
                            urlLink = "https://www.amazon.in/dp/product/" + result.getContents() + "/";
                        } else if (countryName.equals("Canada")) {
                            urlLink = "https://www.amazon.ca/dp/product/" + result.getContents() + "/";
                        }
                    } else {
                        if (countryName.equals("India")) {
                            urlLink = "https://www.amazon.in/s/field-keywords=" + result.getContents();
                        } else if (countryName.equals("Canada")) {
                            urlLink = "https://www.amazon.ca/s/field-keywords=" + result.getContents();
                        }
                    }

                    Log.i("link = ", urlLink);
                    final downloadImages downloadImagess = new downloadImages(this, imageView, subtitle, result.getContents().length());
                    downloadImagess.execute(urlLink);

                    dialog.show();

                    cancel.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            downloadImagess.cancel(true);
                        }
                    });
                    done.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            isBase = false;

                            edit_courceNo.setText(result.getContents());
                            bookimage.setVisibility(VISIBLE);
                            view1.setVisibility(VISIBLE);
                            view2.setVisibility(VISIBLE);
                            barcode_btn.setVisibility(INVISIBLE);
                            barcodeimage.setVisibility(INVISIBLE);
                            editCName.setVisibility(VISIBLE);
                            String profile = urlOfImage;

                            if (profile.equals("")) {
                                Picasso.with(Barcode_Four.this)
                                        .load(R.drawable.user)
                                        .placeholder(R.drawable.user)
                                        .into(bookimage);

                            } else {
                                Picasso.with(Barcode_Four.this)
                                        .load(profile)
                                        .placeholder(R.drawable.user)
                                        .into(bookimage);
                            }
                            bookname.setText(subtitle.getText().toString().trim());
                            //bookisbn.setText(result.getContents());
                            dialog.dismiss();
                            edit_courceNo.setText(result.getContents());
                            ISBNcode = result.getContents();
                            downloadImagess.cancel(true);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    });
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
                magicalTakePhoto.resultPhoto(requestCode, resultCode, data);
                imageView.setImageBitmap(magicalTakePhoto.getMyPhoto());
            }
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int READ = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Barcode_Four.this, HomeActivity.class);
        startActivity(intent);

        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public String[] getHtml(String url) throws IOException {
        // Build and set timeout values for the request.
        String[] dataofURL = new String[2];
        URL urllink = new URL(url);
        if (isAvailable(urllink)) {
            URLConnection connection = urllink.openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            // Read and store the result line by line then return the entire string.
            InputStream in = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder html = new StringBuilder();
            for (String line; (line = reader.readLine()) != null; ) {
                if (line.contains("<title>")) {
                    dataofURL[0] = line;
                }
                if (line.contains("https") && line.contains(".jpg") && line.contains("var url")) {
                    Log.i("data", line);
                    html.append(line);
                    dataofURL[1] = line;
                }
            }
            in.close();
            return dataofURL;
        }
        return null;
    }

    public String[] getHtmlISBN13(String url) throws IOException {
        // Build and set timeout values for the request.
        String[] dataofURL = new String[2];
        URL urllink = new URL(url);
        if (isAvailable(urllink)) {
            URLConnection connection = urllink.openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            InputStream in = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder html = new StringBuilder();
            boolean counter = true;
            for (String line; (line = reader.readLine()) != null; ) {
                //Log.i("title",line);
                if (line.contains("<h2 data-attribute=")) {
                    dataofURL[0] = line;
                }
                if (line.contains("<img src=") && line.contains("srcset")) {
                    if (counter == true) {
                        dataofURL[1] = line;
                        html.append(line);
                        counter = false;
                    }
                }
            }
            in.close();
            return dataofURL;
        }
        return null;
    }

    public void customDialog() {

        dialog = new Dialog(Barcode_Four.this);
        dialog.setContentView(R.layout.popup_notification_item_noimage);
        dialog.setCancelable(false);

        final TextView subtitle = (TextView) dialog.findViewById(R.id.txt_pop_subtitle);
        final TextView title = (TextView) dialog.findViewById(R.id.txt_pop_title);

        imageView = (ImageView) dialog.findViewById(R.id.imageView2);

        done = (ImageView) dialog.findViewById(R.id.done);
        cancel = (ImageView) dialog.findViewById(R.id.cancel);
        isbn_code_image = (ImageView) dialog.findViewById(R.id.isbn_code_image);
        imageCamera = (ImageView) dialog.findViewById(R.id.imageCamera);
//        progressBar = (ProgressBar) dialog.findViewById(R.id.progressbar);
        final TextView txt_pop_subtitle = (TextView) dialog.findViewById(R.id.txt_pop_subtitle);

        String titleISBN = "ISBN " + result.getContents(); //pp

        Log.e("isbn", titleISBN);
        title.setText(titleISBN);


        //pp
        imageCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });


        done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(txt_pop_subtitle.getText().toString())) {
                    Toast.makeText(Barcode_Four.this, "Enter Book title", Toast.LENGTH_SHORT).show();
                } else {
                    if (dialog != null) {
                        dialog.dismiss();

                    }
                    isBase = true;
                    edit_courceNo.setText(result.getContents());
                    bookimage.setVisibility(VISIBLE);
                    view1.setVisibility(VISIBLE);
                    view2.setVisibility(VISIBLE);
                    barcode_btn.setVisibility(INVISIBLE);
                    barcodeimage.setVisibility(INVISIBLE);
                    editCName.setVisibility(VISIBLE);
                    try {
                        String profile = urlOfImage;
                        bookname.setText(subtitle.getText().toString().trim());
                        edit_courceNo.setText(result.getContents());
                        ISBNcode = result.getContents();

                        titleBook = txt_pop_subtitle.getText().toString();

                        Log.e("isbn is", "ok " + titleBook);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } catch (Exception e) {

                    }
                }

            }
        });
        dialog.show();

    }

    boolean isAvailable(URL url) {
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            urlConnection.disconnect();
            return true;
        } catch (IOException e) {
            Log.e("Exception", "Exception", e);
        }
        return false;
    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            if (!edit_courceNo.getText().toString().equals("")) {
                ISBN_NO = edit_courceNo.getText().toString().trim();
            } else {
                if (countryName.equals("India")) {
                    //ISBN_NO = editCName.getText().toString().trim();
                    if (ISBN_NO.length() == 10) {
                        new downloadImages1(getApplicationContext(), 10, false).execute("https://www.amazon.in/dp/product/" + ISBN_NO + "/");
                    }
                    if (ISBN_NO.length() == 13) {
                        new downloadImages1(getApplicationContext(), 13, false).execute("https://www.amazon.in/s/field-keywords=" + ISBN_NO);
                    }
                } else {
                    if (ISBN_NO.length() == 10) {
                        new downloadImages1(getApplicationContext(), 10, false).execute("https://www.amazon.ca/dp/product/" + ISBN_NO + "/");
                    }
                    if (ISBN_NO.length() == 13) {
                        new downloadImages1(getApplicationContext(), 13, false).execute("https://www.amazon.ca/s/field-keywords=" + ISBN_NO);
                    }
                }
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "addcourse.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", app_sharedPreferences.getString("rid", ""));
                jo.put(Constant.cISBN1, ISBN_NO);



                if (titleBook != null) {
                    jo.put(Constant.CNAME1, titleBook);

                    if (isBase == true) {
                        jo.put("type", "Base64");//pp
                        jo.put(Constant.IMAGE1, urlOfImage);

                    } else {

                        jo.put("type", "Url");//pp
                        jo.put(Constant.IMAGE1, urlOfImage);

                    }


                } else {
                    jo.put(Constant.CNAME1, ISBN_NO);
                    if (isBase == true) {
                        jo.put("type", "Base64");//pp
                        jo.put(Constant.IMAGE1, urlOfImage);

                    } else {

                        jo.put("type", "Url");//pp
                        jo.put(Constant.IMAGE1, urlOfImage);

                    }
                }

                jo.put(Constant.COURSENAME, editcourse);

                jo.put(Constant.PRICE, "0");

                jo.put(Constant.IMAGES, "");

                jo.put(Constant.DATE, "");

                jo.put(Constant.RATING, "");

                jo.put(Constant.scan, "yes");

                System.out.println("the send data is :" + jo);



                Log.e("send 3","  urlOfImage  "+urlOfImage );
                Log.e("send 3","  edit_courceNo  "+edit_courceNo.getText().toString().trim() );
                Log.e("send 3","  titleBook  "+titleBook );



                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);
                    {
                        System.out.println("the vb login j " + j);
                        Intent intent = new Intent(Barcode_Four.this, HomeActivity.class);
                        startActivity(intent);
                        editor.putBoolean("isFirstTime", false).commit();

                        finish();
                    }
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(Barcode_Four.this, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    private class PostDataTOServer1 extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Barcode_Four.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "addcourse.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", app_sharedPreferences.getString("rid", ""));

                jo.put(Constant.cISBN1, CISBN1);

                jo.put(Constant.CNAME1, CNAME1);

                if (strBase1.equalsIgnoreCase("yes")) {
                    jo.put("type", "Base64");//pp
                    jo.put(Constant.IMAGE1, IMAGE1);


                } else if(strBase1.equalsIgnoreCase("no")) {


                    jo.put("type", "Url");//pp
                    jo.put(Constant.IMAGE1, IMAGE1);

                }


                jo.put(Constant.COURSENAME, COURSENAME1);

                jo.put(Constant.PRICE, "0");

                jo.put(Constant.IMAGES, "");

                jo.put(Constant.DATE, "");

                jo.put(Constant.RATING, "");

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);
                    {
                        new PostDataTOServer2().execute();
                    }
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(Barcode_Four.this, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    private class PostDataTOServer2 extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "addcourse.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", app_sharedPreferences.getString("rid", ""));

                jo.put(Constant.cISBN1, CISBN2);

                jo.put(Constant.CNAME1, CNAME2);

                if (strBase2.equalsIgnoreCase("yes")) {
                    jo.put("type", "Base64");//pp
                    jo.put(Constant.IMAGE1, IMAGE2);


                } else if(strBase2.equalsIgnoreCase("no")) {


                    jo.put("type", "Url");//pp
                    jo.put(Constant.IMAGE1, IMAGE2);

                }


                jo.put(Constant.COURSENAME, COURSENAME2);

                jo.put(Constant.PRICE, "0");

                jo.put(Constant.IMAGES, "");

                jo.put(Constant.DATE, "");

                jo.put(Constant.RATING, "");

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);
                    {
                        new PostDataTOServer3().execute();
                    }
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(Barcode_Four.this, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    private class PostDataTOServer3 extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String serverUrl = Constant.MAIN_URL + "addcourse.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);
            try {
                JSONObject jo = new JSONObject();

                jo.put("rid", app_sharedPreferences.getString("rid", ""));

                jo.put(Constant.cISBN1, CISBN3);

                jo.put(Constant.CNAME1, CNAME3);

                if (strBase3.equalsIgnoreCase("yes")) {
                    jo.put("type", "Base64");//pp
                    jo.put(Constant.IMAGE1, IMAGE3);


                } else if(strBase3.equalsIgnoreCase("no")) {


                    jo.put("type", "Url");//pp
                    jo.put(Constant.IMAGE1, IMAGE3);

                }


                jo.put(Constant.COURSENAME, COURSENAME3);


                jo.put(Constant.PRICE, "0");

                jo.put(Constant.IMAGES, "");

                jo.put(Constant.DATE, "");

                jo.put(Constant.RATING, "");

                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);
            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");
                    System.out.println("my login JSONObject : " + j);
                    {
                        new PostDataTOServer().execute();
                    }
                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(Barcode_Four.this, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    public class downloadImages extends AsyncTask<String, Void, String> {
        Context context;
        ImageView imageView;
        String titleOfBook;
        TextView textView;
        int length;

        downloadImages(Context context, ImageView imageView, TextView textView, int lenght) {
            this.context = context;
            this.imageView = imageView;
            this.textView = textView;
            this.length = lenght;
        }

        protected void onPreExecute() {
            progressBar.setVisibility(VISIBLE);
            imageView.setVisibility(INVISIBLE);
        }

        @Override
        protected String doInBackground(String... string) {
            String webUrl = string[0];
            try {


                if (length == 10) {
                    String html[] = getHtml(webUrl);
                    IMG_URL = html[1].substring(html[1].indexOf('"') + 1, html[1].indexOf(';') - 1);

                    if (html.length > 0) {

                        try {

                            Log.i("img_url", IMG_URL);
                            Log.i("title", html[0] + countryName);
                            if (countryName.equals("India")) {
                                titleOfBook = html[0].substring(html[0].indexOf(">") + 4, html[0].indexOf("Book"));
                                titleOfBook = titleOfBook.replace("&#39;", "");
                            } else if (countryName.equals("Canada")) {
                                titleOfBook = html[0].substring(html[0].indexOf(">") + 1, html[0].indexOf(":"));
                                titleOfBook = titleOfBook.replace("&#39;", "");
                            }
                            titleBook = titleOfBook;


                            urlOfImage = IMG_URL;

                            if (urlOfImage.contains("data")) {
                                Log.e("has", "url is 10" + urlOfImage);
                                Log.e("data", "is match 10");


                                DownloadImageWithURLTask downloadTask = new DownloadImageWithURLTask(imageView, "yes");
                                downloadTask.execute(urlOfImage);
                            } else {
                                Log.e("data", "is not match 10");

                                Log.e("else has", "url is 10" + urlOfImage);
                                DownloadImageWithURLTask downloadTask = new DownloadImageWithURLTask(imageView, "no");
                                downloadTask.execute(IMG_URL);
                            }


                        } catch (Exception e) {

                        }

                    }
                }
                if (length == 13) {


                    String html[] = getHtmlISBN13(webUrl);

                    if (html.length > 0) {

                        try {
                            IMG_URL = html[1].substring(html[1].indexOf("=") + 2, html[1].indexOf(".jpg") + 4);

                            Log.e("image url ", "is " + IMG_URL);


                            Log.e("img_url", IMG_URL);
                            Log.e("title", html[0] + countryName);
                            Log.i("hello", "ok");

                            if (countryName.equals("India")) {

                                try {

                                    String title = html[0].substring(html[0].indexOf("<h2") + 1, html[0].indexOf("</h2>"));
                                    String title1 = title.substring(title.indexOf(">") + 1);
                                    Log.e("title", title1);
                                    titleOfBook = title1;
                                } catch (Exception e) {

                                }

                            } else {

                                try {
                                    Log.i("title", html[0]);
                                    String title = html[0].substring(html[0].indexOf("<h2") + 1, html[0].indexOf("</h2>"));
                                    String title1 = title.substring(title.indexOf(">") + 1);
                                    Log.i("title", title1);
                                    titleOfBook = title1;
                                } catch (Exception e) {

                                }

                            }
                            titleBook = titleOfBook;
                            urlOfImage = IMG_URL;
                            if (urlOfImage.contains("data")) {
                                Log.e("has", "url is 13" + urlOfImage);
                                Log.e("data", "is  match 13");

                                DownloadImageWithURLTask downloadTask = new DownloadImageWithURLTask(imageView, "yes");
                                downloadTask.execute(urlOfImage);
                            } else {
                                Log.e("data", "is not match 13");

                                Log.e("else has", "url is 13" + urlOfImage);
                                DownloadImageWithURLTask downloadTask = new DownloadImageWithURLTask(imageView, "no");
                                downloadTask.execute(IMG_URL);
                            }


                        } catch (Exception e) {

                        }


                    }
//
                    else if (html.length == 0) {
                        Log.e("null", "null");


                    }
                }

            } catch (IOException e) {
                isAvailable = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textView.setText(titleOfBook);
            if (!isAvailable) {
                Toast.makeText(context, "Please Enter proper ISBN Number.", Toast.LENGTH_LONG).show();
                isAvailable = true;
            }
            progressBar.setVisibility(INVISIBLE);
        }
    }

    public class downloadImages1 extends AsyncTask<String, Void, String> {
        Context context;
        String titleOfBook;
        int length;
        boolean isadd;

        downloadImages1(Context context, int length, boolean isadd) {
            this.context = context;
            this.length = length;
            this.isadd = isadd;
        }

        @Override
        protected String doInBackground(String... string) {
            String webUrl = string[0];
            Log.i("web url", webUrl);
            try {
                if (length == 10) {
                    String html[] = getHtml(webUrl);
                    if (html.length > 0) {
                        String IMG_URL = html[1].substring(html[1].indexOf('"') + 1, html[1].indexOf(';') - 1);
                        if (countryName.equals("India")) {
                            titleOfBook = html[0].substring(html[0].indexOf("Buy") + 4, html[0].indexOf("Book"));
                            titleOfBook = titleOfBook.replace("&#39;", "'");
                        } else if (countryName.equals("Canada")) {
                            titleOfBook = html[0].substring(html[0].indexOf(">") + 1, html[0].indexOf(":"));
                            titleOfBook = titleOfBook.replace("&#39;", "'");
                        }
                        titleBook = titleOfBook;
                        urlOfImage = IMG_URL;
                    }
                }
                if (length == 13) {
                    String html[] = getHtmlISBN13(webUrl);
                    if (html.length > 0) {
                        String IMG_URL = html[1].substring(html[1].indexOf("=") + 2, html[1].indexOf(".jpg") + 4);
                        Log.i("img_url", IMG_URL);
                        // Log.i("title", html[0] + countryName);
                        if (countryName.equals("India")) {
                            String title = html[0].substring(html[0].indexOf("<h2") + 1, html[0].indexOf("</h2>"));
                            String title1 = title.substring(title.indexOf(">") + 1);
                            Log.i("title", title1);
                            titleOfBook = title1;
                        } else {
                            Log.i("title", html[0]);
                            String title = html[0].substring(html[0].indexOf("<h2") + 1, html[0].indexOf("</h2>"));
                            String title1 = title.substring(title.indexOf(">") + 1);
                            Log.i("title", title1);
                            titleOfBook = title1;
                        }
                        titleBook = titleOfBook;
                        urlOfImage = IMG_URL;
                    }
                }
            } catch (IOException e) {
                isAvailable = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (!isAvailable) {
                Toast.makeText(context, "Please Enter proper ISBN Number.", Toast.LENGTH_LONG).show();
                isAvailable = true;
            }
        }
    }

    private class DownloadImageWithURLTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        String values;
        ProgressDialog progressDialog;

        public DownloadImageWithURLTask(ImageView bmImage, String values) {
            this.bmImage = bmImage;
            this.values = values;
        }

        protected Bitmap doInBackground(String... urls) {
            String pathToFile = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new URL(pathToFile).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error is ", e.getMessage());

                //pp
//                imageView.setBackgroundDrawable(getResources().getDrawable(R.drawable.no_image));
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {


            if (values.equalsIgnoreCase("yes")) {
                if (dialog != null) {
                    dialog.dismiss();

                }
                Log.e("content", "is ");
                customDialog();
            } else {


                Log.e("content", "else is ");
                bmImage.setVisibility(VISIBLE);
                bmImage.setImageBitmap(result);
            }

        }
    }
}


