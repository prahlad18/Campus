package app.livecampus.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Activity.Personalchat_activity;
import app.livecampus.Activity.Personalchatofferaccepted;
import app.livecampus.Pojo.SellBookModel_new;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 3/21/18.
 */

public class Sellandreject_adapter extends BaseAdapter {

    public String id;
    public String i;
    JSONObject success;
    ProgressDialog pDialog;
    SharedPreferences sp;
    Activity activity;
    String course_id, price, rating;
    String image, name;
    RatingBar ratingBar;
    Point p;
    ArrayList<String> imagesEncodedList;
    String book_id, book_name, book_image, rid, book_idc, buyer_id, book_price, buyusername, status, charnm;
    String selectedDate;
    ArrayList<SellBookModel_new> list = new ArrayList<>();
    private Activity context;
    private int PICK_IMAGE = 1;

    public Sellandreject_adapter(Activity context, ArrayList<SellBookModel_new> list, Activity activity) {
        this.context = context;
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.activity_sellandrejectitem, viewGroup, false);
        }
        TextView txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        TextView txtUserUniversity = (TextView) view.findViewById(R.id.txtUserUniversity);
        TextView txtUserMajor = (TextView) view.findViewById(R.id.txtUserMajor);
        TextView bdprice = (TextView) view.findViewById(R.id.bdprice);
        ImageView imageBook = (ImageView) view.findViewById(R.id.imageBook);
        CircleImageView imageUser = (CircleImageView) view.findViewById(R.id.imageUser);
        TextView txtUserYear = (TextView) view.findViewById(R.id.txtUserYear);
        TextView txtsellit = (TextView) view.findViewById(R.id.sellbook_price);
        RatingBar txtUserrating = (RatingBar) view.findViewById(R.id.txtNotification);
        TextView txtrejected = (TextView) view.findViewById(R.id.buy);
        TextView txtUsermsg = (TextView) view.findViewById(R.id.msg);

        sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        final SellBookModel_new model = (SellBookModel_new) getItem(i);
    /*
    final String title = getTitleOfBook(model.getTitle());
        tx.setText(title);*/

        book_id = model.getRecever_id();
        image = model.getImage();

        String imageuser = model.getUserimage();
        if (imageuser.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.user)
                    .placeholder(R.drawable.main_ic)
                    .into(imageUser);
        } else {
            Picasso.with(context)
                    .load(imageuser)
                    .placeholder(R.drawable.user)
                    .into(imageUser);
        }


        bdprice.setText("$" + model.getPrice() + ".00");


        name = model.getBookNane();

        String chars = capitalize(name);
        if (chars.equals("null")) {

            txtUserName.setText("");

        } else {

            txtUserName.setText(chars);

        }

        if (model.getUnivervity().equals("null")) {

            txtUserUniversity.setText("");

        } else {
            txtUserUniversity.setText(model.getUnivervity());
        }
        if (model.getMajor().equals("null")) {

            txtUserMajor.setText("");

        } else {
            txtUserMajor.setText(model.getMajor());


        }

        if (model.getYear().equals("null")) {

            txtUserYear.setText("");

        } else {
            txtUserYear.setText(model.getYear());

        }

        txtUsermsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("receiver_image", model.getUserimage());
                // Log.e("sendername", getDataAdapter1.getReciever_name());
                Intent i = new Intent(context, Personalchat_activity.class);
                i.putExtra("receiver_id", model.getBuyerid());
                i.putExtra("sendername", model.getBookNane());
                i.putExtra("senderimage", model.getUserimage());
                i.putExtra("msg", "msg");
                context.startActivity(i);
                context.finish();


               /* Intent i = new Intent(context, Msg_activitytwo.class);
                book_name=model.getBookname();
                book_image=model.getBookImage();
                book_id=model.getRecever_id();
                rid=model.getSellerid();
                buyer_id=model.getBuyerid();
                i.putExtra("user_chk",rid);
                i.putExtra("buyer_id",buyer_id);
                i.putExtra("book_id",book_id);
                i.putExtra("name",book_name);
                i.putExtra("image",book_image);
                //i.putExtra("rid",rid);
                Log.e("user_chk",rid);
                Log.e("book_id",book_id);
                Log.e("buyer_id",buyer_id);

                sp.edit().putString("bookName",book_name).commit();
                sp.edit().putString("book_id",book_id).commit();
                sp.edit().putString("bookImage",book_image).commit();

                context.startActivity(i);*/


            }
        });

        txtsellit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                status = "Accept";
                charnm = capitalize(sp.getString(Constant.username, ""));
                book_name = model.getBookname();
                book_image = model.getBookImage();
                buyusername = model.getBookNane();
                book_id = model.getRecever_id();
                rid = model.getSellerid();
                book_price = model.getPrice();
                buyer_id = model.getBuyerid();
                Intent i = new Intent(context, Personalchatofferaccepted.class);
                i.putExtra("receiver_id", model.getBuyerid());
                i.putExtra("sendername", model.getBookNane());
                i.putExtra("senderimage", model.getUserimage());
                i.putExtra("counter", "counter");

                i.putExtra("user_chk", rid);
                i.putExtra("buyer_id", buyer_id);
                i.putExtra("book_id", book_id);
                i.putExtra("offer_acceted", "Offer Accepted:");
                i.putExtra("name", book_name);
                i.putExtra("image", book_image);
                i.putExtra("price", book_price);
                //i.putExtra("rid",rid);
                Log.e("user_chk", rid);
                Log.e("book_id", book_id);
                Log.e("buyer_id", buyer_id);

                sp.edit().putString("bookName", book_name).commit();
                sp.edit().putString("book_id", book_id).commit();
                sp.edit().putString("bookImage", book_image).commit();

                context.startActivity(i);

                new Pushnotification().execute();

            /*    status="Accept";
                charnm = capitalize(sp.getString(Constant.username,""));
                new Pushnotification().execute();
                Intent i = new Intent(context, Countersellit_activity.class);
                book_name=model.getBookname();
                book_image=model.getBookImage();
                buyusername=model.getBookNane();
                book_id=model.getRecever_id();
                rid=model.getSellerid();
                book_price=model.getPrice();
                buyer_id=model.getBuyerid();
                i.putExtra("user_chk",rid);
                i.putExtra("buyer_id",buyer_id);
                i.putExtra("book_id",book_id);
                i.putExtra("offer_acceted","Offer Accepted:");
                i.putExtra("name",book_name);
                i.putExtra("image",book_image);
                i.putExtra("price",book_price);
                //i.putExtra("rid",rid);
                Log.e("user_chk",rid);
                Log.e("book_id",book_id);
                Log.e("buyer_id",buyer_id);

                sp.edit().putString("bookName",book_name).commit();
                sp.edit().putString("book_id",book_id).commit();
                sp.edit().putString("bookImage",book_image).commit();

                context.startActivity(i);*/


            }
        });

        txtrejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "Reject";
                charnm = capitalize(sp.getString(Constant.username, ""));
                Log.e("receiver_image", model.getUserimage());
                book_name = model.getBookname();
                book_image = model.getBookImage();
                buyusername = model.getBookNane();
                book_id = model.getRecever_id();
                rid = model.getSellerid();
                book_price = model.getPrice();
                buyer_id = model.getBuyerid();
                // Log.e("sendername", getDataAdapter1.getReciever_name());
                Intent i = new Intent(context, Personalchatofferaccepted.class);
                i.putExtra("receiver_id", model.getBuyerid());
                i.putExtra("sendername", model.getBookNane());
                i.putExtra("senderimage", model.getUserimage());
                i.putExtra("counter", "counter");

                i.putExtra("user_chk", rid);
                i.putExtra("offer_rejected", "Offer Rejected:");
                i.putExtra("buyer_id", buyer_id);
                i.putExtra("book_id", book_id);
                i.putExtra("name", book_name);
                i.putExtra("image", book_image);
                i.putExtra("price", book_price);
                context.startActivity(i);
                context.finish();
                new Pushnotification().execute();

               /* status="Reject";
                charnm = capitalize(sp.getString(Constant.username,""));
                rid=model.getSellerid();
                new Pushnotification().execute();
                Intent i = new Intent(context, Countersellit_activity.class);
                book_name=model.getBookname();
                book_image=model.getBookImage();
                buyusername=model.getBookNane();
                book_id=model.getRecever_id();
                rid=model.getSellerid();
                book_price=model.getPrice();
                buyer_id=model.getBuyerid();
                i.putExtra("user_chk",rid);
                i.putExtra("offer_rejected","Offer Rejected:");
                i.putExtra("buyer_id",buyer_id);
                i.putExtra("book_id",book_id);
                i.putExtra("name",book_name);
                i.putExtra("image",book_image);
                i.putExtra("price",book_price);
                //i.putExtra("rid",rid);
                Log.e("user_chk",rid);
                Log.e("book_id",book_id);
                Log.e("buyer_id",buyer_id);*/

                sp.edit().putString("bookName", book_name).commit();
                sp.edit().putString("book_id", book_id).commit();
                sp.edit().putString("bookImage", book_image).commit();

                context.startActivity(i);


            }
        });


        return view;
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "offer_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("buy_id", buyer_id);
                jo.put("sell_id", rid);
                jo.put("status", status);
                jo.put("book_id", book_id);
                jo.put("sell_name", charnm);
                jo.put("buy_name", buyusername);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }


}
