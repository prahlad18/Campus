package app.livecampus.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Activity.Buy_sellit_activity;
import app.livecampus.Fragments.AddPriceOfBookFragment;
import app.livecampus.Fragments.SellAndReject;
import app.livecampus.Pojo.SellBookModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;

/**
 * Created by isquare on 20-Jan-18.
 */

public class SellBookListAdapter extends BaseAdapter {

    public TextView notification;
    public String id;
    public String title;
    public String i;
    JSONObject success;
    ProgressDialog pDialog;
    SharedPreferences sp;
    String book_id;
    JSONArray jsonArrayCategory;
    Activity activity;


    ArrayList<SellBookModel> list = new ArrayList<>();
    private Context context;

    public SellBookListAdapter(Context context, ArrayList<SellBookModel> list, Activity activity) {
        this.context = context;
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.display_textbook_list_adapter, viewGroup, false);
        }
        TextView txtBookTitle = (TextView) view.findViewById(R.id.txt_book_title);
        //TextView txtEdit = (TextView) view.findViewById(R.id.txt_edit_book);
        TextView date = (TextView) view.findViewById(R.id.date);
        final ImageView imgBook = (ImageView) view.findViewById(R.id.img_book);
        final TextView txtPrice = (TextView) view.findViewById(R.id.txt_sellbook_price);
        TextView txtDelete = (TextView) view.findViewById(R.id.txt_delete);
        notification = (TextView) view.findViewById(R.id.booknotif);
        RatingBar txtNotification = (RatingBar) view.findViewById(R.id.txtNotification);


        sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        Log.e("date", "" + sp.getString(Constant.DATE, ""));
        final SellBookModel model = (SellBookModel) getItem(i);
        title = model.getTitle();
        txtBookTitle.setText(title);
        String image = model.getImage();
        if (title.length() > 10) {

            title = title.substring(0, 9) + "...";

            txtBookTitle.setText(title);
        } else {

            txtBookTitle.setText(title); //Dont do any change

        }

        book_id = model.getCourse_id();


        if (model.getOffercount().equals("0")) {

            notification.setVisibility(View.INVISIBLE);
        } else {
            notification.setVisibility(View.VISIBLE);
            notification.setText(model.getOffercount());

        }


        // new Clearenotification().execute();

        //new Clearenotification().execute();

        imgBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.getPrice().equalsIgnoreCase("offer")) {
                    //Toast.makeText(context,"first sreen",Toast.LENGTH_SHORT).show();
                    Intent buy = new Intent(context, Buy_sellit_activity.class);
                    book_id = model.getCourse_id();
                    buy.putExtra("book_id", book_id);
                    Log.e("book_id", book_id);
                    new Clearenotification().execute();
                    context.startActivity(buy);


                } else if (model.getPrice().equalsIgnoreCase("counter")) {
                    book_id = model.getCourse_id();

                    new Clearenotification().execute();
                    final SellBookModel category = list.get(i);
                    String course_id = category.getCourse_id();
                    String strImage = category.getImage();
                    String strTitle = category.getTitle();
                    String strIsbn = category.getIsbn();
                    FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    SellAndReject addPriceOfBookFragment = new SellAndReject();
                    Bundle bundle = new Bundle();
                    bundle.putString("course_id", course_id);
                    bundle.putString("image", strImage);
                    bundle.putString("title", strTitle);
                    bundle.putString("ISBN", strIsbn);
                    bundle.putString("price", category.getFinalprice());
                    bundle.putString("rating", category.getRating());
                    sp.edit().putString(Constant.COURSE_ID, course_id).commit();
                    sp.edit().putString(Constant.IMAGE, strImage).commit();
                    sp.edit().putString(Constant.TITLE, strTitle).commit();
                    sp.edit().putString(Constant.ISBN, strIsbn).commit();
                    sp.edit().putString(Constant.PRI, category.getFinalprice()).commit();
                    sp.edit().putFloat(Constant.RAT, Float.parseFloat(category.getRating())).commit();
                    addPriceOfBookFragment.setArguments(bundle);
                    ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                    ft.commit();


                } else if (model.getPrice().equalsIgnoreCase("sold")) {

                    String buyername = model.getBuyername();
                    String chars = capitalize(buyername);


                    Toast.makeText(context, "This book is bought by" + " " + chars, Toast.LENGTH_LONG).show();


                } else {

                    if (model.getPrice().equals("0")) {
                        final SellBookModel category = list.get(i);
                        String course_id = category.getCourse_id();
                        String strImage = category.getImage();
                        String strTitle = category.getTitle();
                        String strIsbn = category.getIsbn();

                       /* FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        AddPriceOfBookFragment addPriceOfBookFragment = new AddPriceOfBookFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("course_id", course_id);

                        bundle.putString("image", strImage);

                        bundle.putString("title", strTitle);
                        bundle.putBoolean("BOOLEAN_VALUE", false);
                        bundle.putString("ISBN", strIsbn);
                        bundle.putString("price", category.getPrice());
                        bundle.putString("rating", category.getRating());
                        addPriceOfBookFragment.setArguments(bundle);
                        ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                        ft.commit();*/

                        Intent intent = new Intent(context, AddPriceOfBookFragment.class);
                        intent.putExtra("course_id", course_id);
                        intent.putExtra("image", strImage);
                        intent.putExtra("title", strTitle);
                        intent.putExtra("BOOLEAN_VALUE", false);
                        intent.putExtra("ISBN", strIsbn);
                        intent.putExtra("price", category.getPrice());
                        intent.putExtra("rating", category.getRating());
                        context.startActivity(intent);
                        Log.e("get image is ", strImage);
                    } else {
                        final SellBookModel category = list.get(i);
                        String course_id = category.getCourse_id();
                        String strImage = category.getImage();
                        String strTitle = category.getTitle();
                        String strIsbn = category.getIsbn();
                       /* FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        AddPriceOfBookFragment addPriceOfBookFragment = new AddPriceOfBookFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("course_id", course_id);

                        bundle.putString("image", strImage);

                        bundle.putString("title", strTitle);
                        bundle.putString("ISBN", strIsbn);
                        bundle.putBoolean("BOOLEAN_VALUE", false);
                        bundle.putString("price", category.getPrice());
                        bundle.putString("rating", category.getRating());
                        addPriceOfBookFragment.setArguments(bundle);
                        ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                        ft.commit();*/
                        Intent intent = new Intent(context, AddPriceOfBookFragment.class);
                        intent.putExtra("course_id", course_id);
                        intent.putExtra("image", strImage);
                        intent.putExtra("title", strTitle);
                        intent.putExtra("BOOLEAN_VALUE", false);
                        intent.putExtra("ISBN", strIsbn);
                        intent.putExtra("price", category.getPrice());
                        intent.putExtra("rating", category.getRating());
                        context.startActivity(intent);
                        Log.e("get image is ", strImage);


                    }


                }
            }
        });

        Log.e("sell bool", "image is   " + image);
        final String isbn = model.getIsbn();
        if (image.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.main_ic)
                    .into(imgBook);
        } else {
            Picasso.with(context)
                    .load(image)
                    .resize(150, 230)
                    .placeholder(R.drawable.main_ic)
                    .into(imgBook);
        }


        if (model.getPrice().equalsIgnoreCase("offer"))//Sold.
        {
            txtPrice.setText("Offer");
            txtNotification.setVisibility(View.INVISIBLE);
            date.setVisibility(View.INVISIBLE);
            txtDelete.setVisibility(View.INVISIBLE);
            txtPrice.setTextColor(context.getResources().getColor(R.color.white));
            txtPrice.setBackgroundResource(R.drawable.offer_back);


        } else if (model.getPrice().equalsIgnoreCase("counter")) {

            txtPrice.setText("Counter");
            txtNotification.setVisibility(View.INVISIBLE);
            date.setVisibility(View.INVISIBLE);
            txtDelete.setVisibility(View.INVISIBLE);
            txtPrice.setTextColor(context.getResources().getColor(R.color.white));
            txtPrice.setBackgroundResource(R.drawable.couterback);


        } else if (model.getPrice().equalsIgnoreCase("sold")) {

            txtPrice.setText("Sold");
            txtNotification.setVisibility(View.INVISIBLE);
            date.setVisibility(View.INVISIBLE);
            txtDelete.setVisibility(View.INVISIBLE);
            txtPrice.setTextColor(context.getResources().getColor(R.color.white));
            txtPrice.setBackgroundResource(R.drawable.sell_buuton_background);


        } else {

            if (model.getPrice().equals("0")) {
                txtPrice.setText("Sell");
                txtPrice.setBackgroundResource(R.drawable.sell_buuton_background);
                date.setVisibility(View.INVISIBLE);
                txtDelete.setVisibility(View.VISIBLE);


            } else {

                txtPrice.setText("$" + model.getPrice() + ".00");
                txtNotification.setVisibility(View.VISIBLE);
                date.setVisibility(View.VISIBLE);
                txtNotification.setRating(Float.parseFloat("" + model.getRating()));
                txtPrice.setTextColor(context.getResources().getColor(R.color.black));
                txtPrice.setBackgroundColor(context.getResources().getColor(R.color.white));


            }


        }


        date.setText(model.getDate());


        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final SellBookModel category = list.get(i);
                Log.e("cource id is ", category.getCourse_id());
                final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("Delete");
                dialog.setMessage("Are you sure you want to delete the book?");
                dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.setCancelable(true);
                    }
                });
                dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        id = category.getCourse_id();
                        new Get_Gallery().execute();
                        list.remove(category);
                        notifyDataSetChanged();
                    }
                });
                dialog.show();

            }
        });

        //date.setText("" + model.getDate());


        txtPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (model.getPrice().equalsIgnoreCase("offer")) {
                    //Toast.makeText(context,"first sreen",Toast.LENGTH_SHORT).show();
                    Intent buy = new Intent(context, Buy_sellit_activity.class);
                    book_id = model.getCourse_id();
                    buy.putExtra("book_id", book_id);
                    Log.e("book_id", book_id);
                    new Clearenotification().execute();
                    context.startActivity(buy);


                } else if (model.getPrice().equalsIgnoreCase("counter")) {
                    book_id = model.getCourse_id();

                    new Clearenotification().execute();
                    final SellBookModel category = list.get(i);
                    String course_id = category.getCourse_id();
                    String strImage = category.getImage();
                    String strTitle = category.getTitle();
                    String strIsbn = category.getIsbn();
                    FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    SellAndReject addPriceOfBookFragment = new SellAndReject();
                    Bundle bundle = new Bundle();
                    bundle.putString("course_id", course_id);
                    bundle.putString("image", strImage);
                    bundle.putString("title", strTitle);
                    bundle.putString("ISBN", strIsbn);
                    bundle.putString("price", category.getFinalprice());
                    bundle.putString("rating", category.getRating());
                    addPriceOfBookFragment.setArguments(bundle);
                    ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                    ft.commit();


                } else if (model.getPrice().equalsIgnoreCase("sold")) {

                    String buyername = model.getBuyername();
                    String chars = capitalize(buyername);


                    Toast.makeText(context, "This book is bought by" + " " + chars, Toast.LENGTH_LONG).show();


                } else {

                    if (model.getPrice().equals("0")) {
                        final SellBookModel category = list.get(i);
                        String course_id = category.getCourse_id();
                        String strImage = category.getImage();
                        String strTitle = category.getTitle();
                        String strIsbn = category.getIsbn();
                       /* FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        AddPriceOfBookFragment addPriceOfBookFragment = new AddPriceOfBookFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("course_id", course_id);
                        bundle.putString("image", strImage);
                        bundle.putString("title", strTitle);
                        bundle.putString("ISBN", strIsbn);
                        bundle.putBoolean("BOOLEAN_VALUE", false);
                        bundle.putString("price", category.getPrice());
                        bundle.putString("rating", category.getRating());
                        addPriceOfBookFragment.setArguments(bundle);
                        ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                        ft.commit();*/
                        Intent intent = new Intent(context, AddPriceOfBookFragment.class);
                        intent.putExtra("course_id", course_id);
                        intent.putExtra("image", strImage);
                        intent.putExtra("title", strTitle);
                        intent.putExtra("BOOLEAN_VALUE", false);
                        intent.putExtra("ISBN", strIsbn);
                        intent.putExtra("price", category.getPrice());
                        intent.putExtra("rating", category.getRating());
                        context.startActivity(intent);
                        Log.e("get image is ", strImage);
                    } else {
                        final SellBookModel category = list.get(i);
                        String course_id = category.getCourse_id();
                        String strImage = category.getImage();
                        String strTitle = category.getTitle();
                        String strIsbn = category.getIsbn();
                       /* FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        AddPriceOfBookFragment addPriceOfBookFragment = new AddPriceOfBookFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("course_id", course_id);


                        bundle.putString("image", strImage);

                        bundle.putString("title", strTitle);
                        bundle.putString("ISBN", strIsbn);
                        bundle.putBoolean("BOOLEAN_VALUE", false);
                        bundle.putString("price", category.getPrice());
                        bundle.putString("rating", category.getRating());
                        addPriceOfBookFragment.setArguments(bundle);
                        ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                        ft.commit();*/
                        Intent intent = new Intent(context, AddPriceOfBookFragment.class);
                        intent.putExtra("course_id", course_id);
                        intent.putExtra("image", strImage);
                        intent.putExtra("title", strTitle);
                        intent.putExtra("BOOLEAN_VALUE", false);
                        intent.putExtra("ISBN", strIsbn);
                        intent.putExtra("price", category.getPrice());
                        intent.putExtra("rating", category.getRating());
                        context.startActivity(intent);
                        Log.e("get image is ", strImage);


                    }


                }

             /*   final SellBookModel category = list.get(i);
                String course_id = category.getCourse_id();
                String strImage = category.getImage();
                String strTitle = getTitleOfBook(category.getTitle());
                String strIsbn = category.getIsbn();
                FragmentManager fm = ((AppCompatActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                AddPriceOfBookFragment addPriceOfBookFragment=new AddPriceOfBookFragment();
                Bundle bundle=new Bundle();
                bundle.putString("course_id",course_id);
                bundle.putString("image",strImage);
                bundle.putString("title",strTitle);
                bundle.putString("ISBN",strIsbn);
                bundle.putString("price",category.getPrice());
                bundle.putString("rating",category.getRating());
                addPriceOfBookFragment.setArguments(bundle);
                ft.replace(R.id.container,addPriceOfBookFragment,"Sell");
                ft.addToBackStack(null);
                ft.commit();
                Log.e("get image is ",strImage);*/
            }
        });

        return view;
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    public class Get_Gallery extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
//            String serverUrl = Constant.MAIN_URL + "delete_course.php";
            String serverUrl = Constant.MAIN_URL + "delete_course.php";

            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                //jo.put("cid", ""+id );
                jo.put("course_id", "" + id);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);

                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();


            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");

                    String course_id = j.getString("course_id");

                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(context, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }
   /* public String getTitleOfBook(String title)
    {
            String titleBook = "";
            String book[] = title.split(" ");
            for (int i = 0; i < 2; i++) {

               titleBook += book[i]+" ";
            }
            return titleBook;

    }*/

    private class Clearenotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "clear_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);


            try {
                JSONObject jo = new JSONObject();


                jo.put("sell_id", sp.getString(Constant.rid, ""));
                jo.put("book_id", book_id);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);
/*
                try {


                    if (success.getString("success").equals("1")) {
                        jsonArrayCategory = success.getJSONArray("posts");
                        for (int i = 0; i < jsonArrayCategory.length(); i++) {


                            success = jsonArrayCategory.getJSONObject(i);
                            String msg = success.getString("BookCount");
                            //Toast.makeText(getpp, msg, Toast.LENGTH_LONG).show();
                            notification.setText(msg);
                           *//* if(msg.toString().equals("null")){

                                notification.setText("");


                            }else {


                            }
*//*




                        }




                    } else if (success.getString("success").equals("0")) {
                        // Toast.makeText(Msg_actvity.this, "Racord not found", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }*/
            }


        }
    }

}

