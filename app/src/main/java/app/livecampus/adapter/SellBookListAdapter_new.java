package app.livecampus.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Activity.Personalchat_activity;
import app.livecampus.Fragments.AddPriceOfBookFragment;
import app.livecampus.Pojo.SellBookModel_new;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by isquare on 20-Jan-18.
 */

public class SellBookListAdapter_new extends BaseAdapter {

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String id;
    public TextView buy, cunter, txtUserby, avlbdate;
    public String thisDate;
    public int currentImage = 0;
    public EditText editTextpric;
    public String ruserImage, sendername, majorname, yeares;
    public boolean chkBuy;
    public String i;
    JSONObject success;
    String[] images;
    ProgressDialog pDialog;
    SharedPreferences sp;
    boolean b;
    JSONObject catObj;
    SellBookModel_new category;
    JSONArray jsonArrayCategory;
    ImageView viewPager, viewPager1, viewPager2;
    Activity activity;
    boolean your_date_is_outdated;
    String images1, images2, images3;
    String course_id, price, rating, receiver_id;
    String image, image1, name;
    int book_rice;
    String imagess1, imagess2, imagess3;
    String issuccess;
    Date date;
    Date ajanidate;
    String strcunt = "yes";
    Date date1;
    RatingBar ratingBar;
    long currentTime;
    boolean isValid;
    long parsedMillis, now;
    ArrayList<Integer> items = new ArrayList();
    //int[] images;
    ArrayList<String> n = new ArrayList<String>();
    Point p;
    ArrayList<String> imagesEncodedList;
    String book_id, book_name, book_image, rid, book_idc, strtextprice, loging_id, book_idconter, userFrom, username, status, charnm;
    //public String Strprice ;
    int Strprice;
    ArrayList<SellBookModel_new> list = new ArrayList<>();
    ArrayList<SellBookModel_new> list2 = new ArrayList<>();
    private Activity context;
    private ImageView hImageViewPic;
    private Button iButton, gButton;
    private int PICK_IMAGE = 1;
    private int[] allImageIds;
    //private int[] n;


    public SellBookListAdapter_new(Activity context, ArrayList<SellBookModel_new> list, Activity activity) {
        this.context = context;
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.listsearchitmes, viewGroup, false);
        }
        TextView txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        TextView txtUserUniversity = (TextView) view.findViewById(R.id.txtUserUniversity);
        TextView txtUserMajor = (TextView) view.findViewById(R.id.txtUserMajor);
        ImageView imageBook = (ImageView) view.findViewById(R.id.imageBook);
        CircleImageView imageUser = (CircleImageView) view.findViewById(R.id.imageUser);
        TextView txtUserYear = (TextView) view.findViewById(R.id.txtUserYear);
        TextView txtUserprice = (TextView) view.findViewById(R.id.sellbook_price);
        avlbdate = (TextView) view.findViewById(R.id.avlbdate);
        RatingBar txtUserrating = (RatingBar) view.findViewById(R.id.txtNotification);
        txtUserby = (TextView) view.findViewById(R.id.buy);

        TextView txtUsermsg = (TextView) view.findViewById(R.id.msg);

        sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        final SellBookModel_new model = (SellBookModel_new) getItem(i);
        isValid = true;


        String sDate1 = model.getDate();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMMM dd yyyy");
        Date todayDate = new Date();
        thisDate = currentDate.format(todayDate);
        try {
            ajanidate = currentDate.parse(thisDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        try {
            date = currentDate.parse(sDate1);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (model.getDate().equals("null")) {


        } else {
            Log.e("currentdate", thisDate);
            Log.e("backenddate", "" + currentDate.format(date));

        }


        rid = model.getRid();
//        images = new int[]{Integer.parseInt(imagess1), Integer.parseInt(imagess2), Integer.parseInt(imagess3)};


        items.add(R.drawable.add);
        items.add(R.drawable.add);

        n.add(model.getImages1());
        n.add(model.getImages2());
        n.add(model.getImages3());

        imagess1 = model.getImages1();
        imagess2 = model.getImages2();
        imagess3 = model.getImages3();

        images1 = model.getImages1();
        images2 = model.getImages2();
        images3 = model.getImages3();


        image = model.getBookImage();


        name = model.getBookNane();

        String chars = capitalize(name);

        if (chars.equals("null")) {

            txtUserName.setText("");

        } else {

            txtUserName.setText(chars);

        }

        txtUserprice.setText("$" + model.getPrice() + ".00");
        txtUserrating.setRating(Float.parseFloat("" + model.getRating()));


        // txtUserrating.setRating(Float.parseFloat(""+model.getRating()));
        if (model.getUnivervity().equals("null")) {

            txtUserUniversity.setText("");

        } else {
            txtUserUniversity.setText(model.getUnivervity());
        }
        if (model.getMajor().equals("null")) {

            txtUserMajor.setText("");

        } else {
            txtUserMajor.setText(model.getMajor());


        }

        if (model.getYear().equals("null")) {

            txtUserYear.setText("");

        } else {
            txtUserYear.setText(model.getYear());

        }

        String strdate = model.getDate();


        if (model.getDate().equals(thisDate)) {
            avlbdate.setVisibility(View.VISIBLE);
            txtUserby.setBackgroundResource(R.drawable.sell_buuton_background);
            txtUserby.setEnabled(true);
            avlbdate.setText("Available on " + strdate);

        }
        if (model.getDate().equals("null")) {


        } else {


            if (date.after(ajanidate)) {
                avlbdate.setVisibility(View.VISIBLE);
                txtUserby.setBackgroundResource(R.drawable.buy_green);
                txtUserby.setEnabled(false);
                avlbdate.setText("Available on " + strdate);
            }

            if (date.before(ajanidate)) {
                avlbdate.setVisibility(View.INVISIBLE);
                txtUserby.setBackgroundResource(R.drawable.buy_green);
                txtUserby.setEnabled(false);
            }

            if (date.equals(ajanidate)) {
                avlbdate.setVisibility(View.VISIBLE);
                txtUserby.setBackgroundResource(R.drawable.sell_buuton_background);
                txtUserby.setEnabled(true);
                avlbdate.setText("Available on " + strdate);
            }


        }


        if (rid.equals(sp.getString(Constant.rid, ""))) {

            txtUserby.setText("Edit");
            txtUserby.setBackgroundResource(R.drawable.sell_buuton_background);
            //txtUserby.setVisibility(View.INVISIBLE);
            txtUsermsg.setVisibility(View.INVISIBLE);


        } else {
            txtUserby.setText("BUY");
            txtUserby.setVisibility(View.VISIBLE);
            txtUsermsg.setVisibility(View.VISIBLE);


        }

        //model.getStatus().equals("1")
        if (model.getStatus().equals("1")) {
            txtUserby.setBackgroundResource(R.drawable.gray_buy_buttonbck);
            txtUserby.setEnabled(false);
            txtUserby.setText("BUY");


        }/*else {
            txtUserby.setEnabled(true);
            txtUserby.setBackgroundResource(R.drawable.sell_buuton_background);

        }*/


        Log.e("search book", "image is " + image);

        if (image.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.main_ic)
                    .placeholder(R.drawable.main_ic)
                    .into(imageBook);
        } else if (image.contains("https://")) {
            Picasso.with(context)
                    .load(image)
                    .placeholder(R.drawable.main_ic)
                    .into(imageBook);
        } else {
            Picasso.with(context)
                    .load(Constant.MAIN_URL + image)
                    .placeholder(R.drawable.main_ic)
                    .into(imageBook);
        }


        String imageuser = model.getUserimage();
        if (imageuser.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.user)
                    .placeholder(R.drawable.main_ic)
                    .into(imageUser);
        } else {
            Picasso.with(context)
                    .load(Constant.MAIN_URL + imageuser)
                    .resize(125, 125)
                    .placeholder(R.drawable.user)
                    .into(imageUser);
        }

        imageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rid = model.getRid();

                if (rid.equals(sp.getString(Constant.rid, ""))) {

                    final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

                    View layout = inflater.inflate(R.layout.popup,
                            (ViewGroup) context.findViewById(R.id.viewgroup));
                    ImageView image = (ImageView) layout.findViewById(R.id.imageFull);
                    TextView name = (TextView) layout.findViewById(R.id.name);
                    TextView mjor = (TextView) layout.findViewById(R.id.mjorname);
                    TextView yeare = (TextView) layout.findViewById(R.id.yeare);
                    ImageView btnChat = (ImageView) layout.findViewById(R.id.btnLink);
                    btnChat.setVisibility(View.INVISIBLE);
                    imageDialog.setView(layout);
                    btnChat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

//                            imageDialog.show().dismiss();


                            //Log.e("click", "click");
                            Log.e("receiver_image", model.getUserimage());
                            // Log.e("sendername", getDataAdapter1.getReciever_name());
                            Intent i = new Intent(context, Personalchat_activity.class);
                            i.putExtra("receiver_id", model.getRid());
                            i.putExtra("sendername", model.getBookNane());
                            i.putExtra("senderimage", model.getUserimage());
                            i.putExtra("searchuser", "searchuser");
                            context.startActivity(i);
                            context.finish();


                        }
                    });


                    ruserImage = model.getUserimage();
                    sendername = model.getBookNane();
                    majorname = model.getMajor();
                    username = sp.getString(Constant.username, "");
                    yeares = model.getYear();

                    String charsn = capitalize(sendername);

                    name.setText("" + charsn);
                    yeare.setText("" + yeares);
                    mjor.setText("" + majorname);

                    if (ruserImage.equals("")) {
                        Picasso.with(context)
                                .load(R.drawable.logo)
                                .placeholder(R.drawable.main_ic)
                                .into(image);
                    } else {
                        Picasso.with(context)
                                .load(Constant.MAIN_URL + ruserImage)
                                .placeholder(R.drawable.main_ic)
                                .into(image);
                    }

                    imageDialog.create();
                    imageDialog.show();


                } else {


                    final AlertDialog.Builder imageDialog = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);

                    View layout = inflater.inflate(R.layout.popup,
                            (ViewGroup) context.findViewById(R.id.viewgroup));
                    ImageView image = (ImageView) layout.findViewById(R.id.imageFull);
                    TextView name = (TextView) layout.findViewById(R.id.name);
                    TextView mjor = (TextView) layout.findViewById(R.id.mjorname);
                    TextView yeare = (TextView) layout.findViewById(R.id.yeare);
                    ImageView btnChat = (ImageView) layout.findViewById(R.id.btnLink);
                    btnChat.setVisibility(View.VISIBLE);
                    imageDialog.setView(layout);
                    btnChat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            imageDialog.show().dismiss();

                            //Log.e("click", "click");
                            Log.e("receiver_image", model.getUserimage());
                            // Log.e("sendername", getDataAdapter1.getReciever_name());
                            Intent i = new Intent(context, Personalchat_activity.class);
                            i.putExtra("receiver_id", model.getRid());
                            i.putExtra("sendername", model.getBookNane());
                            i.putExtra("senderimage", model.getUserimage());
                            i.putExtra("searchuser", "searchuser");
                            context.startActivity(i);
                            context.finish();


                        }
                    });


                    ruserImage = model.getUserimage();
                    sendername = model.getBookNane();
                    majorname = model.getMajor();
                    username = sp.getString(Constant.username, "");
                    yeares = model.getYear();

                    String charsn = capitalize(sendername);

                    name.setText("" + charsn);
                    yeare.setText("" + yeares);
                    mjor.setText("" + majorname);

                    if (ruserImage.equals("")) {
                        Picasso.with(context)
                                .load(R.drawable.logo)
                                .placeholder(R.drawable.main_ic)
                                .into(image);
                    } else {
                        Picasso.with(context)
                                .load(Constant.MAIN_URL + ruserImage)
                                .placeholder(R.drawable.main_ic)
                                .into(image);
                    }

                    imageDialog.create();
                    imageDialog.show();


                }


            }
        });


        txtUserby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (txtUserby.getText().toString().equalsIgnoreCase("Edit")) {

                    final SellBookModel_new category = list.get(i);
                    String course_id = category.getRecever_id();
                    String strImage = category.getBookImage();
                    String strTitle = category.getBookNane();
                    String strIsbn = category.getIsbn();/*
                    FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    AddPriceOfBookFragment addPriceOfBookFragment = new AddPriceOfBookFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("course_id", course_id);
                    if (strImage.contains("https://")) {
                        bundle.putString("image", strImage);

                    } else {
                        bundle.putString("image", Constant.MAIN_URL + strImage);

                    }
                    bundle.putString("title", strTitle);
                    bundle.putString("ISBN", strIsbn);
                    bundle.putBoolean("BOOLEAN_VALUE", true);
                    bundle.putString("price", category.getPrice());
                    bundle.putString("rating", category.getRating());
                    addPriceOfBookFragment.setArguments(bundle);
                    ft.replace(R.id.container, addPriceOfBookFragment, "Sell");
                    ft.commit();*/

                    Intent intent = new Intent(context, AddPriceOfBookFragment.class);
                    intent.putExtra("course_id", course_id);
                    if (strImage.contains("https://")) {
                        intent.putExtra("image", strImage);

                    } else {
                        intent.putExtra("image", Constant.MAIN_URL + strImage);

                    }
                    intent.putExtra("title", strTitle);
                    intent.putExtra("BOOLEAN_VALUE", false);
                    intent.putExtra("ISBN", strIsbn);
                    intent.putExtra("price", category.getPrice());
                    intent.putExtra("rating", category.getRating());
                    context.startActivity(intent);


                    //Log.e("get image is ", strImage);


                } else {
                    final SellBookModel_new category = list.get(i);

                    book_id = category.getRecever_id();
//                Log.e("book_idcounter",book_id);
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.buy_message);
                    context.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    //PhotoView image = (PhotoView) dialog.findViewById(R.id.image);

                    ImageView bckimg = (ImageView) dialog.findViewById(R.id.imagebook);
                    TextView blure = (TextView) dialog.findViewById(R.id.blure);
                    RatingBar rating = (RatingBar) dialog.findViewById(R.id.ratingBar1);
                    ImageView imgCamera = (ImageView) dialog.findViewById(R.id.image_camera);
                    ImageView imbook2 = (ImageView) dialog.findViewById(R.id.image_camera2);
                    ImageView imbook3 = (ImageView) dialog.findViewById(R.id.image_camera3);
                    ImageView cnl = (ImageView) dialog.findViewById(R.id.cnl);
                    ImageView image_blurb = (ImageView) dialog.findViewById(R.id.image_blurb);
                    final LinearLayout lineare2 = (LinearLayout) dialog.findViewById(R.id.bulrclk);
                    final LinearLayout lineare1 = (LinearLayout) dialog.findViewById(R.id.lnr1);


                    editTextpric = (EditText) dialog.findViewById(R.id.edit);


                    TextView bookname = (TextView) dialog.findViewById(R.id.bookname);
                    buy = (TextView) dialog.findViewById(R.id.buy);


                    cunter = (TextView) dialog.findViewById(R.id.counter);
                    TextView bookisbn = (TextView) dialog.findViewById(R.id.isbn);
                    final TextView price = (TextView) dialog.findViewById(R.id.price);
                    TextView cancle = (TextView) dialog.findViewById(R.id.back);


                    cancle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    String blub = category.getBlub();
                    String name1 = category.getBookNane();
                    String chars1 = capitalize(name1);

                    blure.setText(chars1 + ":" + blub);


                    images1 = category.getImages1();
                    if (images1.equals("")) {
                        Picasso.with(context)
                                .load(R.drawable.logo)
                                .placeholder(R.drawable.main_ic)
                                .into(imgCamera);
                    } else {
                        Picasso.with(context)
                                .load(images1)
                                .placeholder(R.drawable.main_ic)
                                .into(imgCamera);
                    }

                    images2 = category.getImages2();
                    if (images2.equals("")) {
                        Picasso.with(context)
                                .load(R.drawable.logo)
                                .placeholder(R.drawable.main_ic)
                                .into(imbook2);
                    } else {
                        Picasso.with(context)
                                .load(images2)
                                .placeholder(R.drawable.main_ic)
                                .into(imbook2);
                    }

                    images3 = category.getImages3();
                    if (images3.equals("")) {
                        Picasso.with(context)
                                .load(R.drawable.logo)
                                .placeholder(R.drawable.main_ic)
                                .into(imbook3);
                    } else {
                        Picasso.with(context)
                                .load(images3)
                                .placeholder(R.drawable.main_ic)
                                .into(imbook3);
                    }


                    if (chkBuy == true) {
                        // sp.getBoolean(Constant.CHKBUY,true)  &&  sp.getString(Constant.Book_id,book_id)
                        buy.setBackgroundResource(R.drawable.gray_buy_buttonbck);

                    }

                    buy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            loging_id = category.getRid();


                            if (loging_id.equals(sp.getString(Constant.rid, ""))) {
                                Toast.makeText(context, "you can not buy book", Toast.LENGTH_SHORT).show();

                                // editTextpric.setVisibility(View.GONE);
                            } else {

                                book_idc = category.getRecever_id();
                                strtextprice = category.getPrice();
                                userFrom = category.getRid();
                                username = category.getBookNane();

                                Log.e("userFrom", userFrom);
                                Log.e("sp id", sp.getString(Constant.rid, ""));

                                dialog.dismiss();
                                status = "Offer";
                                new Buybook().execute();
                            }
                        }


                    });
                    cunter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                            book_idconter = category.getRecever_id();


                            if (editTextpric.getText().toString().length() == 0) {
                                editTextpric.setError("Enter price");
                                editTextpric.requestFocus();

                            } else {
                                book_rice = Integer.parseInt(category.getPrice());
                                Strprice = Integer.parseInt(editTextpric.getText().toString());
                                loging_id = category.getRid();
//                            Log.e("getrid",loging_id);
//                            Log.e("loging_id",sp.getString(Constant.rid,""));

                                if (loging_id.equals(sp.getString(Constant.rid, ""))) {
                                    Toast.makeText(context, "you can not add bid", Toast.LENGTH_SHORT).show();
                                    editTextpric.setVisibility(View.GONE);
                                } else if (Strprice > book_rice) {
                                    Toast.makeText(context, "valid book price!", Toast.LENGTH_SHORT).show();


                                } else {
                                    dialog.dismiss();
                                    status = "Counter";
                                    userFrom = category.getRid();
                                    book_idc = category.getRecever_id();
                                    username = category.getBookNane();
                                    new PostDataTOServer().execute();


                                }


                            }


                        }
                    });

                    imgCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            images1 = category.getImages1();
                            currentImage = 0;
                            alertytwo();
                            Log.e("current pos", "is" + currentImage);

                        }
                    });
                    image_blurb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lineare1.setVisibility(View.GONE);
                            lineare2.setVisibility(View.VISIBLE);
                        }
                    });
                    cnl.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lineare1.setVisibility(View.VISIBLE);
                            lineare2.setVisibility(View.GONE);
                        }
                    });

                    imbook2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            images2 = category.getImages2();
                            currentImage = 1;
                            alertytwo();
                            Log.e("current pos", "is" + currentImage);


                        }
                    });

                    imbook3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            images3 = category.getImages3();
                            currentImage = 2;
                            alertytwo();
                            Log.e("current pos", "is" + currentImage);


                        }
                    });


                    if (category.getBookname().equals("null")) {

                        bookname.setText("");

                    } else {

                        bookname.setText(category.getBookname());

                    }
                    image1 = category.getBookImage();
                    Log.e("image ", "ok " + image1);
                    if (image1.equals("")) {
                        Picasso.with(context)
                                .load(R.drawable.main_ic)
                                .placeholder(R.drawable.main_ic)
                                .into(bckimg);
                    } else if (image1.contains("https://")) {
                        Picasso.with(context)
                                .load(image1)
                                .placeholder(R.drawable.main_ic)
                                .into(bckimg);
                    } else {
                        Picasso.with(context)
                                .load(Constant.MAIN_URL + image1)
                                .placeholder(R.drawable.main_ic)
                                .into(bckimg);
                    }


                    price.setText("$" + category.getPrice() + ".00");
                    bookisbn.setText("isbn #" + category.getIsbn());
                    rating.setRating(Float.parseFloat("" + category.getRating()));

                    dialog.show();


                }


            }

        });

        txtUsermsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Log.e("click", "click");
                Log.e("receiver_image", model.getUserimage());
                // Log.e("sendername", getDataAdapter1.getReciever_name());
                Intent i = new Intent(context, Personalchat_activity.class);
                i.putExtra("receiver_id", model.getRid());
                i.putExtra("sendername", model.getBookNane());
                i.putExtra("senderimage", model.getUserimage());
                i.putExtra("searchuser", "searchuser");
                context.startActivity(i);
                context.finish();

              /*  Intent i = new Intent(context, Msg_activitynew.class);//Msg_activitynew
                book_name=model.getBookname();
                book_image=model.getBookImage();
                book_id=model.getRecever_id();
                rid=model.getRid();
                i.putExtra("user_chk",rid);
                i.putExtra("book_id",book_id);
                i.putExtra("name",book_name);
                i.putExtra("image",book_image);
                //i.putExtra("rid",rid);
                Log.e("user_chk",rid);
                Log.e("book_id",book_id);

                sp.edit().putString("bookName",book_name).commit();
                sp.edit().putString("book_id",book_id).commit();
                sp.edit().putString("bookImage",book_image).commit();

                context.startActivity(i);
*/


            }
        });


        return view;
    }

    private void alertytwo() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_bookimage);
        context.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        ImageView cancle = (ImageView) dialog.findViewById(R.id.cancle);
        ImageView next = (ImageView) dialog.findViewById(R.id.next);
        ImageView back = (ImageView) dialog.findViewById(R.id.back);
        viewPager = (ImageView) dialog.findViewById(R.id.viewPager);


        Log.e("current pos two", "is" + currentImage);

        images = new String[]{images1, images2, images3};
        currentImage = currentImage % images.length;


        Picasso.with(context)
                .load(images[currentImage])
                .placeholder(R.drawable.main_ic)
                .into(viewPager);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentImage++;
                currentImage = currentImage % images.length;

//                viewPager.setImageResource((images[currentImage]));
                Picasso.with(context)
                        .load(images[currentImage])
                        .placeholder(R.drawable.main_ic)
                        .into(viewPager);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentImage--;
                currentImage = (currentImage + images.length) % images.length;

//                viewPager.setImageResource((images[currentImage]));

                Picasso.with(context)
                        .load(images[currentImage])
                        .placeholder(R.drawable.main_ic)
                        .into(viewPager);


            }
        });




    /*    MyCustomPagerAdapter myCustomPagerAdapter = new MyCustomPagerAdapter(context,list2);
        viewPager.setAdapter(myCustomPagerAdapter);*/
        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //currentImage=0;

            }
        });

        dialog.show();

    }


   /* private class PostDataTOServer extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "countersave.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", "" +book_idconter);
                jo.put("price", "" + Strprice);
                jo.put("status", "counter");
                jo.put("rid", "" +sp.getString(Constant.rid,""));
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();
            try {
                if(success.getString("success").equals("1")) {
                    jsonArrayCategory = success.getJSONArray("posts");


                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel_new();
                        catObj = jsonArrayCategory.getJSONObject(i);

                        *//*category.setCourse_id(catObj.getString("course_id"));
                        category.setTitle(catObj.getString(Constant.CNAME1));
                        category.setIsbn(catObj.getString(Constant.cISBN1));
                        category.setImage(catObj.getString(Constant.IMAGE1));
                        category.setRating(catObj.getString("rating"));
                        Log.i("rating",catObj.getString("rating"));*//*

                        //listOfBook.add(category);

                        Intent i1=new Intent(context,HomeActivity.class);
                        context.startActivity(i1);
                        context.finish();
                        Toast.makeText(context,"you have successfully add counter value meet to seller",Toast.LENGTH_SHORT).show();

                    }

                }
                else if(success.getString("success").equals("0"))
                {
                   Toast.makeText(context,"you have already counter value add this book",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }*/


    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private class PostDataTOServer extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
         /*   pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "countersave.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                jo.put("book_id", "" + book_idconter);
                jo.put("price", "" + Strprice);
                jo.put("status", "counter");
                jo.put("rid", "" + sp.getString(Constant.rid, ""));


                // Log.e("book_id get_course api",book_idc);
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if (pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {

                    jsonArrayCategory = success.getJSONArray("posts");


                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel_new();
                        catObj = jsonArrayCategory.getJSONObject(i);


                        String msg = catObj.getString("Message");
                        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

                        if (catObj.getString("Message").equalsIgnoreCase("You have successfully add counter value for this book.")) {

                            charnm = capitalize(sp.getString(Constant.username, ""));

                            new Pushnotification().execute();


                        }
                        System.out.println("msg is" + catObj.getString("Message"));


                    }

                } else if (success.getString("success").equals("0")) {
                    // Toast.makeText(context,"you have already counter value add this book",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    private class Buybook extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
         /*   pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "get_course_file.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("book_id", "" + book_idc);
                jo.put("price", "" + strtextprice);
                jo.put("status", "offer");
                jo.put("rid", "" + sp.getString(Constant.rid, ""));
                // Log.e("book_id get_course api",book_idc);
                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if (pDialog.isShowing())
                pDialog.dismiss();*/
            try {
                if (success.getString("success").equals("1")) {


                    jsonArrayCategory = success.getJSONArray("posts");


                    for (int i = 0; i < jsonArrayCategory.length(); i++) {

                        category = new SellBookModel_new();
                        catObj = jsonArrayCategory.getJSONObject(i);


                        String msg = catObj.getString("Message");
                        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

                        if (catObj.getString("Message").equalsIgnoreCase("You request send successfully for this book.")) {
                            sp.edit().putBoolean(Constant.CHKBUY, true).commit();
                            sp.edit().putString(Constant.Book_id, book_idc).commit();
                            charnm = capitalize(sp.getString(Constant.username, ""));

                            new Pushnotification().execute();

                        }
                        System.out.println("msg is" + catObj.getString("Message"));
                        //Toast.makeText(context,"Your requet was been sent",Toast.LENGTH_SHORT).show();


                    }


                } else if (success.getString("success").equals("0")) {
                    //Toast.makeText(context,"Your requet was been not sent",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "buy_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("buy_id", sp.getString(Constant.rid, ""));
                jo.put("sell_id", userFrom);
                jo.put("status", status);
                jo.put("book_id", book_idc);//new add
                //jo.put("message", messageBody);
                jo.put("sell_name", username);
                jo.put("buy_name", charnm);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                //txt_search_1.setVisibility(View.GONE);

                try {


                    if (success.getString("success").equals("1")) {
                       /* jsonArrayCategory = success.getJSONArray("posts");
                        if (success.getString("success").equals("1")) {
                            JSONObject j = success.getJSONObject("posts");


                            System.out.println("my login JSONObject : " + j);
                            value++;
                            category = new DataModel();


                            category.setSenderID("" + sp.getString(Constant.rid, ""));
                            category.setRecieverID("" + receiver_id);
                            category.setMsg(""+ messageBody);

                            arrayList.add(category);
                            adapter = new RecyclerViewAdapter_chat(Personalchat_activity.this, arrayList);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            listView.scrollToPosition(arrayList.size() - 1);
*/


                    }


                } catch (JSONException e) {
                    System.out.println("the title exeption is :" + e);
                    e.printStackTrace();
                }
            }

        }
    }


}

