package app.livecampus.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.SellBookModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public String id;
    Context context;
    JSONObject success;
    ProgressDialog pDialog;
    SharedPreferences sp;
    String book_id;
    String strcoursenm;
    JSONArray jsonArrayCategory;
    ArrayList<SellBookModel> getDataAdapter;


    public RecyclerViewAdapter(Context context, ArrayList<SellBookModel> arrayList) {

        super();
        this.context = context;
        this.getDataAdapter = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.examecramdiplay, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final SellBookModel getDataAdapter1 = getDataAdapter.get(position);
        String image = getDataAdapter1.getImage();

        strcoursenm = getDataAdapter1.getCoursename();

        //holder.coursenm.setText(""+strcoursenm);


        if (strcoursenm.equals("") || strcoursenm.equals("null")) {

            holder.coursenm.setVisibility(View.INVISIBLE);
        } else {
            holder.coursenm.setVisibility(View.VISIBLE);

            holder.coursenm.setText("" + strcoursenm);

        }

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final SellBookModel getDataAdapter1 = getDataAdapter.get(position);
                String cour_id = getDataAdapter1.getCourse_id();

                Log.e("cource id is ", cour_id);
                final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("Delete");
                dialog.setMessage("Are you sure you want to delete the book?");
                dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.setCancelable(true);
                    }
                });
                dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        id = getDataAdapter1.getCourse_id();
                        new Get_Gallery().execute();
                        getDataAdapter.remove(getDataAdapter1);
                        notifyDataSetChanged();
                    }
                });
                dialog.show();

            }
        });


        if (image.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.main_ic)
                    .into(holder.imgBook);
        } else {
            Picasso.with(context)
                    .load(image)
                    .resize(150, 230)
                    .placeholder(R.drawable.main_ic)
                    .into(holder.imgBook);
        }
    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgBook;
        LinearLayout ln1;
        TextView coursenm, txtDelete;
        View view1, view2;

        public ViewHolder(View itemView) {

            super(itemView);
            imgBook = (ImageView) itemView.findViewById(R.id.img_book);
            coursenm = (TextView) itemView.findViewById(R.id.coursenm);
            txtDelete = (TextView) itemView.findViewById(R.id.txt_delete);

        }


    }

    public class Get_Gallery extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
//            String serverUrl = Constant.MAIN_URL + "delete_course.php";
            String serverUrl = Constant.MAIN_URL + "delete_course.php";

            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();


                //jo.put("cid", ""+id );
                jo.put("course_id", "" + id);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);

                success = new JSONObject(responseText);
                System.out.println("the response is :" + success);

                success = new JSONObject(responseText);

            } catch (Exception e) {
                pDialog.dismiss();
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();


            try {
                if (success.getString("success").equals("1")) {

                    JSONObject j = success.getJSONObject("posts");

                    String course_id = j.getString("course_id");

                } else if (success.getString("success").equals("0")) {
                    Toast.makeText(context, "Incorrect Details", Toast.LENGTH_SHORT).show();
                } else {
                }
            } catch (Exception e) {
                System.out.println("the exx" + e);
            }
        }
    }
}