package app.livecampus.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.livecampus.Activity.Personalchatofferaccepted;
import app.livecampus.Pojo.SellBookModel_new;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 6/9/18.
 */

public class Buy_sellit_adaptor extends BaseAdapter {

    public String id;
    public String i;
    JSONObject success;
    ProgressDialog pDialog;
    SharedPreferences sp;
    Activity activity;
    String course_id, price, rating;
    String image, name, title;
    CircleImageView imageUser;
    ImageView imageBook, cancle;
    RatingBar ratingBar;
    TextView txtUserName, txtUserUniversity, txtUserMajor, txtUserYear, txtUserprice, txtsellit, txtBookTitle, txtisbn, back, txt_home, txt_search;
    Point p;
    ArrayList<String> imagesEncodedList;
    String book_id, book_name, book_image, rid, book_idc, buyer_id, book_price, buyusername, status, charnm;
    String selectedDate;
    ArrayList<SellBookModel_new> list = new ArrayList<>();
    private Activity context;
    private int PICK_IMAGE = 1;

    public Buy_sellit_adaptor(Activity context, ArrayList<SellBookModel_new> list) {
        this.context = context;
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.buy_sellit_items, viewGroup, false);
        }
        txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        txtUserUniversity = (TextView) view.findViewById(R.id.txtUserUniversity);
        txtUserMajor = (TextView) view.findViewById(R.id.txtUserMajor);
        imageBook = (ImageView) view.findViewById(R.id.imageBook);
        imageUser = (CircleImageView) view.findViewById(R.id.imageUser);
        txtUserYear = (TextView) view.findViewById(R.id.txtUserYear);
        txtBookTitle = (TextView) view.findViewById(R.id.txt_book_title);
        txtisbn = (TextView) view.findViewById(R.id.txt_book_isbn);
        txtUserprice = (TextView) view.findViewById(R.id.sellbook_price);
        txtsellit = (TextView) view.findViewById(R.id.sellit);
        cancle = (ImageView) view.findViewById(R.id.cancle);

        sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);
        final SellBookModel_new model = (SellBookModel_new) getItem(i);


//rakesh

        name = model.getBookNane();

        String chars = capitalize(name);
        txtUserName.setText(chars);


        txtUserUniversity.setText(model.getUnivervity());

        txtUserMajor.setText(model.getMajor());
        txtUserYear.setText(model.getYear());
        txtisbn.setText("ISBN #" + model.getIsbn());
        txtUserprice.setText("$" + model.getPrice() + ".00");


        title = model.getBookname();

        txtBookTitle.setText(title);


        image = model.getBookImage();


        if (image.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.main_ic)
                    .placeholder(R.drawable.main_ic)
                    .into(imageBook);
        } else if (image.contains("https://")) {
            Picasso.with(context)
                    .load(image)
                    .placeholder(R.drawable.main_ic)
                    .into(imageBook);
        } else {
            Picasso.with(context)
                    .load(Constant.MAIN_URL + image)
                    .placeholder(R.drawable.main_ic)
                    .into(imageBook);
        }

        Log.e("buy sell", "image is  " + image);


        String imageuser = model.getUserimage();
        if (imageuser.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.user)
                    .placeholder(R.drawable.main_ic)
                    .into(imageUser);
        } else {
            Picasso.with(context)
                    .load(imageuser)
                    .placeholder(R.drawable.user)
                    .into(imageUser);
        }


        txtsellit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                status = "Accept";
//                new Pushnotification().execute();
                charnm = capitalize(sp.getString(Constant.username, ""));
                book_name = model.getBookname();
                book_image = model.getBookImage();
                buyusername = model.getBookNane();
                book_id = model.getRecever_id();
                rid = model.getSellerid();
                book_price = model.getPrice();
                buyer_id = model.getBuyerid();
                Intent i = new Intent(context, Personalchatofferaccepted.class);
                i.putExtra("receiver_id", model.getBuyerid());
                i.putExtra("sendername", model.getBookNane());
                i.putExtra("senderimage", model.getUserimage());
                i.putExtra("counter", "counter");

                i.putExtra("user_chk", rid);
                i.putExtra("buyer_id", buyer_id);
                i.putExtra("book_id", book_id);
                i.putExtra("offer_acceted", "Offer Accepted:");
                i.putExtra("name", book_name);
                i.putExtra("image", book_image);
                i.putExtra("price", book_price);
                //i.putExtra("rid",rid);
                Log.e("user_chk", rid);
                Log.e("book_id", book_id);
                Log.e("buyer_id", buyer_id);
                Log.e("send buy", "image  " + model.getBookNane());
                sp.edit().putString("bookName", book_name).commit();
                sp.edit().putString("book_id", book_id).commit();
                sp.edit().putString("bookImage", book_image).commit();

                context.startActivity(i);

                new Pushnotification().execute();


            }
        });

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = "Reject";
                charnm = capitalize(sp.getString(Constant.username, ""));
                Log.e("receiver_image", model.getUserimage());
                book_name = model.getBookname();
                book_image = model.getBookImage();
                buyusername = model.getBookNane();
                book_id = model.getRecever_id();
                rid = model.getSellerid();
                book_price = model.getPrice();
                buyer_id = model.getBuyerid();
                // Log.e("sendername", getDataAdapter1.getReciever_name());
                Intent i = new Intent(context, Personalchatofferaccepted.class);
                i.putExtra("receiver_id", model.getBuyerid());
                i.putExtra("sendername", model.getBookNane());
                i.putExtra("senderimage", model.getUserimage());
                i.putExtra("counter", "counter");

                i.putExtra("user_chk", rid);
                i.putExtra("offer_rejected", "Offer Rejected:");
                i.putExtra("buyer_id", buyer_id);
                i.putExtra("book_id", book_id);
                i.putExtra("name", book_name);
                i.putExtra("image", book_image);
                i.putExtra("price", book_price);


                context.startActivity(i);
                context.finish();
                Log.e("send Reject", "image  " + model.getBookNane());


                sp.edit().putString("bookName", book_name).commit();
                sp.edit().putString("book_id", book_id).commit();
                sp.edit().putString("bookImage", book_image).commit();

                context.startActivity(i);
                new Pushnotification().execute();


            }
        });

        return view;
    }

    private String capitalize(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    private class Pushnotification extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            String serverUrl = Constant.MAIN_URL + "offer_push.php";
            System.out.println("the url is :" + serverUrl);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(serverUrl);

            try {
                JSONObject jo = new JSONObject();

                jo.put("buy_id", buyer_id);
                jo.put("sell_id", rid);
                jo.put("status", status);
                jo.put("book_id", book_id);
                //jo.put("message", messageBody);
                jo.put("sell_name", charnm);
                jo.put("buy_name", buyusername);


                System.out.println("the send data is :" + jo);

                StringEntity se = new StringEntity(jo.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                String responseText = EntityUtils.toString(entity);
                System.out.println("the response is :" + responseText);
                success = new JSONObject(responseText);
            } catch (Exception e) {
                System.out.println("Exception inProfileActivity : " + e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            {
                super.onPostExecute(result);

                try {
                    if (success.getString("success").equals("1")) {

                    }
                } catch (Exception e) {

                }


            }

        }
    }


}