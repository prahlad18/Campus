package app.livecampus.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import app.livecampus.Pojo.DataModel;
import app.livecampus.R;
import app.livecampus.Utils.Constant;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by isquare3 on 4/12/18.
 */

public class Campuschatadaptor extends RecyclerView.Adapter<Campuschatadaptor.ViewHolder> {

    public CircleImageView bookimage;
    public boolean call = true;
    public String userID = "4";
    public Thread t;
    public TextView title;
    public TextView tv_sender;
    public String user_id, reciver_id;
    Context context;
    ArrayList<DataModel> getDataAdapter;
    SharedPreferences sp;
    RecyclerView listView;
    JSONArray jsonArrayCategory;
    RecyclerView.Adapter adapter;
    ArrayList<DataModel> arrayList;
    ArrayList<DataModel> hs = new ArrayList<>();
    DataModel category;
    GridLayoutManager mLayoutManager;
    AsyncTask<Void, Void, Void> SendData;
    Handler mHandler;
    String book_id, book_name, book_image, rid;
    int value = -1;
    private String messageBody, img;
    private JSONObject success;
    private ProgressDialog pDialog;
    private Button mButtonSend, btn_ok;
    private EditText mEditTextMessage;

    public Campuschatadaptor(Context context, ArrayList<DataModel> arrayList) {

        super();
        this.context = context;
        this.getDataAdapter = arrayList;
    }

    @Override
    public Campuschatadaptor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.campuschatitem, parent, false);
        Campuschatadaptor.ViewHolder viewHolder = new Campuschatadaptor.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        DataModel getDataAdapter1 = getDataAdapter.get(position);
        sp = context.getSharedPreferences("Prefrence", Context.MODE_PRIVATE);

        String username = sp.getString(Constant.username, "");
        user_id = sp.getString(Constant.uid, "");
        String ruserImage = getDataAdapter1.getImage();
        String senderdt = getDataAdapter1.getDatetime();
        String receverdt = getDataAdapter1.getDatetime();
        String profile = sp.getString(Constant.profile, "");


        String rusername = getDataAdapter1.getReciever_name();
        String chars = capitalize(username);


        rid = sp.getString(Constant.rid, "");
        holder.tv_receiver.setVisibility(View.VISIBLE);
        holder.tv_sender.setVisibility(View.VISIBLE);
        holder.tv_sender_name.setVisibility(View.VISIBLE);
        holder.tv_receiver_name.setVisibility(View.VISIBLE);


        if (ruserImage.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.main_ic)
                    .into(holder.circleImageView);
        } else {
            Picasso.with(context)
                    .load(Constant.MAIN_URL + ruserImage)
                    .placeholder(R.drawable.main_ic)
                    .resize(125, 125)
                    .into(holder.circleImageView);
        }
        if (profile.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.logo)
                    .placeholder(R.drawable.logo)
                    .into(holder.circleImageView1);
        } else {
            Picasso.with(context)
                    .load(profile)
                    .resize(125, 125)
                    .placeholder(R.drawable.logo)
                    .into(holder.circleImageView1);
        }

        holder.senderdt.setText(senderdt);
        holder.receiverdt.setText(receverdt);

        if (rid.equals(getDataAdapter1.getSenderID())) {


            if (getDataAdapter1.getMsg().equalsIgnoreCase("LOL")) {

                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);

            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("WTF")) {

                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);

            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("YOLO")) {

                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);

            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("HI")) {

                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);

            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("CRAZY")) {

                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);

            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("TTYL")) {

                holder.tv_sender.setText(getDataAdapter1.getMsg());
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);

            } else {
                String name = getDataAdapter1.getMsg();
                String charsf = capitalize(name);


                holder.tv_sender.setText(charsf);
                holder.tv_sender_name.setText(" :" + chars);

                holder.tv_receiver.setVisibility(View.GONE);
                holder.tv_receiver_name.setVisibility(View.GONE);
                holder.tv_sender.setVisibility(View.VISIBLE);
                holder.tv_sender_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.GONE);
                holder.circleImageView1.setVisibility(View.VISIBLE);
                holder.senderdt.setVisibility(View.VISIBLE);
                holder.receiverdt.setVisibility(View.GONE);


            }


        } else {
            if (getDataAdapter1.getMsg().equalsIgnoreCase("LOL")) {

                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);

            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("WTF")) {

                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);
            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("YOLO")) {

                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);
            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("HI")) {

                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);
            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("CRAZY")) {

                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);
            } else if (getDataAdapter1.getMsg().equalsIgnoreCase("TTYL")) {

                holder.tv_receiver.setText(getDataAdapter1.getMsg());
                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);
            } else {


                String name = getDataAdapter1.getMsg();
                String charsr = capitalize(name);

                holder.tv_receiver_name.setText(rusername + ": ");
                holder.tv_receiver.setText(charsr);
                holder.tv_sender.setVisibility(View.GONE);
                holder.tv_sender_name.setVisibility(View.GONE);
                holder.tv_receiver.setVisibility(View.VISIBLE);
                holder.tv_receiver_name.setVisibility(View.VISIBLE);
                holder.circleImageView.setVisibility(View.VISIBLE);
                holder.circleImageView1.setVisibility(View.GONE);
                holder.senderdt.setVisibility(View.GONE);
                holder.receiverdt.setVisibility(View.VISIBLE);

            }


        }

    }

    @Override
    public int getItemCount() {

        return getDataAdapter.size();
    }

    private String capitalize(String text) {
        if (null != text && text.length() != 0) {
            text = text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();

        }
        return text;
    }

   /* private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }*/

   /* private String capitalize( final String text ) {

        return text.substring( 0, 1 ).toUpperCase() + text.substring( 1 ).toLowerCase();
    }*/

    public class ViewHolder extends RecyclerView.ViewHolder {
        String userID = "4";
        TextView tv_sender, tv_receiver, tv_receiver_name, tv_sender_name, senderdt, receiverdt;
        LinearLayout linearLayout;
        CircleImageView circleImageView, circleImageView1;

        public ViewHolder(View itemView) {

            super(itemView);
            tv_sender = (TextView) itemView.findViewById(R.id.tv_sender);
            tv_receiver_name = (TextView) itemView.findViewById(R.id.tv_receiver_name);
            senderdt = (TextView) itemView.findViewById(R.id.senderdt);
            receiverdt = (TextView) itemView.findViewById(R.id.receiverdt);
            tv_sender_name = (TextView) itemView.findViewById(R.id.tv_sender_name);
            tv_receiver = (TextView) itemView.findViewById(R.id.tv_reciever);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.userImage);
            circleImageView1 = (CircleImageView) itemView.findViewById(R.id.senderimage);
            //linearLayout.setOnClickListener(this);


        }

        /*@Override
        public void onClick(View v) {

            DataModel getDataAdapter1 = getDataAdapter.get(getAdapterPosition());



            switch (v.getId()) {
                case R.id.linearLayout:
                    Log.e("click","click");
                    Log.e("receiver_id",getDataAdapter1.getSenderID());
                    Log.e("sendername",getDataAdapter1.getReciever_name());
                    Intent i = new Intent(context, Personalchat_activity.class);
                    i.putExtra("receiver_id",getDataAdapter1.getSenderID());
                    i.putExtra("sendername",getDataAdapter1.getReciever_name());
                    context.startActivity(i);

                    break;

//                    http://www.smartbaba.in/liveatcampus/api
            }

        }*/
    }

}










