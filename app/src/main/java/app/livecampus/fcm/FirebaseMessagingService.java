package app.livecampus.fcm;
/*

        Copyright 2018.

        ========================================================================
        Way Of Schooling CONFIDENTIAL
        ______________________________________________________________________

        All Rights Reserved.

        NOTICE:  No part of this publication may be reproduced, distributed, or transmitted in any form or    by any means, including photocopying, recording, or other electronic or mechanical methods, without    the prior written permission of the Way Of Schooling CORPORATION except in the case of brief    quotations embodied in critical reviews and certain other noncommercial uses permitted by copyright    law.
        All information contained herein is, and remains
        the property of Way Of Schooling CORPORATION ,
        The intellectual and technical concepts contained
        herein are proprietary to Way Of Schooling Incorporated
        and its suppliers and may be covered by India,U.S. and Foreign Patents,
        patents in process, and are protected by trade secret or copyright law.
        Dissemination of this information or reproduction of this material
        is strictly forbidden unless prior written permission is obtained
        from Way Of Schooling Incorporated.

        ========================================================================
        Way Of Schooling  CORPORATION CONFIDENTIAL
        ========================================================================
        */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.livecampus.R;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    public static final String SCHOOL_PUSH_NOTIFICATION_BROADCAST = "school_push_notification_broadcast";
    public static final String CLASS_PUSH_NOTIFICATION_BROADCAST = "class_push_notification_broadcast";
    public static final String SCHEDULE_PUSH_NOTIFICATION_BROADCAST = "schedule_push_notification_broadcast";
    public static final String EVENTS_PUSH_NOTIFICATION_BROADCAST = "events_push_notification_broadcast";
    public static final String TEACHER_COMM_PUSH_NOTIFICATION_BROADCAST = "teacher_comm_push_notification_broadcast";
    public static final String TEACHER_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST = "teacher_comm_thread_push_notification_broadcast";
    public static final String SCHOOL_COMM_PUSH_NOTIFICATION_BROADCAST = "school_comm_push_notification_broadcast";
    public static final String SCHOOL_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST = "school_comm_thread_push_notification_broadcast";
    public static final String P2P_COMM_PUSH_NOTIFICATION_BROADCAST = "p2p_comm_push_notification_broadcast";
    public static final String P2P_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST = "p2p_comm_thread_push_notification_broadcast";
    public static final String SCHOOL_BUS_PUSH_NOTIFICATION_BROADCAST = "school_bus_push_notification_broadcast";
    public static final String ATTENDANCE_PUSH_NOTIFICATION_BROADCAST = "attendance_push_notification_broadcast";
    public static final String UPDATE_DASHBOARD_BROADCAST = "update_dashboard_broadcast";
    public static final String HOMEWORK_PUSH_NOTIFICATION_BROADCAST = "homework_push_notification_broadcast";
    public static final String CHANNEL_TYPE = "channelType";
    public static final String MESSAGE_TYPE = "messageType";
    public static final String SCHOOL = "school";
    public static final String CLASS = "class";
    public static final String APPOINTMENT = "appointment";
    public static final String EVENTS = "event";
    public static final String NOTIFICATION = "notification";
    public static final String COMMUNICATION = "communication";
    public static final String SCHOOL_BUS = "schoolbus";
    public static final String ATTENDANCE = "attendance";
    public static final String P2P_COMM = "p2p";
    public static final String COMM_TYPE = "commType";
    public static final String BUSTYPE = "busType";
    public static final String STUDENT_ID = "studentId";
    public static final String PROFILE_ID = "profileId";
    public static final String GRADE_ID = "gradeId";
    public static final String CLASS_ID = "classId";
    public static final String SCHOOL_ID = "schoolId";
    public static final String MESSAGE_ID = "messageId";
    public static final String APPOINTMENT_ID = "appointmentId";
    public static final String NOTIFICATION_ID = "notificationId";
    public static final String GROUP_ID = "groupId";
    public static final String HOMEWORK = "homework";
    //public static final String SCHOOL = "school";
    public static final String TEACHER = "teacher";
    private static final String TAG = FirebaseMessagingService.class.getSimpleName();
    Intent intent;
    String imageUrl;
    String channelType, messageType, gradId, classId, studentId, profileId, schoolId, messageId, appointmentId, notificationId, groupId, commType;
    List<String> studentIds = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("notification", remoteMessage.getNotification().getBody() + " N " + remoteMessage.getData());
        Log.e("the message get", " firebase 1 " + remoteMessage.getMessageType());
        Log.e("the message get", " firebase 2 " + remoteMessage.getData().toString());
        Log.e("the message get", " firebase 3 " + remoteMessage.getMessageId());
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        Log.e(TAG, "From: " + remoteMessage.getFrom());


        if (messageType != null) {
            showNotification(remoteMessage);


            /*switch (messageType) {
                case APPOINTMENT:

                    if (parentPermission != null) {

                        if (parentPermission.isAppointment()) {
                            if (UpcomingAppointmentFragment.isScheduleVisible) {
                                Intent intent = new Intent(SCHEDULE_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(STUDENT_ID, studentId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }
                        }

                    } else {

                        if (UpcomingAppointmentFragment.isScheduleVisible) {
                            Intent intent = new Intent(SCHEDULE_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        } else {
                            showNotification(remoteMessage);
                        }

                    }

                    break;
                case EVENTS:

                    if (parentPermission != null) {
                        if (parentPermission.isEvents()) {
                            if (isTodaysEventVisible || isUpcomingEventVisible) {
                                Intent intent = new Intent(EVENTS_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(STUDENT_ID, studentId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }
                        }

                    } else {
                        if (isTodaysEventVisible || isUpcomingEventVisible) {
                            Intent intent = new Intent(EVENTS_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        } else {
                            showNotification(remoteMessage);
                        }
                    }

                    break;
                case P2P_COMM:

                    if (parentPermission != null) {
                        if (parentPermission.isP2p()) {
                            if (isP2PChatListScreenVisible) {

                                int currentCounter = BadgeCountTracker.getP2PCommunicationSingleThreadBadgeCounter(this, groupId);
                                BadgeCountTracker.saveP2PCommunicationSingleThreadBadgeCounter(this, currentCounter + 1, groupId);

                                Intent intent = new Intent(P2P_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(GROUP_ID, groupId);
                                intent.putExtra(STUDENT_ID, studentId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                            } else if (isP2PChatScreenVisible) {
                                Intent intent = new Intent(P2P_COMM_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(GROUP_ID, groupId);
                                intent.putExtra(STUDENT_ID, studentId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                            } else {

                                showNotification(remoteMessage);
                            }
                        }

                    } else {

                        if (isP2PChatListScreenVisible) {

                            int currentCounter = BadgeCountTracker.getP2PCommunicationSingleThreadBadgeCounter(this, groupId);
                            BadgeCountTracker.saveP2PCommunicationSingleThreadBadgeCounter(this, currentCounter + 1, groupId);

                            Intent intent = new Intent(P2P_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(GROUP_ID, groupId);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        } else if (isP2PChatScreenVisible) {
                            Intent intent = new Intent(P2P_COMM_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(GROUP_ID, groupId);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        } else {

                            showNotification(remoteMessage);
                        }
                    }


                    break;
                case COMMUNICATION:

                    if (parentPermission != null) {
                        if (parentPermission.isCommunication()) {


                            if (commType != null && commType.equals(TEACHER)) {

                                if (isTeacherCommVisible) {

                                    int currentCounter = BadgeCountTracker.getCommunicationSingleThreadBadgeCounter(this, messageId);
                                    BadgeCountTracker.saveCommunicationSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                                    Intent intent = new Intent(TEACHER_COMM_PUSH_NOTIFICATION_BROADCAST);
                                    intent.putExtra(STUDENT_ID, studentId);
                                    intent.putExtra(MESSAGE_ID, messageId);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                } else if (ThreadCommunicationActivity.isTheradScreenVisible) {

                                    if (SchoolApp.getInstance().getStudent().getStudentId().equals(studentId)) {

                                        Intent intent = new Intent(TEACHER_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST);
                                        intent.putExtra(STUDENT_ID, studentId);
                                        intent.putExtra(MESSAGE_ID, messageId);
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                    } else {

                                        showNotification(remoteMessage);
                                    }


                                } else {

                                    showNotification(remoteMessage);
                                }

                            } else if (commType != null && commType.equals(SCHOOL)) {

                                if (isSchoolCommListVisible) {

                                    int currentCounter = BadgeCountTracker.getSchoolCommunicationSingleThreadBadgeCounter(this, messageId);
                                    BadgeCountTracker.saveSchoolCommunicationSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                                    Intent intent = new Intent(SCHOOL_COMM_PUSH_NOTIFICATION_BROADCAST);
                                    intent.putExtra(STUDENT_ID, studentId);
                                    intent.putExtra(MESSAGE_ID, messageId);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                } else if (ThreadCommunicationActivity.isTheradScreenVisible) {

                                    if (SchoolApp.getInstance().getStudent().getStudentId().equals(studentId)) {

                                        Intent intent = new Intent(SCHOOL_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST);
                                        intent.putExtra(STUDENT_ID, studentId);
                                        intent.putExtra(MESSAGE_ID, messageId);
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                    } else {

                                        showNotification(remoteMessage);
                                    }


                                } else {

                                    showNotification(remoteMessage);
                                }

                            }

                        }

                    } else {


                        if (commType != null && commType.equals(TEACHER)) {

                            if (isTeacherCommVisible) {

                                int currentCounter = BadgeCountTracker.getCommunicationSingleThreadBadgeCounter(this, messageId);
                                BadgeCountTracker.saveCommunicationSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                                Intent intent = new Intent(TEACHER_COMM_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(STUDENT_ID, studentId);
                                intent.putExtra(MESSAGE_ID, messageId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                            } else if (ThreadCommunicationActivity.isTheradScreenVisible) {

                                if (SchoolApp.getInstance().getStudent().getStudentId().equals(studentId)) {

                                    Intent intent = new Intent(TEACHER_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST);
                                    intent.putExtra(STUDENT_ID, studentId);
                                    intent.putExtra(MESSAGE_ID, messageId);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                } else {

                                    showNotification(remoteMessage);
                                }


                            } else {

                                showNotification(remoteMessage);
                            }

                        } else if (commType != null && commType.equals(SCHOOL)) {

                            if (isSchoolCommListVisible) {

                                int currentCounter = BadgeCountTracker.getSchoolCommunicationSingleThreadBadgeCounter(this, messageId);
                                BadgeCountTracker.saveSchoolCommunicationSingleThreadBadgeCounter(this, currentCounter + 1, messageId);

                                Intent intent = new Intent(SCHOOL_COMM_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(STUDENT_ID, studentId);
                                intent.putExtra(MESSAGE_ID, messageId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                            } else if (ThreadCommunicationActivity.isTheradScreenVisible) {

                                if (SchoolApp.getInstance().getStudent().getStudentId().equals(studentId)) {

                                    Intent intent = new Intent(SCHOOL_COMM_THREAD_PUSH_NOTIFICATION_BROADCAST);
                                    intent.putExtra(STUDENT_ID, studentId);
                                    intent.putExtra(MESSAGE_ID, messageId);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                                } else {

                                    showNotification(remoteMessage);
                                }


                            } else {

                                showNotification(remoteMessage);
                            }

                        }

                    }


                    break;

                case SCHOOL_BUS:

                    if (parentPermission != null) {
                        if (parentPermission.isRoute()) {
                            if (isBusScreenVisible) {
                                Intent intent = new Intent(SCHOOL_BUS_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(PROFILE_ID, profileId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }
                        }

                    } else {
                        if (isBusScreenVisible) {
                            Intent intent = new Intent(SCHOOL_BUS_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(PROFILE_ID, profileId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        } else {
                            showNotification(remoteMessage);
                        }
                    }

                    break;

                case ATTENDANCE:

                    if (parentPermission != null) {
                        if (parentPermission.isAttendance()) {
                            if (HomeFragment.isHomeScreenVisible) {
                                Intent intent = new Intent(ATTENDANCE_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(STUDENT_ID, studentId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }
                        }

                    } else {
                        if (HomeFragment.isHomeScreenVisible) {
                            Intent intent = new Intent(ATTENDANCE_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        } else {
                            showNotification(remoteMessage);
                        }
                    }


                    break;

                case HOMEWORK:

                    if (parentPermission != null) {
                        if (parentPermission.isHomework()) {
                            if (HomeFragment.isHomeScreenVisible) {
                                Intent intent = new Intent(HOMEWORK_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(STUDENT_ID, studentId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }

                        }

                    } else {
                        if (HomeFragment.isHomeScreenVisible) {
                            Intent intent = new Intent(HOMEWORK_PUSH_NOTIFICATION_BROADCAST);
                            intent.putExtra(STUDENT_ID, studentId);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                        } else {
                            showNotification(remoteMessage);
                        }

                    }


                    break;

                case NOTIFICATION:

                    if (parentPermission != null) {
                        if (parentPermission.isChannel()) {

                            if (channelType != null && channelType.equals(CLASS)) {

                                if (ClassChanelFragment.isClassChannelVisible) {
                                    Intent intent = new Intent(CLASS_PUSH_NOTIFICATION_BROADCAST);
                                    intent.putExtra(GRADE_ID, gradId);
                                    intent.putExtra(CLASS_ID, classId);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                                } else {
                                    showNotification(remoteMessage);
                                }

                            } else if (channelType != null && channelType.equals(SCHOOL)) {

                                if (SchoolChanelFragment.isSchoolChannelVisible) {
                                    Intent intent = new Intent(SCHOOL_PUSH_NOTIFICATION_BROADCAST);
                                    intent.putExtra(SCHOOL_ID, schoolId);
                                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                                } else {
                                    showNotification(remoteMessage);
                                }
                            }

                        }

                    } else {

                        if (channelType != null && channelType.equals(CLASS)) {

                            if (ClassChanelFragment.isClassChannelVisible) {
                                Intent intent = new Intent(CLASS_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(GRADE_ID, gradId);
                                intent.putExtra(CLASS_ID, classId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }

                        } else if (channelType != null && channelType.equals(SCHOOL)) {

                            if (SchoolChanelFragment.isSchoolChannelVisible) {
                                Intent intent = new Intent(SCHOOL_PUSH_NOTIFICATION_BROADCAST);
                                intent.putExtra(SCHOOL_ID, schoolId);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            } else {
                                showNotification(remoteMessage);
                            }
                        }

                    }

                    break;

                default:

                    showNotification(remoteMessage);

                    break;
            }

            //  showNotification(remoteMessage);
*/

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotification(RemoteMessage message) {


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Log.e("type ", "is " + messageType);

        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(message.getNotification().getTitle())
                .setContentText(message.getNotification().getBody())
                .setSmallIcon(R.drawable.ic_launcher)
                .setSound(uri)
                .setContentIntent(pendingIntent);*/

        int notifyID = 1;
        String CHANNEL_ID = "fcm_default_channel";// The id of the channel.


        CharSequence name = getString(R.string.default_notification_channel_id);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = null;
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notification = new Notification.Builder(this)
                    .setAutoCancel(true)
                    .setContentTitle(message.getNotification().getTitle())
                    .setContentText(message.getNotification().getBody())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(CHANNEL_ID)
                    .setSound(uri)
                    .setContentIntent(pendingIntent)
                    .build();
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
            mNotificationManager.notify(notifyID, notification);
        } else {
            Log.e("type ", "is " + messageType);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setAutoCancel(true)
                    .setContentTitle(message.getNotification().getTitle())
                    .setContentText(message.getNotification().getBody())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setSound(uri)
                    .setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            manager.notify(0, builder.build());
        }


      /*  NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(0, builder.build());
*/


    }

    public void handleIntent(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        Bundle extras = intent.getExtras();
        if (extras != null) {

            try {
                JSONObject data = json.getJSONObject("data");

                String title = data.getString("title");
                String message = data.getString("message");
                boolean isBackground = data.getBoolean("is_background");
                imageUrl = data.getString("image");
                String timestamp = data.getString("timestamp");
                JSONObject payload = data.getJSONObject("payload");

                Log.e(TAG, "title: " + title);
                Log.e(TAG, "message: " + message);
                Log.e(TAG, "isBackground: " + isBackground);
                Log.e(TAG, "payload: " + payload.toString());
                Log.e(TAG, "imageUrl: " + imageUrl);
                Log.e(TAG, "timestamp: " + timestamp);


            } catch (Exception e) {
                System.out.println("error in " + e.getMessage());
            }



            /*if (messageType != null) {
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
            }*/

        } else {
            Log.e("handleIntent", "not found in handleIntent");
        }


    }
}
